﻿using System.IO;
using Ionic.Zip;

namespace ZipPlugin
{
    public class ZipUtil
    {
        public static void Unzip(string zipFilePath, string location)
        {
#if UNITY_EDITOR
            Directory.CreateDirectory(location);

            using (ZipFile zip = ZipFile.Read(zipFilePath))
            {
                zip.ExtractAll(location, ExtractExistingFileAction.OverwriteSilently);
            }
#endif
        }

        public static void Zip(string zipFileName, params string[] files)
        {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
            string path = Path.GetDirectoryName(zipFileName);
            Directory.CreateDirectory(path);

            using (ZipFile zip = new ZipFile())
            {
                foreach (string file in files)
                {
                    zip.AddFile(file, "");
                }

                zip.Save(zipFileName);
            }
#elif UNITY_ANDROID
		using (AndroidJavaClass zipper = new AndroidJavaClass ("com.tsw.zipper")) {
			{
				zipper.CallStatic ("zip", zipFileName, files);
			}
		}
#elif UNITY_IPHONE
		foreach (string file in files) {
			addZipFile (file);
		}
		zip (zipFileName);
#endif
        }

        public static void ZipFoldersAndFiles(string zipFileName, string filename, params string[] folders)
        {
#if UNITY_EDITOR
            string path = Path.GetDirectoryName(zipFileName);
            Directory.CreateDirectory(path);

            using (ZipFile zip = new ZipFile())
            {
                foreach (var folder in folders)
                {
                    var folderPath = path + "/" + folder;
                    if (!Directory.Exists(folderPath)) continue;
                    zip.AddDirectory(folderPath, folder);
                }

                if (!string.IsNullOrEmpty(filename))
                    zip.AddFile(path + "/" + filename, "");
                zip.Save(zipFileName);
            }
#endif
        }

        public static void ZipFolders(string zipFileName, params string[] folders)
        {
#if UNITY_EDITOR
            string path = Path.GetDirectoryName(zipFileName);
            Directory.CreateDirectory(path);

            using (ZipFile zip = new ZipFile())
            {
                foreach (string folder in folders)
                {
                    zip.AddDirectory(path + "/" + folder, folder);
                }

                zip.Save(zipFileName);
            }
#endif
        }
    }
}