using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableEventBuilder
    {
        private ScriptableEvent Build()
        {
            var scriptable = ScriptableObject.CreateInstance<ScriptableEvent>();
            return scriptable;
        }

        public static implicit operator ScriptableEvent(ScriptableEventBuilder builder) => builder.Build();
    }
}