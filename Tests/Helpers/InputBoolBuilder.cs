using FantasyArts.Tools.Runtime.Architecture.Scriptables;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class InputBoolBuilder
    {
        private bool _value;
        private bool _isNegated;

        public InputBoolBuilder WithValue(bool value)
        {
            _value = value;
            return this;
        }

        public InputBoolBuilder AsNegated(bool isNegated)
        {
            _isNegated = isNegated;
            return this;
        }

        private InputBool Build()
        {
            ScriptableBool scriptableBool = A.ScriptableBool.WithValue(_value);
            var logicalBool = new InputBool(scriptableBool, _isNegated);
            return logicalBool;
        }

        public static implicit operator InputBool(InputBoolBuilder builder) => builder.Build();
    }
}