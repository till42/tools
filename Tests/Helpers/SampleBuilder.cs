using System.Collections.Generic;
using FantasyArts.Tools.Runtime.Architecture.Extensions;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class SampleBuilder<T>
    {
        private IEnumerable<T> _enumerable;


        public SampleBuilder<T> WithEnumerable(IEnumerable<T> enumerable)
        {
            _enumerable = enumerable;
            return this;
        }

        public Sample<T> Build()
        {
            return new Sample<T>(_enumerable);
        }

        public static implicit operator Sample<T>(SampleBuilder<T> builder) => builder.Build();
    }
}