using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEditor;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableLeveledBoolBuilder
    {
        private bool[] _values;

        public ScriptableLeveledBoolBuilder WithValues(bool[] values)
        {
            _values = values;
            return this;
        }

        public ScriptableLeveledBool Build()
        {
            var scriptable = ScriptableObject.CreateInstance<ScriptableLeveledBool>();

            if (_values != null)
                scriptable.SetPrivate(s => s.Values, _values);

            return scriptable;
        }

        public static implicit operator ScriptableLeveledBool(ScriptableLeveledBoolBuilder builder) =>
            builder.Build();
    }
}