﻿using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableEventIntBuilder
    {
        private ScriptableEventInt Build()
        {
            var scriptable = ScriptableObject.CreateInstance<ScriptableEventInt>();
            return scriptable;
        }

        public static implicit operator ScriptableEventInt(ScriptableEventIntBuilder builder) => builder.Build();
    }
}