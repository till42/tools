﻿using FantasyArts.Tools.Runtime.Architecture.StateMachine;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class StateMachineScriptableBuilder
    {
        private int _numberOfStates;

        public StateMachineScriptableBuilder WithStates(int numberOfStates)
        {
            _numberOfStates = numberOfStates;
            return this;
        }

        public StateMachineScriptable Build()
        {
            var stateMachine = ScriptableObject.CreateInstance<StateMachineScriptable>();

            CreateStates(stateMachine);

            CreateConnectingTransitions(stateMachine);

            return stateMachine;
        }

        private void CreateStates(StateMachineScriptable stateMachine)
        {
            for (var i = 0; i < _numberOfStates; i++)
            {
                stateMachine.AppendNewStateScriptable($"{i}");
            }
        }

        private void CreateConnectingTransitions(StateMachineScriptable stateMachine)
        {
            var states = stateMachine.States;
            for (int i = 0; i < states.Count - 1; i++)
            {
                var state = states[i];
                var nextState = states[i + 1];
                state.AddTransitionTo(nextState, $"{i}");
            }
        }

        public static implicit operator StateMachineScriptable(StateMachineScriptableBuilder builder) =>
            builder.Build();
    }
}