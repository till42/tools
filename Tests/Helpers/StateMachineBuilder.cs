﻿using FantasyArts.Tools.Runtime.Architecture.StateMachine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class StateMachineBuilder
    {
        private IState _startState;

        public StateMachineBuilder StartedInState(IState startState)
        {
            _startState = startState;
            return this;
        }

        public StateMachine Build()
        {
            var stateMachine = new StateMachine();

            if (_startState != null)
            {
                stateMachine.Start(_startState);
            }

            return stateMachine;
        }

        public static implicit operator StateMachine(StateMachineBuilder builder) => builder.Build();
    }
}