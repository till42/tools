using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class Builder<T> where T : Component
    {
        private T Build()
        {
            var gameObject = new GameObject();
            var component = gameObject.AddComponent<T>();
            return component;
        }

        public static implicit operator T(Builder<T> builder) => builder.Build();
    }
}