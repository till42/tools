using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableIntListenerBuilder
    {
        private ScriptableInt _scriptableInt;
        private ScriptableIntCondition _condition;

        private RaiseMonitor _raisedAction;
        private RaiseMonitor _raisedUnityEventMonitor;

        private bool _doClearRaiseCounts;

        public ScriptableIntListenerBuilder WithScriptableInt(out ScriptableInt scriptableInt)
        {
            scriptableInt = A.ScriptableInt;
            _scriptableInt = scriptableInt;
            return this;
        }

        public ScriptableIntListenerBuilder WithCondition(out ScriptableIntCondition condition)
        {
            ScriptableIntConditionGreaterThan conditionGreaterThan = A.ScriptableIntConditionGreaterThan;
            condition = conditionGreaterThan;
            _condition = condition;
            return this;
        }

        public ScriptableIntListenerBuilder WithRaiseActionMonitor(out RaiseMonitor raisedAction)
        {
            raisedAction = new RaiseMonitor();
            _raisedAction = raisedAction;
            return this;
        }

        public ScriptableIntListenerBuilder WithRaiseUnityEventMonitor(out RaiseMonitor raisedUnityEventMonitor)
        {
            raisedUnityEventMonitor = new RaiseMonitor();
            _raisedUnityEventMonitor = raisedUnityEventMonitor;
            return this;
        }

        public ScriptableIntListenerBuilder WithClearedRaiseCounts()
        {
            _doClearRaiseCounts = true;
            return this;
        }

        public ScriptableIntListenBehaviour Build()
        {
            var gameObject = new GameObject();
            var listener = gameObject.AddComponent<ScriptableIntListenBehaviour>();

            if (_raisedAction != null)
                listener.OnEventAction += _raisedAction.RaiseAction;

            if (_raisedUnityEventMonitor != null)
                listener.OnEventRaised.AddListener(_raisedUnityEventMonitor.RaiseAction);

            if (_scriptableInt != null)
                listener.SetPrivate(l => l.ScriptableInt, _scriptableInt);

            if (_condition != null)
                listener.SetPrivate(l => l.Condition, _condition);


            if (_doClearRaiseCounts)
            {
                _raisedAction?.Clear();
                _raisedUnityEventMonitor?.Clear();
                listener.Clear();
            }

            return listener;
        }

        public static implicit operator ScriptableIntListenBehaviour(ScriptableIntListenerBuilder builder) =>
            builder.Build();
    }
}