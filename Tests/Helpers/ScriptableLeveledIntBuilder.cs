using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEditor;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableLeveledIntBuilder
    {
        private int[] _values;

        public ScriptableLeveledIntBuilder WithValues(int[] values)
        {
            _values = values;
            return this;
        }

        public ScriptableLeveledInt Build()
        {
            var scriptable = ScriptableObject.CreateInstance<ScriptableLeveledInt>();

            if (_values != null)
                scriptable.SetPrivate(s => s.Values, _values);

            return scriptable;
        }

        public static implicit operator ScriptableLeveledInt(ScriptableLeveledIntBuilder builder) =>
            builder.Build();
    }
}