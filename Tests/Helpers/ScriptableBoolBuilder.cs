using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableBoolBuilder
    {
        private bool _value;

        public ScriptableBoolBuilder WithValue(bool value)
        {
            _value = value;
            return this;
        }

        public ScriptableBool Build()
        {
            var scriptable = ScriptableObject.CreateInstance<ScriptableBool>();
            scriptable.SetPrivate(s => s.Value, _value);
            return scriptable;
        }

        public static implicit operator ScriptableBool(ScriptableBoolBuilder builder) => builder.Build();
    }
}