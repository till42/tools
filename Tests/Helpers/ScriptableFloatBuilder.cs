using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableFloatBuilder
    {
        private float _value;

        public ScriptableFloatBuilder WithValue(float value)
        {
            _value = value;
            return this;
        }

        public ScriptableFloat Build()
        {
            var scriptable = ScriptableObject.CreateInstance<ScriptableFloat>();
            scriptable.SetPrivate(s => s.Value, _value);
            return scriptable;
        }

        public static implicit operator ScriptableFloat(ScriptableFloatBuilder builder) => builder.Build();
    }
}