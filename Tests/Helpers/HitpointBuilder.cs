using FantasyArts.Tools.Runtime.Game;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class HitpointBuilder
    {
        private int _maxHitpoints;

        public Hitpoint WithMaxValue(int maxValue)
        {
            _maxHitpoints = maxValue;
            return this;
        }

        public Hitpoint Build()
        {
            return new Hitpoint(_maxHitpoints);
        }

        public static implicit operator Hitpoint(HitpointBuilder builder) => builder.Build();
    }
}