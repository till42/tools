using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableEventListenerBuilder
    {
        private ScriptableEvent _scriptableEvent;

        private RaiseMonitor _raisedAction;
        private RaiseMonitor _raisedUnityEventMonitor;

        public ScriptableEventListenerBuilder WithScriptableEvent(out ScriptableEvent scriptableEvent)
        {
            scriptableEvent = A.ScriptableEvent;
            _scriptableEvent = scriptableEvent;
            return this;
        }

        public ScriptableEventListenerBuilder WithRaiseActionMonitor(out RaiseMonitor raisedAction)
        {
            raisedAction = new RaiseMonitor();
            _raisedAction = raisedAction;
            return this;
        }

        public ScriptableEventListenerBuilder WithRaiseUnityEventMonitor(out RaiseMonitor raisedUnityEventMonitor)
        {
            raisedUnityEventMonitor = new RaiseMonitor();
            _raisedUnityEventMonitor = raisedUnityEventMonitor;
            return this;
        }

        public ScriptableEventListenBehaviour Build()
        {
            var gameObject = new GameObject();
            var listener = gameObject.AddComponent<ScriptableEventListenBehaviour>();
            if (_scriptableEvent != null)
                listener.SetPrivate(l => l.ScriptableEvent, _scriptableEvent);

            if (_raisedAction != null)
                listener.OnEventAction += _raisedAction.RaiseAction;

            if (_raisedUnityEventMonitor != null)
                listener.OnEventRaised.AddListener(_raisedUnityEventMonitor.RaiseAction);

            return listener;
        }

        public static implicit operator ScriptableEventListenBehaviour(ScriptableEventListenerBuilder builder) =>
            builder.Build();
    }
}