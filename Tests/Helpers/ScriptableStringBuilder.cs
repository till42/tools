using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEditor;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableStringBuilder
    {
        private string _value;

        public ScriptableStringBuilder WithValue(string value)
        {
            _value = value;
            return this;
        }


        public ScriptableString Build()
        {
            var scriptable = ScriptableObject.CreateInstance<ScriptableString>();
            var serializedObject = new SerializedObject(scriptable);

            var propertyValue = serializedObject.FindProperty("_value");
            propertyValue.stringValue = _value;

            serializedObject.ApplyModifiedPropertiesWithoutUndo();
            return scriptable;
        }

        public static implicit operator ScriptableString(ScriptableStringBuilder builder) => builder.Build();
    }
}