using FantasyArts.Tools.Runtime.Architecture.Scriptables;

namespace FantasyArts.Tools.Tests.Helpers
{
    public static class BoolArrayExtensions
    {
        public static InputBool[] CreateInputBools(this bool[] values, bool isNegated = false)
        {
            var result = new InputBool[values.Length];
            for (var i = 0; i < values.Length; i++)
            {
                result[i] = An.InputBool.WithValue(values[i]).AsNegated(isNegated);
            }

            return result;
        }
    }
}