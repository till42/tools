using FluentAssertions.Events;

namespace FantasyArts.Tools.Tests.Helpers
{
    public static class IMonitorExtensions
    {
        public static void AssertRaise<T>(this IMonitor<T> monitor, bool expectedRaise, string eventName)
        {
            if (expectedRaise)
                monitor.Should().Raise(eventName);
            else
                monitor.Should().NotRaise(eventName);
        }
    }
}