﻿using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableOneTimeEventBuilder
    {
        private ScriptableOneTimeEvent Build()
        {
            var scriptable = ScriptableObject.CreateInstance<ScriptableOneTimeEvent>();
            return scriptable;
        }

        public static implicit operator ScriptableOneTimeEvent(ScriptableOneTimeEventBuilder builder) =>
            builder.Build();
    }
}