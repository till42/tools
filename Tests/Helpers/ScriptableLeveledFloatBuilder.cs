using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEditor;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableLeveledFloatBuilder
    {
        private float[] _values;

        public ScriptableLeveledFloatBuilder WithValues(float[] values)
        {
            _values = values;
            return this;
        }

        public ScriptableLeveledFloat Build()
        {
            var scriptable = ScriptableObject.CreateInstance<ScriptableLeveledFloat>();

            if (_values != null)
                scriptable.SetPrivate(s => s.Values, _values);

            return scriptable;
        }

        public static implicit operator ScriptableLeveledFloat(ScriptableLeveledFloatBuilder builder) =>
            builder.Build();
    }
}