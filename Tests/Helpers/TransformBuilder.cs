using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class TransformBuilder
    {
        private int _childCount;
        public Transform _parent;

        public TransformBuilder AsParentOf(Transform parent)
        {
            _parent = parent;
            return this;
        }

        public TransformBuilder WithChildcount(int childCount)
        {
            _childCount = childCount;
            return this;
        }


        public Transform Build()
        {
            var gameObject = new GameObject();
            if (_parent != null)
                gameObject.transform.SetParent(_parent);

            var transform = gameObject.transform;

            for (int i = 0; i < _childCount; i++)
            {
                Transform child = A.Transform.AsParentOf(transform);
            }

            return transform;
        }

        public static implicit operator Transform(TransformBuilder builder) => builder.Build();
    }
}