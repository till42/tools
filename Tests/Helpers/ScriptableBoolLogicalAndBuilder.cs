﻿using System.Collections.Generic;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableBoolLogicalAndBuilder
    {
        private List<InputBool> _inputBools = new List<InputBool>();

        public ScriptableBoolLogicalAndBuilder WithInputBools(InputBool[] inputBools)
        {
            _inputBools.AddRange(inputBools);
            return this;
        }

        private ScriptableBoolLogicalAnd Build()
        {
            var scriptable = ScriptableObject.CreateInstance<ScriptableBoolLogicalAnd>();
            scriptable.SetValue("_inputBools", _inputBools.ToArray());
            scriptable.Reset();
            return scriptable;
        }

        public static implicit operator ScriptableBoolLogicalAnd(ScriptableBoolLogicalAndBuilder builder) =>
            builder.Build();
    }
}