using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableIntConditionLessThanBuilder
    {
        private int _threshold;

        public ScriptableIntConditionLessThanBuilder WithThreshold(int threshold)
        {
            _threshold = threshold;
            return this;
        }


        public ScriptableIntConditionLessThan Build()
        {
            var condition = ScriptableObject.CreateInstance<ScriptableIntConditionLessThan>();

            condition.SetPrivate(c => c.Threshold, _threshold);

            return condition;
        }

        public static implicit operator ScriptableIntConditionLessThan(
            ScriptableIntConditionLessThanBuilder builder) =>
            builder.Build();
    }
}