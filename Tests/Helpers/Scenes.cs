using System.IO;
using System.Linq;
using System.Diagnostics;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FantasyArts.Tools.Tests.Helpers
{
    public static class Scenes
    {
        public static string Find(string sceneName)
        {
            var path = TestDirectory;
            var scenesGUIDs = AssetDatabase.FindAssets($"{sceneName} t:Scene", new[] {path});
            var scenesPaths = scenesGUIDs.Select(AssetDatabase.GUIDToAssetPath).ToList();
            if (scenesPaths.Count <= 0) return default;
            var scenePath = scenesPaths[0];
            return scenePath;
        }

        public static AsyncOperation Open(string sceneName)
        {
            var scenePath = Find(sceneName);
            if (string.IsNullOrEmpty(scenePath)) return default;


            return
                EditorSceneManager.LoadSceneAsyncInPlayMode(scenePath, new LoadSceneParameters(LoadSceneMode.Single));


            //EditorSceneManager.OpenScene("Assets/Scene2.unity");
            // EditorSceneManager.OpenScene(scenePath);
            // EditorSceneManager.LoadSceneInPlayMode(scenePath, new LoadSceneParameters(LoadSceneMode.Single));
        }

        public static string TestDirectory
        {
            get
            {
                var absolutePath = TestDirectoryAbsolutePath;
                var dataPath = Application.dataPath;
                var dataParent = Directory.GetParent(dataPath).FullName;
                var relativePath = absolutePath.Replace(dataParent, "");
                relativePath = relativePath.TrimStart('\\');
                return relativePath;
            }
        }

        private static string TestDirectoryAbsolutePath
        {
            get
            {
                var filename = GetScriptFileName();
                if (string.IsNullOrEmpty(filename))
                    return "";
                var directory = Path.GetDirectoryName(filename);
                var testDirectory = Directory.GetParent(directory);
                return testDirectory.FullName;
            }
        }

        private static string GetScriptFileName()
        {
            var stackTrace = new StackTrace(true);
            var frame = stackTrace.GetFrame(0);
            var fileName = frame.GetFileName();
            return fileName;
        }
    }
}