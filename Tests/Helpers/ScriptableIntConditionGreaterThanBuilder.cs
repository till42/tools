using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableIntConditionGreaterThanBuilder
    {
        private int _threshold;

        public ScriptableIntConditionGreaterThanBuilder WithThreshold(int threshold)
        {
            _threshold = threshold;
            return this;
        }


        public ScriptableIntConditionGreaterThan Build()
        {
            var condition = ScriptableObject.CreateInstance<ScriptableIntConditionGreaterThan>();

            condition.SetPrivate(c => c.Threshold, _threshold);

            return condition;
        }

        public static implicit operator ScriptableIntConditionGreaterThan(
            ScriptableIntConditionGreaterThanBuilder builder) =>
            builder.Build();
    }
}