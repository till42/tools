namespace FantasyArts.Tools.Tests.Helpers
{
    public static class A
    {
        public static HitpointBuilder Hitpoint => new HitpointBuilder();

        public static TransformBuilder Transform => new TransformBuilder();
      

        public static ScriptableIntBuilder ScriptableInt => new ScriptableIntBuilder();
        public static ScriptableStringBuilder ScriptableString => new ScriptableStringBuilder();

        public static ScriptableBoolBuilder ScriptableBool => new ScriptableBoolBuilder();
        public static ScriptableFloatBuilder ScriptableFloat => new ScriptableFloatBuilder();
        public static ScriptableEventBuilder ScriptableEvent => new ScriptableEventBuilder();
        public static ScriptableOneTimeEventBuilder ScriptableOneTimeEvent => new ScriptableOneTimeEventBuilder();
        public static ScriptableEventIntBuilder ScriptableEventInt => new ScriptableEventIntBuilder();
        public static ScriptableEventListenerBuilder ScriptableEventListener => new ScriptableEventListenerBuilder();
        public static ScriptableIntListenerBuilder ScriptableIntListener => new ScriptableIntListenerBuilder();

        public static ScriptableBoolLogicalAndBuilder ScriptableBoolLogicalAnd => new ScriptableBoolLogicalAndBuilder();

        public static ScriptableLeveledFloatBuilder ScriptableLeveledFloat => new ScriptableLeveledFloatBuilder();
        public static ScriptableLeveledIntBuilder ScriptableLeveledInt => new ScriptableLeveledIntBuilder();
        public static ScriptableLeveledBoolBuilder ScriptableLeveledBool => new ScriptableLeveledBoolBuilder();

        public static ScriptableIntConditionGreaterThanBuilder ScriptableIntConditionGreaterThan =>
            new ScriptableIntConditionGreaterThanBuilder();

        public static ScriptableIntConditionLessThanBuilder ScriptableIntConditionLessThan =>
            new ScriptableIntConditionLessThanBuilder();

        public static StateBuilder State => new StateBuilder();
        public static StateMachineBuilder StateMachine => new StateMachineBuilder();
        public static StateMachineScriptableBuilder StateMachineScriptable => new StateMachineScriptableBuilder();
    }
}