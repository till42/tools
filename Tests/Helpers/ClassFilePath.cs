using System.Diagnostics;
using System.IO;

namespace FantasyArts.Tools.Tests.Helpers
{
    public static class ClassFilePath
    {
        public static string GetPath() =>
            // get the calling frame with index=2. (index=0,1 would always be THIS script: ScriptFile.cs)
            GetPath(2);

        public static string GetPath(int indexModifier)
        {
            var trace = new StackTrace(true);

            var frame = trace.GetFrame(indexModifier);

            return frame.GetFileName();
        }

        public static string GetFileInSubfolder(string subfolder, string filename)
        {
            // get the calling frame with index=2. (index=0,1 would always be THIS script: ScriptFile.cs)
            var path = GetPath(2);
            var directoryName = Path.GetDirectoryName(path);
            return Path.Combine(directoryName, subfolder, filename);
        }
    }
}