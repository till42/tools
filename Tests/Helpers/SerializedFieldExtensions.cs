using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using Object = UnityEngine.Object;

namespace FantasyArts.Tools.Tests.Helpers
{
    public static class SerializedFieldExtensions
    {
        public static T GetValue<T>(this Object instance, string propertyName)
        {
            var property = instance.GetProperty(propertyName);
            return property.GetValue<T>();
        }

        public static void SetValue<T>(this Object instance, string fieldName, T value)
        {
            var property = instance.GetProperty(fieldName);
            property.SetValue(value);
        }

        private static SerializedProperty GetProperty(this Object instance, string fieldName)
        {
            var serializedObject = new SerializedObject(instance);
            serializedObject.Update();
            var property = serializedObject.FindProperty(fieldName);
            return property;
        }

        private static T GetValue<T>(this SerializedProperty property)
            => GetNestedObject<T>(property.propertyPath, GetSerializedPropertyRootComponent(property), true);

        private static void SetValue<T>(this SerializedProperty property, T value)
        {
            object obj = GetSerializedPropertyRootComponent(property);
            var fieldStructure = property.propertyPath.Split('.');
            for (var i = 0; i < fieldStructure.Length - 1; i++)
            {
                obj = GetFieldOrPropertyValue<object>(fieldStructure[i], obj);
            }

            var fieldName = fieldStructure.Last();
            SetFieldOrPropertyValue(fieldName, obj, value, true);
        }

        private static Object GetSerializedPropertyRootComponent(SerializedProperty property)
            => property.serializedObject.targetObject;

        private static T GetNestedObject<T>(string path, object obj, bool includeAllBases = false)
        {
            foreach (var part in path.Split('.'))
            {
                obj = GetFieldOrPropertyValue<object>(part, obj, includeAllBases);
            }

            return (T) obj;
        }

        private static T GetFieldOrPropertyValue<T>(string fieldName, object obj, bool includeAllBases = false)
        {
            var bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public |
                           BindingFlags.NonPublic;
            var field = obj.GetField(fieldName);
            if (field != null) return (T) field.GetValue(obj);

            var property = obj.GetType().GetProperty(fieldName, bindings);
            if (property != null) return (T) property.GetValue(obj, null);

            if (!includeAllBases) return default;

            foreach (var type in GetBaseClassesAndInterfaces(obj.GetType()))
            {
                field = type.GetField(fieldName, bindings);
                if (field != null) return (T) field.GetValue(obj);

                property = type.GetProperty(fieldName, bindings);
                if (property != null) return (T) property.GetValue(obj, null);
            }

            return default;
        }

        private static void SetFieldOrPropertyValue(string fieldName, object obj, object value,
            bool includeAllBases = false)
        {
            var bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public |
                           BindingFlags.NonPublic;
            var field = obj.GetField(fieldName);
            if (field != null)
            {
                field.SetValue(obj, value);
                return;
            }

            var property = obj.GetProperty(fieldName);
            if (property != null)
            {
                property.SetValue(obj, value, null);
                return;
            }

            if (!includeAllBases) return;
            foreach (var type in GetBaseClassesAndInterfaces(obj.GetType()))
            {
                field = type.GetField(fieldName, bindings);
                if (field != null)
                {
                    field.SetValue(obj, value);
                    return;
                }

                property = type.GetProperty(fieldName, bindings);
                if (property == null) continue;

                property.SetValue(obj, value, null);
                return;
            }
        }

        private static FieldInfo GetField(this object obj, string fieldName)
        {
            const BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public |
                                          BindingFlags.NonPublic | BindingFlags.FlattenHierarchy;

            var field = obj.GetType().GetField(fieldName, bindings);
            // if (field == null)
            //     field = obj.GetType().BaseType.GetField(fieldName, bindings);
            return field;
        }

        private static PropertyInfo GetProperty(this object obj, string fieldName)
        {
            const BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public |
                                          BindingFlags.NonPublic | BindingFlags.FlattenHierarchy;

            var property = obj.GetType().GetProperty(fieldName, bindings);
            // if (property == null)
            //     property = obj.GetType().BaseType.GetProperty(fieldName, bindings);
            return property;
        }

        private static IEnumerable<Type> GetBaseClassesAndInterfaces(this Type type, bool includeSelf = false)
        {
            var allTypes = new List<Type>();

            if (includeSelf)
                allTypes.Add(type);

            if (type.BaseType == typeof(object))
            {
                allTypes.AddRange(type.GetInterfaces());
            }
            else
            {
                allTypes.AddRange(
                    Enumerable
                        .Repeat(type.BaseType, 1)
                        .Concat(type.GetInterfaces())
                        .Concat(type.BaseType.GetBaseClassesAndInterfaces())
                        .Distinct());
            }

            return allTypes;
        }
    }
}