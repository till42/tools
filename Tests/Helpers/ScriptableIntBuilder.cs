using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEditor;
using UnityEngine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class ScriptableIntBuilder
    {
        private int _value;
        private int _minValue = int.MinValue;

        public ScriptableIntBuilder WithValue(int value)
        {
            _value = value;
            return this;
        }

        public ScriptableIntBuilder WithMinValue(int minValue)
        {
            _minValue = minValue;
            return this;
        }

        public ScriptableInt Build()
        {
            var scriptable = ScriptableObject.CreateInstance<ScriptableInt>();
            var serializedObject = new SerializedObject(scriptable);

            var propertyMinValue = serializedObject.FindProperty("_minValue");
            propertyMinValue.intValue = _minValue;

            var propertyValue = serializedObject.FindProperty("_value");
            propertyValue.intValue = _value;

            serializedObject.ApplyModifiedPropertiesWithoutUndo();
            return scriptable;
        }

        public static implicit operator ScriptableInt(ScriptableIntBuilder builder) => builder.Build();
    }
}