﻿using System.Collections.Generic;
using FantasyArts.Tools.Runtime.Architecture.StateMachine;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class StateBuilder
    {
        private string _name;
        private Dictionary<string, State> _transitions = new Dictionary<string, State>();

        public StateBuilder WithName(string name)
        {
            _name = name;
            return this;
        }

        public StateBuilder WithTransition(State toState, string trigger)
        {
            _transitions.Add(trigger, toState);
            return this;
        }

        public State Build()
        {
            var state = new State(_name);
            foreach (var keyValuePair in _transitions)
            {
                state.AddTransitionTo(keyValuePair.Value, keyValuePair.Key);
            }

            return state;
        }

        public static implicit operator State(StateBuilder builder) => builder.Build();
    }
}