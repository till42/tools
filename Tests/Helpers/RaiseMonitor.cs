using System;

namespace FantasyArts.Tools.Tests.Helpers
{
    public class RaiseMonitor
    {
        public int RaiseCount { get; private set; }

        public bool WasRaised => RaiseCount > 0;

        public void RaiseAction() => RaiseCount++;

        public void Clear() => RaiseCount = 0;
    }
}