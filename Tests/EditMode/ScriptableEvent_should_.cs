using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableEvent_should_
    {
        [Test]
        public void be_created()
        {
            ScriptableEvent scriptableEvent = A.ScriptableEvent;
            scriptableEvent.Should().NotBeNull();
        }

        [Test]
        public void invoke_registered_action_on_raise()
        {
            var wasCalled = false;
            void RaiseAction() => wasCalled = true;
            ScriptableEvent scriptableEvent = A.ScriptableEvent;

            scriptableEvent.RegisterRaiseAction(RaiseAction);
            scriptableEvent.Raise();

            wasCalled.Should().BeTrue();
        }

        [Test]
        public void Not_Invoke_UnRegistered_Action_On_Raise()
        {
            var wasCalled = false;
            void RaiseAction() => wasCalled = true;
            ScriptableEvent scriptableEvent = A.ScriptableEvent;

            scriptableEvent.RegisterRaiseAction(RaiseAction);
            scriptableEvent.UnRegisterRaiseAction(RaiseAction);
            scriptableEvent.Raise();

            wasCalled.Should().BeFalse();
        }

        [Test]
        public void invoke_UnityEvent_on_raise()
        {
            var wasCalled = false;
            void RaiseAction() => wasCalled = true;
            ScriptableEvent scriptableEvent = A.ScriptableEvent;

            scriptableEvent.RaisedEvent.AddListener(RaiseAction);
            scriptableEvent.Raise();

            wasCalled.Should().BeTrue();
        }
    }
}