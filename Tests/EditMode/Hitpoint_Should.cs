using FantasyArts.Tools.Runtime.Game;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class Hitpoint_Should
    {
        [Test]
        public void Be_Created()
        {
            Hitpoint hitpoint = A.Hitpoint;

            hitpoint.Should().NotBeNull();
        }

        [Test]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(5, ExpectedResult = 5)]
        [TestCase(15, ExpectedResult = 15)]
        [TestCase(-1, ExpectedResult = 0)]
        [TestCase(-15, ExpectedResult = 0)]
        public int Have_Initial_MaxValue(int maxValue)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);

            return hitpoint.MaxValue;
        }

        [Test]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(5, ExpectedResult = 5)]
        [TestCase(15, ExpectedResult = 15)]
        [TestCase(-1, ExpectedResult = 0)]
        [TestCase(-15, ExpectedResult = 0)]
        public int Have_Initial_CurrentValue(int maxValue)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);

            return hitpoint.CurrentValue;
        }

        [Test]
        [TestCase(0, 0, ExpectedResult = 0)]
        [TestCase(5, 3, ExpectedResult = 5)]
        [TestCase(15, 10, ExpectedResult = 15)]
        [TestCase(-1, -1, ExpectedResult = 0)]
        [TestCase(-15, -2, ExpectedResult = 0)]
        [TestCase(15, -2, ExpectedResult = 15)]
        public int When_Reset_Have_Correct_Value(int maxValue, int previousValue)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);
            hitpoint.SetValue(previousValue);

            hitpoint.Reset();

            return hitpoint.CurrentValue;
        }

        [Test]
        [TestCase(0, 0, false)]
        [TestCase(5, 3, true)]
        [TestCase(15, 10, true)]
        [TestCase(-1, -1, false)]
        [TestCase(-15, -2, false)]
        [TestCase(15, -2, true)]
        public void When_Reset_Raise_Event(int maxValue, int previousValue, bool expectedUpdateEvent)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);
            hitpoint.SetValue(previousValue);

            using var monitor = hitpoint.Monitor();
            hitpoint.Reset();

            monitor.AssertRaise(expectedUpdateEvent, "OnUpdate");
        }


        [Test]
        [TestCase(0, 5, ExpectedResult = 0)]
        [TestCase(5, 5, ExpectedResult = 0)]
        [TestCase(15, 14, ExpectedResult = 1)]
        [TestCase(15, 17, ExpectedResult = 0)]
        [TestCase(-15, 5, ExpectedResult = 0)]
        [TestCase(15, -5, ExpectedResult = 15)]
        public int When_TakeDamage_Have_Correct_Value(int maxValue, int damageAmount)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);

            hitpoint.TakeDamage(damageAmount);

            return hitpoint.CurrentValue;
        }

        [Test]
        [TestCase(0, 5, false)]
        [TestCase(5, 5, true)]
        [TestCase(15, 14, true)]
        [TestCase(15, 17, true)]
        [TestCase(-15, 5, false)]
        [TestCase(15, -5, false)]
        public void When_TakeDamage_Raise_Update_Event(int maxValue, int damageAmount, bool expectedUpdateEvent)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);
            using var monitor = hitpoint.Monitor();

            hitpoint.TakeDamage(damageAmount);

            monitor.AssertRaise(expectedUpdateEvent, "OnUpdate");
        }

        [Test]
        [TestCase(0, 5, false)]
        [TestCase(5, 5, true)]
        [TestCase(15, 14, false)]
        [TestCase(15, 17, true)]
        [TestCase(-15, 5, false)]
        [TestCase(15, -5, false)]
        public void When_Take_Damage_Raise_Depleted_Event(int maxValue, int damageAmount, bool expectedUpdateEvent)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);
            using var monitor = hitpoint.Monitor();

            hitpoint.TakeDamage(damageAmount);

            monitor.AssertRaise(expectedUpdateEvent, "OnDepleted");
        }

        [Test]
        [TestCase(0, 5, ExpectedResult = 0)]
        [TestCase(5, 5, ExpectedResult = 5)]
        [TestCase(15, 14, ExpectedResult = 14)]
        [TestCase(15, 17, ExpectedResult = 15)]
        [TestCase(-15, 5, ExpectedResult = 0)]
        [TestCase(15, -5, ExpectedResult = 0)]
        public int When_SetValue_Have_Correct_Value(int maxValue, int newValue)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);

            hitpoint.SetValue(newValue);

            return hitpoint.CurrentValue;
        }

        [Test]
        [TestCase(15, -5, true)]
        [TestCase(15, 15, false)]
        public void When_SetValue_Raise_Update_Event(int maxValue, int newValue, bool expectedUpdateEvent)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);
            using var monitor = hitpoint.Monitor();

            hitpoint.SetValue(newValue);

            monitor.AssertRaise(expectedUpdateEvent, "OnUpdate");
        }

        [Test]
        [TestCase(0, 5, false)]
        [TestCase(5, 5, false)]
        [TestCase(15, 14, false)]
        [TestCase(15, 0, true)]
        [TestCase(-15, 5, false)]
        [TestCase(15, -5, true)]
        public void When_SetValue_Raise_Depleted_Event(int maxValue, int newValue, bool expectedUpdateEvent)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);
            using var monitor = hitpoint.Monitor();

            hitpoint.SetValue(newValue);

            monitor.AssertRaise(expectedUpdateEvent, "OnDepleted");
        }

        [Test]
        [TestCase(0, 5, 5, ExpectedResult = 0)]
        [TestCase(5, 1, 4, ExpectedResult = 5)]
        [TestCase(15, 14, 4, ExpectedResult = 15)]
        [TestCase(15, 17, 5, ExpectedResult = 15)]
        [TestCase(-15, 5, 4, ExpectedResult = 0)]
        [TestCase(15, 1, 15, ExpectedResult = 15)]
        [TestCase(15, 10, -5, ExpectedResult = 10)]
        public int When_Heal_Have_Correct_Value(int maxValue, int previousAmount, int healAmount)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);
            hitpoint.SetValue(previousAmount);

            hitpoint.HealBy(healAmount);

            return hitpoint.CurrentValue;
        }

        [Test]
        [TestCase(0, 5, 5, false)]
        [TestCase(5, 1, 4, true)]
        [TestCase(15, 14, 4, true)]
        [TestCase(15, 17, 5, false)]
        [TestCase(-15, 5, 4, false)]
        [TestCase(15, 1, 15, true)]
        [TestCase(15, 10, -5, false)]
        public void When_Heal_Raise_Update_Event(int maxValue, int previousAmount, int healAmount,
            bool expectedUpdateEvent)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);
            hitpoint.SetValue(previousAmount);
            using var monitor = hitpoint.Monitor();

            hitpoint.HealBy(healAmount);

            monitor.AssertRaise(expectedUpdateEvent, "OnUpdate");
        }

        [Test]
        [TestCase(0, ExpectedResult = 0f)]
        [TestCase(1, ExpectedResult = 1f)]
        [TestCase(15, ExpectedResult = 1f)]
        [TestCase(-1, ExpectedResult = 0f)]
        [TestCase(-15, ExpectedResult = 0f)]
        public float Have_Initial_Percentage(int maxValue)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);

            return hitpoint.Percentage;
        }

        [Test]
        [TestCase(0, 0, ExpectedResult = 0f)]
        [TestCase(0, 1, ExpectedResult = 0f)]
        [TestCase(1, 0, ExpectedResult = 0f)]
        [TestCase(-1, 0, ExpectedResult = 0f)]
        [TestCase(1, 2, ExpectedResult = 0.5f)]
        [TestCase(10, 40, ExpectedResult = 0.25f)]
        public float Have_Correct_Percentage(int value, int maxValue)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);
            hitpoint.SetValue(value);

            return hitpoint.Percentage;
        }

        [Test]
        [TestCase(0, 0, ExpectedResult = true)]
        [TestCase(0, 1, ExpectedResult = true)]
        [TestCase(1, 0, ExpectedResult = true)]
        [TestCase(-1, 0, ExpectedResult = true)]
        [TestCase(1, 2, ExpectedResult = false)]
        [TestCase(10, 40, ExpectedResult = false)]
        public bool Have_Correct_IsDepleted(int value, int maxValue)
        {
            Hitpoint hitpoint = A.Hitpoint.WithMaxValue(maxValue);
            hitpoint.SetValue(value);

            return hitpoint.IsDepleted;
        }
    }
}