using System.IO;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ClassFile_should_
    {
        [Test]
        public void _return_filename()
        {
            var path = ClassFilePath.GetPath();

            var fileName = Path.GetFileName(path);
            fileName.Should().Be("ClassFile_should_.cs");
        }

        [Test]
        [TestCase("TestFiles", "Text.txt")]
        public void _return_file_in_subfolder(string subfolder, string filename)
        {
            var path = ClassFilePath.GetFileInSubfolder(subfolder, filename);

            path.Should().Contain(subfolder);
            Path.GetFileName(path).Should().Be(filename);
        }

        [Test]
        [TestCase("TestFiles", "Text.txt", ExpectedResult = true)]
        [TestCase("TestFiles", "doesNotExist.xxx", ExpectedResult = false)]
        [TestCase("DoesNotExist", "Text.txt", ExpectedResult = false)]
        public bool _find_file(string subfolder, string filename)
        {
            var path = ClassFilePath.GetFileInSubfolder(subfolder, filename);
            return File.Exists(path);
        }
    }
}