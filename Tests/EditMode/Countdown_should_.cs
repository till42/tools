using FantasyArts.Tools.Runtime.Game;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class Countdown_should_
    {
        [Test]
        public void _be_created()
        {
            var countdown = new Countdown(0f);
            countdown.Should().NotBeNull();
        }

        [Test]
        [TestCase(30f)]
        [TestCase(1f)]
        [TestCase(0.5f)]
        public void _be_created_with_initial_seconds(float seconds)
        {
            var countdown = new Countdown(seconds);
            countdown.Remaining.Should().Be(seconds);
        }
    }
}