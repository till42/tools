using FantasyArts.Tools.Runtime.Architecture.Extensions;
using FluentAssertions;
using NUnit.Framework;


namespace FantasyArts.Tools.Tests.EditMode
{
    public class ArrayExtensionsShould
    {
        [Test]
        public void Pick_RandomElement()
        {
            var intArray = new int[] {0, 1};

            var numberOf = new int[intArray.Length];

            for (int j = 0; j < 1000; j++)
            {
                var i = intArray.PickRandomItem();
                numberOf[i]++;
            }

            for (int j = 0; j < numberOf.Length; j++)
            {
                numberOf[j].Should().NotBe(0);
            }
        }

        [Test]
        [TestCase(0, 'A', 'B', 'C', 'D', 'E', ExpectedResult = 'A')]
        [TestCase(2, 'A', 'B', 'C', 'D', 'E', ExpectedResult = 'C')]
        [TestCase(4, 'A', 'B', 'C', 'D', 'E', ExpectedResult = 'E')]
        [TestCase(6, 'A', 'B', 'C', 'D', 'E', ExpectedResult = 'B')]
        [TestCase(8, 'A', 'B', 'C', 'D', 'E', ExpectedResult = 'D')]
        [TestCase(8, ExpectedResult = default(char))]
        public char ReturnRollingElement(int index, params char[] array)
        {
            return array.GetRollingElementAt(index);
        }
    }
}