﻿using System.Text.RegularExpressions;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using static UnityEngine.LogType;
using static UnityEngine.TestTools.LogAssert;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableBoolLogicalAnd_should_
    {
        private ScriptableBoolLogicalAnd _sut;

        [SetUp]
        public void _setup()
        {
            _sut = A.ScriptableBoolLogicalAnd;
        }

        [Test]
        public void _be_created()
        {
            _sut.Should().NotBeNull();
        }

        [Test]
        public void _not_be_able_to_set_value()
        {
            _sut.Value = true;
            Expect(Error, new Regex("set"));
        }

        [Test]
        public void _set_inputBools()
        {
            var inputBools = new InputBool[] { An.InputBool, An.InputBool };
            _sut.SetValue("_inputBools", inputBools);
            var result = _sut.GetValue<InputBool[]>("_inputBools");
            result.Should().Equal(inputBools);
        }

        [Test]
        [TestCase(true, ExpectedResult = true)]
        [TestCase(true, false, ExpectedResult = false)]
        [TestCase(true, true, ExpectedResult = true)]
        [TestCase(true, true, false, ExpectedResult = false)]
        [TestCase(true, true, true, ExpectedResult = true)]
        public bool _calculate_And_Operation(params bool[] values)
        {
            var inputBools = values.CreateInputBools();

            _sut = A.ScriptableBoolLogicalAnd.WithInputBools(inputBools);

            return _sut.Value;
        }

        [Test]
        [TestCase(true, ExpectedResult = false)]
        [TestCase(false, ExpectedResult = true)]
        [TestCase(true, false, ExpectedResult = false)]
        [TestCase(true, true, ExpectedResult = false)]
        [TestCase(true, true, false, ExpectedResult = false)]
        [TestCase(true, true, true, ExpectedResult = false)]
        [TestCase(false, false, false, ExpectedResult = true)]
        public bool _calculate_logicalAnd_negated(params bool[] values)
        {
            var logicalBools = values.CreateInputBools(true);
            _sut = A.ScriptableBoolLogicalAnd.WithInputBools(logicalBools);

            return _sut.Value;
        }

        [Test]
        [TestCase(false, ExpectedResult = true)]
        [TestCase(true, true, ExpectedResult = false)]
        [TestCase(false, true, ExpectedResult = true)]
        [TestCase(true, true, false, ExpectedResult = false)]
        [TestCase(true, true, true, ExpectedResult = false)]
        [TestCase(false, true, true, ExpectedResult = true)]
        public bool _change_result_when_first_input_bools_are_changed(params bool[] values)
        {
            var inputBools = values.CreateInputBools();
            _sut = A.ScriptableBoolLogicalAnd.WithInputBools(inputBools);
            var firstInput = inputBools[0].ScriptableBool;
            firstInput.Value = !firstInput.Value;

            return _sut.Value;
        }

        [Test]
        [TestCase(false, ExpectedResult = true)]
        [TestCase(true, true, ExpectedResult = false)]
        [TestCase(false, true, ExpectedResult = true)]
        [TestCase(true, true, false, ExpectedResult = false)]
        [TestCase(true, true, true, ExpectedResult = false)]
        [TestCase(false, true, true, ExpectedResult = true)]
        public bool _raise_update_when_first_input_bools_are_changed(params bool[] values)
        {
            var inputBools = values.CreateInputBools();
            _sut = A.ScriptableBoolLogicalAnd.WithInputBools(inputBools);
            var wasRaised = false;
            var valueRaised = false;
            _sut.RegisterUpdateAction(b =>
            {
                wasRaised = true;
                valueRaised = b;
            });
            var firstInput = inputBools[0].ScriptableBool;

            firstInput.Value = !firstInput.Value;

            wasRaised.Should().BeTrue();
            return valueRaised;
        }
    }
}