﻿using FantasyArts.Tools.Runtime.Architecture.StateMachine;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class StateMachineShould
    {
        [Test]
        public void Be_Created()
        {
            StateMachine stateMachine = A.StateMachine;

            stateMachine.Should().NotBeNull();
        }

        [Test]
        public void Trigger_OnEnter_Of_FirstState()
        {
            StateMachine stateMachine = A.StateMachine;
            State state = A.State;
            var monitor = state.Monitor();

            stateMachine.Start(state);

            monitor.Should().Raise("OnEnter");
        }

        [Test]
        public void Trigger_OnUpdate_Of_FirstState()
        {
            State state = A.State;
            var monitor = state.Monitor();
            StateMachine stateMachine = A.StateMachine.StartedInState(state);

            stateMachine.Update();

            monitor.Should().Raise("OnUpdate");
        }

        [Test]
        public void NotTrigger_OnUpdate_When_NotUpdated()
        {
            State state = A.State;
            var monitor = state.Monitor();
            StateMachine stateMachine = A.StateMachine.StartedInState(state);

            //no update here

            monitor.Should().NotRaise("OnUpdate");
        }

        [Test]
        public void Trigger_OnExit_Of_First_State_And_OnEnter_Of_Second_State()
        {
            State state2 = A.State;
            State state1 = A.State.WithTransition(state2, "Trigger");
            var monitor1 = state1.Monitor();
            var monitor2 = state2.Monitor();
            StateMachine stateMachine = A.StateMachine.StartedInState(state1);

            stateMachine.SetTrigger("Trigger");

            monitor1.Should().Raise("OnExit");
            monitor2.Should().Raise("OnEnter");
        }

        [Test]
        public void React_To_MachineLevel_Conditional_Trigger()
        {
            State state2 = A.State;
            State state1 = A.State;
            var monitor1 = state1.Monitor();
            var monitor2 = state2.Monitor();
            StateMachine stateMachine = A.StateMachine.StartedInState(state1);
            stateMachine.AddTriggerFromAnyState(IsTrigger2, state2);

            stateMachine.Update();

            monitor1.Should().Raise("OnExit");
            monitor2.Should().Raise("OnEnter");

            bool IsTrigger2() => true;
        }
    }
}