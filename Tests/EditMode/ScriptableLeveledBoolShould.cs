using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableLeveledBoolShould
    {
        [Test]
        public void Be_Created()
        {
            ScriptableLeveledBool leveledBool = A.ScriptableLeveledBool;

            leveledBool.Should().NotBeNull();
        }

        [Test]
        [TestCase(int.MinValue, true, false, true, false, ExpectedResult = true)]
        [TestCase(-1, true, false, true, false, ExpectedResult = true)]
        [TestCase(0, true, false, true, false, ExpectedResult = true)]
        [TestCase(1, true, false, true, false, ExpectedResult = false)]
        [TestCase(2, true, false, true, false, ExpectedResult = true)]
        [TestCase(3, true, false, true, false, ExpectedResult = false)]
        [TestCase(15, true, false, true, false, ExpectedResult = false)]
        [TestCase(int.MaxValue, true, false, true, false, ExpectedResult = false)]
        [TestCase(0, ExpectedResult = default(bool))]
        [TestCase(1, ExpectedResult = default(bool))]
        public bool Return_Value_For_Level(int level, params bool[] values)
        {
            ScriptableLeveledBool leveledBool = A.ScriptableLeveledBool.WithValues(values);

            return leveledBool[level];
        }
    }
}