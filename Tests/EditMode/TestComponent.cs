﻿using System;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class HelperComponents
    {
        public class TestComponent : MonoBehaviour
        {
#pragma warning disable CS0414
            [SerializeField] private bool _testBool = true;
            [SerializeField] private int _testInt = 15;
            [SerializeField] private long _testLong = 555L;
            [SerializeField] private string _testString = "Test";
            [SerializeField] private float _testFloat = 1f;
            [SerializeField] private double _testDouble = 1d;
            [SerializeField] private Vector2 _testVector2 = Vector2.one;
            [SerializeField] private Vector3 _testVector3 = Vector3.one;
            [SerializeField] private Vector4 _testVector4 = Vector4.one;
            [SerializeField] private Vector2Int _testVector2Int = Vector2Int.one;
            [SerializeField] private Vector3Int _testVector3Int = Vector3Int.one;
            [SerializeField] private Quaternion _testQuaternion = Quaternion.Euler(90f, 90f, 90f);
            [SerializeField] private Color _testColor = Color.cyan;
            [SerializeField] private Rect _testRect = Rect.MinMaxRect(1, 1, 10, 10);
            [SerializeField] private RectInt _testRectInt;
            [SerializeField] private Bounds _testBounds;
            [SerializeField] private BoundsInt _testBoundsInt;
            [SerializeField] private AnimationCurve _testAnimationCurve = AnimationCurve.Constant(1f, 1f, 5f);
            [SerializeField] private TestEnum _testEnum;
            [SerializeField] private TestComponent _otherTestComponent;
            public TestComponent OtherTestComponent => _otherTestComponent;
            [SerializeField] private Transform _otherTransform;
            public Transform OtherTransform => _otherTransform;

            [SerializeField] private int[] _intArray;
            [SerializeField] private TestClass _testClass;
            [SerializeField] private TestClass[] _testClasses;


#pragma warning restore CS0414
        }

        public class TestComponentGrandChild : TestComponent { }

        public class TestComponentChild : TestComponent { }

        [Serializable]
        public class TestClass
        {
            public string String;
        }
    }
}