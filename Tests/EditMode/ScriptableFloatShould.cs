using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableFloatShould
    {
        [Test]
        public void Be_Created()
        {
            ScriptableFloat scriptableFloat = A.ScriptableFloat;
            scriptableFloat.Should().NotBeNull();
        }

        [Test]
        public void Have_Default_As_Initial_Value()
        {
            ScriptableFloat scriptableFloat = A.ScriptableFloat;
            scriptableFloat.Value.Should().Be(default);
        }

        [Test]
        [TestCase(5f, ExpectedResult = 5f)]
        [TestCase(0f, ExpectedResult = 0f)]
        [TestCase(0.12122132323524f, ExpectedResult = 0.12122132323524f)]
        [TestCase(-3243242340.12432535f, ExpectedResult = -3243242340.12432535f)]
        [TestCase(float.PositiveInfinity, ExpectedResult = float.PositiveInfinity)]
        [TestCase(float.NegativeInfinity, ExpectedResult = float.NegativeInfinity)]
        [TestCase(float.MinValue, ExpectedResult = float.MinValue)]
        [TestCase(float.MaxValue, ExpectedResult = float.MaxValue)]
        public float Have_Builder_Value(float value)
        {
            ScriptableFloat scriptableFloat = A.ScriptableFloat.WithValue(value);
            return scriptableFloat;
        }

        [Test]
        [TestCase(5f, ExpectedResult = 5f)]
        [TestCase(0f, ExpectedResult = 0f)]
        [TestCase(0.12122132323524f, ExpectedResult = 0.12122132323524f)]
        [TestCase(-3243242340.12432535f, ExpectedResult = -3243242340.12432535f)]
        [TestCase(float.PositiveInfinity, ExpectedResult = float.PositiveInfinity)]
        [TestCase(float.NegativeInfinity, ExpectedResult = float.NegativeInfinity)]
        [TestCase(float.MinValue, ExpectedResult = float.MinValue)]
        [TestCase(float.MaxValue, ExpectedResult = float.MaxValue)]
        public float Set_Value(float value)
        {
            ScriptableFloat scriptableFloat = A.ScriptableFloat;

            scriptableFloat.Value = value;

            return scriptableFloat;
        }

        [Test]
        [TestCase(5f, ExpectedResult = 5f)]
        [TestCase(0f, ExpectedResult = 0f)]
        [TestCase(0.12122132323524f, ExpectedResult = 0.12122132323524f)]
        [TestCase(-3243242340.12432535f, ExpectedResult = -3243242340.12432535f)]
        [TestCase(float.PositiveInfinity, ExpectedResult = float.PositiveInfinity)]
        [TestCase(float.NegativeInfinity, ExpectedResult = float.NegativeInfinity)]
        [TestCase(float.MinValue, ExpectedResult = float.MinValue)]
        [TestCase(float.MaxValue, ExpectedResult = float.MaxValue)]
        public float Call_Update_Event(float value)
        {
            ScriptableFloat scriptableFloat = A.ScriptableFloat;
            var eventValue = default(float);
            scriptableFloat.RegisterUpdateAction(f => eventValue = f);

            scriptableFloat.Value = value;

            return eventValue;
        }

        [Test]
        [TestCase(5f, ExpectedResult = 5f)]
        [TestCase(0f, ExpectedResult = 0f)]
        [TestCase(0.12122132323524f, ExpectedResult = 0.12122132323524f)]
        [TestCase(-3243242340.12432535f, ExpectedResult = -3243242340.12432535f)]
        [TestCase(float.PositiveInfinity, ExpectedResult = float.PositiveInfinity)]
        [TestCase(float.NegativeInfinity, ExpectedResult = float.NegativeInfinity)]
        [TestCase(float.MinValue, ExpectedResult = float.MinValue)]
        [TestCase(float.MaxValue, ExpectedResult = float.MaxValue)]
        public float Call_Update_Event_OnRegistration(float value)
        {
            ScriptableFloat scriptableFloat = A.ScriptableFloat;
            var eventValue = default(float);
            scriptableFloat.Value = value;
            scriptableFloat.RegisterUpdateAction(f => eventValue = f);

            return eventValue;
        }

        [Test]
        [TestCase(5f, ExpectedResult = default(float))]
        [TestCase(0f, ExpectedResult = default(float))]
        [TestCase(0.12122132323524f, ExpectedResult = default(float))]
        [TestCase(-3243242340.12432535f, ExpectedResult = default(float))]
        [TestCase(float.PositiveInfinity, ExpectedResult = default(float))]
        [TestCase(float.NegativeInfinity, ExpectedResult = default(float))]
        [TestCase(float.MinValue, ExpectedResult = default(float))]
        [TestCase(float.MaxValue, ExpectedResult = default(float))]
        public float Not_Call_Update_Event_After_Unregistration(float value)
        {
            ScriptableFloat scriptableFloat = A.ScriptableFloat;
            var eventValue = default(float);
            void UpdateAction(float i) => eventValue = i;

            scriptableFloat.RegisterUpdateAction(UpdateAction);
            scriptableFloat.UnregisterUpdateAction(UpdateAction);

            scriptableFloat.Value = value;

            return eventValue;
        }
    }
}