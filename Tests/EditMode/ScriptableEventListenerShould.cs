using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableEventListenerShould
    {
        [Test]
        public void Be_Created()
        {
            ScriptableEventListenBehaviour scriptableEventListenBehaviour = A.ScriptableEventListener;
            scriptableEventListenBehaviour.Should().NotBeNull();
        }

        [Test]
        public void Raise_Action_On_ScriptableEvent_Raise()
        {
            A.ScriptableEventListener.WithScriptableEvent(out var scriptableEvent)
                .WithRaiseActionMonitor(out var raisedAction).Build();

            scriptableEvent.Raise();

            raisedAction.WasRaised.Should().BeTrue();
        }

        [Test]
        public void Raise_UnityEvent_On_ScriptableEvent_Raise()
        {
            A.ScriptableEventListener.WithScriptableEvent(out var scriptableEvent)
                .WithRaiseUnityEventMonitor(out var raisedUnityEvent).Build();

            scriptableEvent.Raise();

            raisedUnityEvent.WasRaised.Should().BeTrue();
        }

        [Test]
        public void Not_Raise_UnityEvent_On_ScriptableEvent_After_Unsubscribed()
        {
            ScriptableEventListenBehaviour scriptableEventListenBehaviour = A.ScriptableEventListener
                .WithScriptableEvent(out var scriptableEvent)
                .WithRaiseUnityEventMonitor(out var raisedUnityEvent);
            scriptableEventListenBehaviour.OnEventRaised.RemoveListener(raisedUnityEvent.RaiseAction);

            scriptableEvent.Raise();

            raisedUnityEvent.WasRaised.Should().BeFalse();
        }
    }
}