using FantasyArts.Tools.Runtime.Architecture.StateMachine;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class TransitionToScriptable_should_ : MonoBehaviour
    {
        [Test]
        public void _be_created()
        {
            var transitionToScriptable = new TransitionToScriptable();
            transitionToScriptable.Should().NotBeNull();
        }

        [Test]
        public void _have_a_trigger_to_a_state()
        {
            var state = ScriptableObject.CreateInstance<StateScriptable>();
            var transitionToScriptable = new TransitionToScriptable();
            transitionToScriptable.SetPrivate(x => x.ToState, state);

            transitionToScriptable.ToState.Should().Be(state);
        }

        [Test]
        [TestCase("SomeTrigger")]
        public void _find_a_state_by_triggerName(string trigger)
        {
            var state = ScriptableObject.CreateInstance<StateScriptable>();
            var transitionToScriptable = new TransitionToScriptable();
            transitionToScriptable.SetPrivate(x => x.ToState, state);
            transitionToScriptable.SetPrivate(x => x.Trigger, trigger);

            transitionToScriptable.TryGetState(trigger, out var foundState).Should().BeTrue();
            foundState.Should().Be(state);
        }
    }
}