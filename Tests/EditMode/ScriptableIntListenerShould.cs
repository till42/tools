using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using JetBrains.Annotations;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableIntListenerShould
    {
        [Test]
        public void Be_Created()
        {
            ScriptableIntListenBehaviour scriptableIntListenBehaviour = A.ScriptableIntListener;
            scriptableIntListenBehaviour.Should().NotBeNull();
        }

        [Test]
        public void Raise_Action_On_ScriptableInt_Update()
        {
            A.ScriptableIntListener.WithScriptableInt(out var scriptableInt)
                .WithRaiseActionMonitor(out var raisedAction).Build();

            scriptableInt.Value++;

            raisedAction.WasRaised.Should().BeTrue();
        }

        [Test]
        public void Raise_UnityEvent_On_ScriptableInt_Update()
        {
            A.ScriptableIntListener.WithScriptableInt(out var scriptableInt)
                .WithRaiseUnityEventMonitor(out var raisedUnityEvent).Build();

            scriptableInt.Value++;

            raisedUnityEvent.WasRaised.Should().BeTrue();
        }

        [Test]
        public void Not_Raise_UnityEvent_On_ScriptableInt_Update_After_Unsubscribed()
        {
            ScriptableIntListenBehaviour scriptableIntListenBehaviour = A.ScriptableIntListener
                .WithScriptableInt(out var scriptableInt)
                .WithRaiseUnityEventMonitor(out var raisedUnityEvent);

            scriptableIntListenBehaviour.OnEventRaised.RemoveListener(raisedUnityEvent.RaiseAction);

            scriptableInt.Value++;

            raisedUnityEvent.RaiseCount.Should().Be(1);
        }

        [Test]
        public void Be_Created_With_Condition()
        {
            ScriptableIntListenBehaviour scriptableIntListenBehaviour = A.ScriptableIntListener.WithCondition(out var condition);

            scriptableIntListenBehaviour.Condition.Should().NotBeNull();
        }

        [Test]
        [TestCase(0, ExpectedResult = false)]
        [TestCase(1, ExpectedResult = true)]
        [TestCase(5, ExpectedResult = true)]
        [TestCase(-1, ExpectedResult = false)]
        public bool Raise_Event_By_Condition_Treshold_0(int value)
        {
            ScriptableIntListenBehaviour scriptableIntListenBehaviour = A.ScriptableIntListener
                .WithScriptableInt(out var scriptableInt)
                .WithRaiseUnityEventMonitor(out var raisedUnityEvent)
                .WithClearedRaiseCounts()
                .WithCondition(out var condition);

            scriptableInt.Value = value;

            return raisedUnityEvent.WasRaised;
        }


        [Test]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(1, ExpectedResult = 1)]
        [TestCase(2, ExpectedResult = 1)]
        [TestCase(5, ExpectedResult = 1)]
        public int Raise_Event_By_Condition_Only_Once(int iterations)
        {
            ScriptableIntListenBehaviour scriptableIntListenBehaviour = A.ScriptableIntListener
                .WithScriptableInt(out var scriptableInt)
                .WithRaiseUnityEventMonitor(out var raisedUnityEvent)
                .WithClearedRaiseCounts()
                .WithCondition(out var condition);

            for (int i = 0; i < iterations; i++)
            {
                scriptableInt.Value += 1;
            }

            return raisedUnityEvent.RaiseCount;
        }
    }
}