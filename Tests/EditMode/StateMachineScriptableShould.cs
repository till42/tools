using System.Linq;
using FantasyArts.Tools.Runtime.Architecture.StateMachine;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class StateMachineScriptableShould
    {
        [Test]
        public void Be_Created()
        {
            StateMachineScriptable stateMachine = A.StateMachineScriptable;
            stateMachine.Should().NotBeNull();
        }

        [Test]
        [TestCase(3)]
        [TestCase(6)]
        [TestCase(15)]
        public void Be_Created_With_X_Number_Of_States(int numberOfStates)
        {
            StateMachineScriptable stateMachine = A.StateMachineScriptable.WithStates(numberOfStates);
            stateMachine.States.Count.Should().Be(numberOfStates);
        }
    }
}