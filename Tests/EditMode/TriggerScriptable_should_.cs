using FantasyArts.Tools.Runtime.Architecture.StateMachine;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class TriggerScriptable_should_
    {
        [Test]
        public void _be_created()
        {
            var triggerScriptable = ScriptableObject.CreateInstance<TriggerScriptable>();
            triggerScriptable.Should().NotBeNull();
        }

        [Test]
        public void _not_throw_exception_when_checking_name()
        {
            TriggerScriptable triggerScriptable = default;
            triggerScriptable.HasName("SomeName").Should().BeFalse();
        }

        [Test]
        [TestCase("SomeName")]
        public void _compare_a_name(string name)
        {
            TriggerScriptable triggerScriptable = ScriptableObject.CreateInstance<TriggerScriptable>();
            triggerScriptable.SetPrivate(x => x.Name, name);
            triggerScriptable.HasName(name).Should().BeTrue();
        }
    }
}