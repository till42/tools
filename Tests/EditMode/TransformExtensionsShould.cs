using FantasyArts.Tools.Architecture;
using FantasyArts.Tools.Runtime.Architecture.Extensions;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class TransformExtensionsShould
    {
        [Test]
        public void Build_A_Valid_Transform()
        {
            Transform transform = A.Transform;

            transform.Should().NotBeNull();
        }

        [Test]
        public void Build_A_Valid_Transform_With_Childcount()
        {
            Transform transform = A.Transform.WithChildcount(5);

            transform.childCount.Should().Be(5);
        }

        [Test]
        [TestCase(0), TestCase(2), TestCase(5)]
        public void Delete_All_Children_For_A_Transform_With_Childcount(int childcount)
        {
            Transform transform = A.Transform.WithChildcount(childcount);

            transform.DestroyAllChildren();

            transform.childCount.Should().Be(0);
        }
    }
}