using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableLeveledFloatShould
    {
        [Test]
        public void Be_Created()
        {
            ScriptableLeveledFloat leveledFloat = A.ScriptableLeveledFloat;

            leveledFloat.Should().NotBeNull();
        }

        [Test]
        [TestCase(int.MinValue, 1f, 2f, 3f, ExpectedResult = 1f)]
        [TestCase(-1, 1f, 2f, 3f, ExpectedResult = 1f)]
        [TestCase(0, 1f, 2f, 3f, ExpectedResult = 1f)]
        [TestCase(1, 1f, 2f, 3f, ExpectedResult = 2f)]
        [TestCase(2, 1f, 2f, 3f, ExpectedResult = 3f)]
        [TestCase(3, 1f, 2f, 3f, ExpectedResult = 3f)]
        [TestCase(15, 1f, 2f, 3f, ExpectedResult = 3f)]
        [TestCase(int.MaxValue, 1f, 2f, 3f, ExpectedResult = 3f)]
        [TestCase(0, ExpectedResult = default(float))]
        [TestCase(1, ExpectedResult = default(float))]
        public float Return_Value_For_Level(int level, params float[] values)
        {
            ScriptableLeveledFloat leveledFloat = A.ScriptableLeveledFloat.WithValues(values);

            return leveledFloat[level];
        }
    }
}