using System;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using FluentAssertions.Events;
using NUnit.Framework;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableEventInt_should_
    {
        [Test]
        public void _be_created()
        {
            ScriptableEventInt scriptableEvent = A.ScriptableEventInt;
            scriptableEvent.Should().NotBeNull();
        }

        [Test]
        [TestCase(5)]
        public void _invoked_with_parameter(int param)
        {
            var invokedValue = int.MinValue;
            void RaiseAction(int value) => invokedValue = value;
            ScriptableEventInt scriptableEventInt = A.ScriptableEventInt;

            scriptableEventInt.RegisterRaiseAction(RaiseAction);
            scriptableEventInt.Raise(param);

            invokedValue.Should().Be(param);
        }

        [Test]
        [TestCase(5)]
        public void Not_Invoke_UnRegistered_Action_On_Raise(int param)
        {
            var invokedValue = default(int);
            void RaiseAction(int value) => invokedValue = value;
            ScriptableEventInt scriptableEventInt = A.ScriptableEventInt;

            scriptableEventInt.RegisterRaiseAction(RaiseAction);
            scriptableEventInt.UnRegisterRaiseAction(RaiseAction);
            scriptableEventInt.Raise(param);

            invokedValue.Should().Be(default);
        }
    }
}