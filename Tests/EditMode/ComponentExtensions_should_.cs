using FluentAssertions;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using static FantasyArts.Tools.Runtime.Architecture.Extensions.ComponentExtensions;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ComponentExtensions_should_
    {
        [SetUp]
        public void _setup()
        {
            var scene = SceneManager.GetActiveScene();
            var rootObjects = scene.GetRootGameObjects();
            for (int i = rootObjects.Length - 1; i >= 0; i--)
            {
                GameObject.DestroyImmediate(rootObjects[i]);
            }
        }

        [Test]
        public void _find_component_in_scene()
        {
            var gameObject = new GameObject("new GameObject", typeof(HelperComponents.TestComponent));
            var component = GetComponentInScene<HelperComponents.TestComponent>();
            component.Should().NotBeNull();
            component.gameObject.Should().BeEquivalentTo(gameObject);
        }

        [Test]
        public void _find_interface_in_scene()
        {
            var gameObject = new GameObject("new GameObject");
            var addedComponent = gameObject.AddComponent<TestComponentWithInterface>();

            var foundComponent = GetComponentInScene<ITestInterface>();

            foundComponent.Should().NotBeNull();
            foundComponent.Should().BeEquivalentTo(addedComponent);
        }

        [Test]
        public void _find_interfaces_in_scene()
        {
            var gameObject1 = new GameObject("GameObject 1");
            var addedComponent1 = gameObject1.AddComponent<TestComponentWithInterface>();
            var gameObject2 = new GameObject("GameObject 2");
            var addedComponent2 = gameObject2.AddComponent<TestComponentWithInterface>();
            var addedComponent3 = gameObject2.AddComponent<TestComponentWithInterface>();

            var foundComponents = GetComponentsInScene<ITestInterface>();

            foundComponents.Should().HaveCount(3);
            foundComponents.Should().Contain(addedComponent1);
            foundComponents.Should().Contain(addedComponent2);
            foundComponents.Should().Contain(addedComponent3);
        }

        private interface ITestInterface { }

        private class TestComponentWithInterface : MonoBehaviour, ITestInterface { }
    }
}