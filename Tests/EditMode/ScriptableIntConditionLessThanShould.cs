using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableIntConditionLessThanShould
    {
        [Test]
        public void Be_Created()
        {
            ScriptableIntConditionLessThan condition = A.ScriptableIntConditionLessThan;
            condition.Should().NotBeNull();
        }

        [Test]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(5, ExpectedResult = 5)]
        [TestCase(-5, ExpectedResult = -5)]
        public int Be_Created_WithThreshold(int threshold)
        {
            ScriptableIntConditionLessThan condition = A.ScriptableIntConditionLessThan.WithThreshold(threshold);
            return condition.Threshold;
        }

        [Test]
        [TestCase(0, 0, ExpectedResult = false)]
        [TestCase(0, 1, ExpectedResult = false)]
        [TestCase(5, 1, ExpectedResult = true)]
        [TestCase(5, 15, ExpectedResult = false)]
        [TestCase(5, 6, ExpectedResult = false)]
        [TestCase(5, 5, ExpectedResult = false)]
        public bool Be_Fulfilled(int threshold, int value)
        {
            ScriptableIntConditionLessThan condition = A.ScriptableIntConditionLessThan.WithThreshold(threshold);
            return condition.IsFulfilled(value);
        }
    }
}