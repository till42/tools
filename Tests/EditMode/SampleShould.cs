using System.Linq;
using FantasyArts.Tools.Runtime.Architecture.Extensions;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class SampleShould
    {
        [Test]
        public void Be_Created()
        {
            var array = new[] {0, 1, 2, 3, 4, 5};

            var sample = array.CreateSample();

            sample.Should().NotBeNull();
        }

        [Test]
        [TestCase(0, 1, 2, 3, 4, 5)]
        public void Draw_Without_Replacement(params int[] array)
        {
            var sample = array.CreateSample();
            var picks = new int[array.Length];

            for (int i = 0; i < array.Length; i++)
            {
                if (!sample.DrawWithoutReplacement(out var result))
                    Assert.Fail();
                picks[result]++;
            }

            picks.ToList().ForEach(p => p.Should().Be(1));
        }

        [Test]
        [TestCase(50, 0, 1, 2, 3, 4, 5)]
        public void Draw_Without_Replacement_Multiple_Times_Without_Loop(int iterations, params int[] array)
        {
            var sample = array.CreateSample();
            var picks = new int[array.Length];

            for (var i = 0; i < iterations; i++)
            {
                for (var j = 0; j < array.Length; j++)
                {
                    if (sample.DrawWithoutReplacement(out var result))
                        picks[result]++;
                }
            }

            picks.ToList().ForEach(p => p.Should().Be(1));
        }

        [Test]
        [TestCase(50, 0, 1, 2, 3, 4, 5)]
        public void Draw_Without_Replacement_Multiple_Times_With_Loop(int iterations, params int[] array)
        {
            var sample = array.CreateSample();
            var picks = new int[array.Length];

            for (var i = 0; i < iterations; i++)
            {
                for (var j = 0; j < array.Length; j++)
                {
                    if (sample.DrawWithoutReplacement(out var result, true))
                        picks[result]++;
                }
            }

            picks.ToList().ForEach(p => p.Should().Be(iterations));
        }
    }
}