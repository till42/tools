﻿using FantasyArts.Tools.Runtime.Architecture.StateMachine;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class StateShould
    {
        [Test]
        [TestCase("name1")]
        [TestCase("")]
        [TestCase(null)]
        public void Know_Its_Own_Name(string name)
        {
            State state = A.State.WithName(name);

            state.ToString().Should().Be(name);
        }


        [Test]
        public void Be_Able_To_Have_A_Transition()
        {
            State state2 = A.State;
            State state1 = A.State.WithTransition(state2, "Trigger");


            state1.Transitions.Should().Contain("Trigger", state2);
        }

        [Test]
        public void Be_Able_To_Have_A_Transition_With_Multiple_Triggers()
        {
            State state2 = A.State.WithName("state2");
            State state1 = A.State.WithName("state1");

            state1.AddTransitionTo(state2, "Trigger1", "Trigger2");

            state1.Transitions.Should().Contain("Trigger1", state2);
            state1.Transitions.Should().Contain("Trigger2", state2);
        }

        [Test]
        public void Have_A_Transition_String_With_Correct_Description()
        {
            State state2 = A.State.WithName("state2");
            State state1 = A.State.WithName("state1").WithTransition(state2, "Trigger");

            state1.GetTransitionStrings().Should().Contain("state1->[Trigger]->state2");
        }

        [Test]
        public void Raise_OnEnter_Event()
        {
            State state = A.State;

            var monitoredSubject = state.Monitor();
            state.Enter();
            monitoredSubject.Should().Raise("OnEnter");
        }

        [Test]
        public void Raise_OnUpdate_Event()
        {
            State state = A.State;

            var monitoredSubject = state.Monitor();
            state.Update();
            monitoredSubject.Should().Raise("OnUpdate");
        }

        [Test]
        public void Raise_OnExit_Event()
        {
            State state = A.State;

            var monitoredSubject = state.Monitor();
            state.Exit();
            monitoredSubject.Should().Raise("OnExit");
        }

        [Test]
        public void Find_Next_State_On_Trigger()
        {
            State state2 = A.State.WithName("state2");
            State state1 = A.State.WithName("state1").WithTransition(state2, "Trigger");

            state1.TryGetNextState("Trigger", out var nextState).Should().BeTrue();
            nextState.Should().Be(state2);
        }

        [Test]
        public void Not_Find_Next_State_If_Not_Defined()
        {
            State state2 = A.State.WithName("state2");
            State state1 = A.State.WithName("state1").WithTransition(state2, "Trigger");

            state1.TryGetNextState("NoTrigger", out var nextState).Should().BeFalse();
            nextState.Should().BeNull();
        }
    }
}