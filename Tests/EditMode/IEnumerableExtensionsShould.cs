using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using FantasyArts.Tools.Runtime.Architecture.Extensions;
using FluentAssertions;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class IEnumerableExtensionsShould
    {
        [Test]
        [TestCase(0, 1, 2, 3, 4, 5, 6, 7, 8)]
        public void Duplicated_Array_Should_Have_Same_Length(params int[] array)
        {
            // var list = new List<int> {0, 1, 2, 3, 4, 5};
            var duplicate = array.Duplicate();

            duplicate.Count().Should().Be(array.Length);
        }

        [Test]
        [TestCase(0, 1, 2, 3, 4, 5, 6, 7, 8)]
        public void Duplicated_Array_Should_Have_Same_Elements(params int[] array)
        {
            // var list = new List<int> {0, 1, 2, 3, 4, 5};
            var duplicate = array.Duplicate();

            for (var i = 0; i < duplicate.Count(); i++)
            {
                var duplicateElement = duplicate.ElementAt(i);
                var arrayElement = array[i];
                duplicateElement.Should().Be(arrayElement);
            }
        }

        [Test]
        [TestCase(0, 1, 2, 3, 4, 5, 6, 7, 8)]
        public void Duplicated_Array_Should_Have_Same_Elements_Even_If_Source_Changes(params int[] array)
        {
            var duplicate = array.Duplicate();
            var duplicate2 = array.Duplicate();

            array = new int[array.Length];

            for (var i = 0; i < duplicate.Count(); i++)
            {
                var duplicateElement = duplicate.ElementAt(i);
                var duplicateElement2 = duplicate2.ElementAt(i);
                duplicateElement.Should().Be(duplicateElement2);
            }
        }
    }
}