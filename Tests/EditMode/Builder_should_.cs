using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using static FantasyArts.Tools.Tests.EditMode.HelperComponents;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class Builder_should_
    {
        private TestComponent _testComponent;

        [Test]
        public void _be_created()
        {
            _testComponent = new Builder<TestComponent>();

            _testComponent.Should().NotBeNull();
        }
    }
}