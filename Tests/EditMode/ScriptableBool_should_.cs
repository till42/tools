using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableBool_should_
    {
        [Test]
        public void _be_created()
        {
            ScriptableBool scriptableBool = A.ScriptableBool;
            scriptableBool.Should().NotBeNull();
        }

        [Test]
        public void _have_default_as_initial_value()
        {
            ScriptableBool scriptableBool = A.ScriptableBool;
            scriptableBool.Value.Should().Be(default);
        }

        [Test]
        [TestCase(true, ExpectedResult = true)]
        [TestCase(false, ExpectedResult = false)]
        public bool _have_builder_value(bool value)
        {
            ScriptableBool scriptableBool = A.ScriptableBool.WithValue(value);
            return scriptableBool;
        }

        [Test]
        [TestCase(true, ExpectedResult = true)]
        [TestCase(false, ExpectedResult = false)]
        public bool _set_value(bool value)
        {
            ScriptableBool scriptableBool = A.ScriptableBool;

            scriptableBool.Value = value;

            return scriptableBool;
        }

        [Test]
        [TestCase(true, ExpectedResult = true)]
        [TestCase(false, ExpectedResult = false)]
        public bool _call_update_event(bool value)
        {
            ScriptableBool scriptableBool = A.ScriptableBool;
            var eventValue = !value;
            scriptableBool.RegisterUpdateAction(b => eventValue = b);

            scriptableBool.Value = value;

            return eventValue;
        }

        [Test]
        [TestCase(true, ExpectedResult = true)]
        [TestCase(false, ExpectedResult = false)]
        public bool _call_update_event_on_registration(bool value)
        {
            ScriptableBool scriptableBool = A.ScriptableBool;
            var eventValue = !value;
            scriptableBool.Value = value;
            scriptableBool.RegisterUpdateAction(b => eventValue = b);

            return eventValue;
        }

        [Test]
        [TestCase(true, ExpectedResult = false)]
        [TestCase(false, ExpectedResult = true)]
        public bool _not_call_update_event_after_unregistration(bool value)
        {
            ScriptableBool scriptableBool = A.ScriptableBool;
            var eventValue = !value;
            void UpdateAction(bool b) => eventValue = b;

            scriptableBool.RegisterUpdateAction(UpdateAction);
            eventValue = !value;
            scriptableBool.UnregisterUpdateAction(UpdateAction);

            scriptableBool.Value = value;

            return eventValue;
        }

        [Test]
        [TestCase(true, ExpectedResult = false)]
        [TestCase(false, ExpectedResult = true)]
        public bool _toggle_its_value(bool initialValue)
        {
            ScriptableBool scriptableBool = A.ScriptableBool.WithValue(initialValue);
            
            scriptableBool.Toggle();
            
            return scriptableBool.Value;
        }
    }
}