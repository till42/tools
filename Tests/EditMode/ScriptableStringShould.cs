using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableStringShould
    {
        [Test]
        public void Be_Created()
        {
            ScriptableString scriptableString = A.ScriptableString;
            scriptableString.Should().NotBeNull();
        }

        [Test]
        public void Have_Default_As_Initial_Value()
        {
            ScriptableString scriptableString = A.ScriptableString;
            scriptableString.Value.Should().Be("");
        }

        [Test]
        [TestCase("Hello"), TestCase("World")]
        public void Be_Reset(string value)
        {
            ScriptableString scriptableString = A.ScriptableString.WithValue(value);
            scriptableString.Reset();
            scriptableString.Value.Should().Be("");
        }

        [Test]
        [TestCase("5", ExpectedResult = "5")]
        [TestCase("0", ExpectedResult = "0")]
        [TestCase("", ExpectedResult = "")]
        public string Have_Builder_Value(string value)
        {
            ScriptableString scriptableString = A.ScriptableString.WithValue(value);
            return scriptableString;
        }

        [Test]
        [TestCase("5", ExpectedResult = "5")]
        [TestCase("0", ExpectedResult = "0")]
        [TestCase("", ExpectedResult = "")]
        public string Set_Value(string value)
        {
            ScriptableString scriptableString = A.ScriptableString;

            scriptableString.Value = value;

            return scriptableString;
        }

        [Test]
        [TestCase("5", ExpectedResult = "5")]
        [TestCase("0", ExpectedResult = "0")]
        [TestCase("", ExpectedResult = "")]
        public string Call_Update_Event(string value)
        {
            ScriptableString scriptableString = A.ScriptableString;
            var eventValue = default(string);
            scriptableString.RegisterUpdateAction(s => eventValue = s);
            eventValue = "wrong value";

            scriptableString.Value = value;

            return eventValue;
        }

        [Test]
        [TestCase("5", ExpectedResult = "5")]
        [TestCase("0", ExpectedResult = "0")]
        [TestCase("", ExpectedResult = "")]
        public string Call_Update_Event_On_Registration(string value)
        {
            ScriptableString scriptableString = A.ScriptableString;
            var eventValue = default(string);
            scriptableString.Value = value;

            scriptableString.RegisterUpdateAction(s => eventValue = s);

            return eventValue;
        }

        [Test]
        [TestCase("5", ExpectedResult = "correct value")]
        [TestCase("0", ExpectedResult = "correct value")]
        [TestCase("", ExpectedResult = "correct value")]
        public string Not_Call_Update_Event_After_Unregistration(string value)
        {
            ScriptableString scriptableString = A.ScriptableString;
            var eventValue = "correct value";
            void UpdateAction(string s) => eventValue = s;

            scriptableString.RegisterUpdateAction(UpdateAction);
            eventValue = "correct value";
            scriptableString.UnregisterUpdateAction(UpdateAction);

            scriptableString.Value = value;

            return eventValue;
        }
    }
}