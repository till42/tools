using FantasyArts.Tools.Architecture;
using FantasyArts.Tools.Runtime.Architecture.Extensions;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class IntExtensionsShould
    {
        [Test]
        [TestCase(0, ExpectedResult = "")]
        [TestCase(1, ExpectedResult = "1")]
        [TestCase(9876, ExpectedResult = "9876")]
        public string Return_Empty_String_If_Zero(int i)
        {
            return i.EmptyIfZero();
        }
    }
}