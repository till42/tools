using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableOneTimeEvent_should_
    {
        private ScriptableOneTimeEvent _sut;

        [SetUp]
        public void _setup()
        {
            _sut = A.ScriptableOneTimeEvent;
        }

        [Test]
        public void Be_Created()
        {
            _sut.Should().NotBeNull();
        }

        [Test]
        public void Invoke_Registered_Action_On_Raise()
        {
            var wasCalled = false;
            void RaiseAction() => wasCalled = true;

            _sut.RegisterRaiseAction(RaiseAction);
            _sut.Raise();

            wasCalled.Should().BeTrue();
        }

        [Test]
        public void Not_Invoke_UnRegistered_Action_On_Raise()
        {
            var wasCalled = false;
            void RaiseAction() => wasCalled = true;

            _sut.RegisterRaiseAction(RaiseAction);
            _sut.UnRegisterRaiseAction(RaiseAction);
            _sut.Raise();

            wasCalled.Should().BeFalse();
        }

        [Test]
        public void _not_be_raised_twice()
        {
            var timesCalled = 0;
            void RaiseAction() => timesCalled++;

            _sut.RegisterRaiseAction(RaiseAction);
            _sut.Raise();
            _sut.Raise();

            timesCalled.Should().Be(1);
        }

        [Test]
        public void _be_raised_twice_after_reset()
        {
            var timesCalled = 0;
            void RaiseAction() => timesCalled++;

            _sut.RegisterRaiseAction(RaiseAction);
            _sut.Raise();
            _sut.Reset();
            _sut.Raise();

            timesCalled.Should().Be(2);
        }

        [Test]
        public void invoke_UnityEvent_on_raise()
        {
            var wasCalled = false;
            void RaiseAction() => wasCalled = true;

            _sut.RaisedEvent.AddListener(RaiseAction);
            _sut.Raise();

            wasCalled.Should().BeTrue();
        }
    }
}