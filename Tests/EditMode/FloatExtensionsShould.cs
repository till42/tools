using FantasyArts.Tools.Architecture;
using FantasyArts.Tools.Runtime.Architecture.Extensions;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class FloatExtensionsShould
    {
        [Test]
        [TestCase(-1f, ExpectedResult = "0:00")]
        [TestCase(0f, ExpectedResult = "0:00")]
        [TestCase(5.5f, ExpectedResult = "0:05")]
        [TestCase(60f, ExpectedResult = "1:00")]
        [TestCase(120f, ExpectedResult = "2:00")]
        [TestCase(121f, ExpectedResult = "2:01")]
        [TestCase(179f, ExpectedResult = "2:59")]
        [TestCase(180f, ExpectedResult = "3:00")]
        public string Return_As_Minutes_And_Seconds(float value)
        {
            return value.AsMinutesAndSeconds();
        }
    }
}