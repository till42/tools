using FantasyArts.Tools.Architecture;
using FantasyArts.Tools.Runtime.Architecture.Extensions;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class StringExtensions_should_
    {
        [Test]
        [TestCase("01. Item", ExpectedResult = "Item")]
        [TestCase("2. Item", ExpectedResult = "Item")]
        [TestCase("04.Item", ExpectedResult = "Item")]
        [TestCase("05.Item", ExpectedResult = "Item")]
        [TestCase("Item", ExpectedResult = "Item")]
        [TestCase("6.Item", ExpectedResult = "Item")]
        [TestCase(".Item", ExpectedResult = ".Item")]
        [TestCase(" Item", ExpectedResult = "Item")]
        [TestCase("", ExpectedResult = "")]
        [TestCase("6. ", ExpectedResult = "")]
        [TestCase("A. Item", ExpectedResult = "A. Item")]
        [TestCase("01. 555", ExpectedResult = "555")]
        [TestCase("555", ExpectedResult = "555")]
        [TestCase(null, ExpectedResult = "")]
        public string Return_String_Without_Leading_Ordinal(string input)
        {
            return input.TrimOrdinalPrefix();
        }

        [Test]
        [TestCase("Item", 5, ExpectedResult = "05. Item")]
        [TestCase("2. Item", 6, ExpectedResult = "06. Item")]
        [TestCase("Item", -1, ExpectedResult = "Item")]
        [TestCase("555", 1, ExpectedResult = "01. 555")]
        [TestCase(null, 1, ExpectedResult = "01.")]
        public string Return_String_With_Leading_Ordinal(string input, int ordinal)
        {
            return input.AddOrdinalPrefix(ordinal);
        }
    }
}