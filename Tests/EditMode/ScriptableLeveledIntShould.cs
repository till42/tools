using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableLeveledIntShould
    {
        [Test]
        public void Be_Created()
        {
            ScriptableLeveledInt leveledInt = A.ScriptableLeveledInt;

            leveledInt.Should().NotBeNull();
        }

        [Test]
        [TestCase(int.MinValue, 1, 2, 3, ExpectedResult = 1)]
        [TestCase(-1, 1, 2, 3, ExpectedResult = 1)]
        [TestCase(0, 1, 2, 3, ExpectedResult = 1)]
        [TestCase(1, 1, 2, 3, ExpectedResult = 2)]
        [TestCase(2, 1, 2, 3, ExpectedResult = 3)]
        [TestCase(3, 1, 2, 3, ExpectedResult = 3)]
        [TestCase(15, 1, 2, 3, ExpectedResult = 3)]
        [TestCase(int.MaxValue, 1, 2, 3, ExpectedResult = 3)]
        [TestCase(0, ExpectedResult = default(int))]
        [TestCase(1, ExpectedResult = default(int))]
        public int Return_Value_For_Level(int level, params int[] values)
        {
            ScriptableLeveledInt leveledInt = A.ScriptableLeveledInt.WithValues(values);

            return leveledInt[level];
        }
    }
}