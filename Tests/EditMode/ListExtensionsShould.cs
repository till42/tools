﻿using System.Collections.Generic;
using FantasyArts.Tools.Runtime.Architecture.Extensions;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.EditMode
{
    [TestFixture]
    public class ListExtensions_should
    {
        private static readonly List<int> List = new List<int>
            {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};

        private static readonly List<int> List5 = new List<int> {0, 1, 2, 3, 4};

        private static readonly List<int> EmptyList = new List<int>();

        private static readonly List<int> NullList;

        [Test]
        public void Pick_A_Random_Item()
        {
            for (int j = 0; j < 10000; j++)
            {
                var i = List.PickRandomItem();
                List.Should().Contain(i);
            }
        }

        [Test]
        public void Each_Item_At_Least_Once_In_1000()
        {
            var picks = new int[List.Count];
            for (int j = 0; j < 10000; j++)
            {
                var i = List.PickRandomItem();
                picks[i]++;
            }

            foreach (var pick in picks)
                pick.Should().BeGreaterThan(0);
        }

        [Test]
        public void _shuffle_a_list()
        {
            var list = new List<int> {0, 1, 2, 3, 4, 5};
            list.Shuffle();

            bool isSorted = true;
            for (var i = 0; i < list.Count; i++)
            {
                if (list[i] != i)
                    isSorted = false;
            }

            isSorted.Should().BeFalse();
        }

        private static List<int> GenerateListOfInts(int numberOfElements)
        {
            var list = new List<int>();
            for (var i = 0; i < numberOfElements; i++)
            {
                list.Add(i);
            }

            return list;
        }
    }
}