using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class ScriptableIntShould
    {
        [Test]
        public void Be_Created()
        {
            ScriptableInt scriptableInt = A.ScriptableInt;
            scriptableInt.Should().NotBeNull();
        }

        [Test]
        public void Have_Default_As_Initial_Value()
        {
            ScriptableInt scriptableInt = A.ScriptableInt;
            scriptableInt.Value.Should().Be(default);
        }

        [Test]
        [TestCase(5), TestCase(0), TestCase(int.MinValue), TestCase(int.MaxValue)]
        public void Be_Reset(int value)
        {
            ScriptableInt scriptableInt = A.ScriptableInt.WithValue(value);
            scriptableInt.Reset();
            scriptableInt.Value.Should().Be(default);
        }

        [Test]
        [TestCase(5, ExpectedResult = 5)]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(int.MinValue, ExpectedResult = int.MinValue)]
        [TestCase(int.MaxValue, ExpectedResult = int.MaxValue)]
        public int Have_Builder_Value(int value)
        {
            ScriptableInt scriptableInt = A.ScriptableInt.WithValue(value);
            return scriptableInt;
        }

        [Test]
        [TestCase(5, ExpectedResult = 5)]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(int.MinValue, ExpectedResult = int.MinValue)]
        [TestCase(int.MaxValue, ExpectedResult = int.MaxValue)]
        public int Set_Value(int value)
        {
            ScriptableInt scriptableInt = A.ScriptableInt;

            scriptableInt.Value = value;

            return scriptableInt;
        }

        [Test]
        [TestCase(5, ExpectedResult = 5)]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(int.MinValue, ExpectedResult = int.MinValue)]
        [TestCase(int.MaxValue, ExpectedResult = int.MaxValue)]
        public int Call_Update_Event(int value)
        {
            ScriptableInt scriptableInt = A.ScriptableInt;
            var eventValue = -1;
            scriptableInt.RegisterUpdateAction(i => eventValue = i);
            eventValue = -1;

            scriptableInt.Value = value;

            return eventValue;
        }

        [Test]
        [TestCase(5, ExpectedResult = 5)]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(int.MinValue, ExpectedResult = int.MinValue)]
        [TestCase(int.MaxValue, ExpectedResult = int.MaxValue)]
        public int Call_Update_Event_On_Registration(int value)
        {
            ScriptableInt scriptableInt = A.ScriptableInt;
            var eventValue = -1;
            scriptableInt.Value = value;

            scriptableInt.RegisterUpdateAction(i => eventValue = i);

            return eventValue;
        }

        [Test]
        [TestCase(5, ExpectedResult = -1)]
        [TestCase(0, ExpectedResult = -1)]
        [TestCase(int.MinValue, ExpectedResult = -1)]
        [TestCase(int.MaxValue, ExpectedResult = -1)]
        public int Not_Call_Update_Event_After_Unregistration(int value)
        {
            ScriptableInt scriptableInt = A.ScriptableInt;
            var eventValue = -1;
            void UpdateAction(int i) => eventValue = i;

            scriptableInt.RegisterUpdateAction(UpdateAction);
            eventValue = -1;
            scriptableInt.UnregisterUpdateAction(UpdateAction);

            scriptableInt.Value = value;

            return eventValue;
        }

        [Test]
        [TestCase(int.MinValue, 10, 20, ExpectedResult = -10)]
        [TestCase(10, 10, 20, ExpectedResult = 10)]
        [TestCase(0, 10, 20, ExpectedResult = 0)]
        [TestCase(0, 10, 5, ExpectedResult = 5)]
        public int Upon_Remove_Not_Go_Below_MinValue(int minValue, int value, int removeAmount)
        {
            ScriptableInt scriptableInt = A.ScriptableInt.WithMinValue(minValue).WithValue(value);
            scriptableInt.Value -= removeAmount;
            return scriptableInt.Value;
        }

        [Test]
        [TestCase(0, 1, ExpectedResult = 1)]
        [TestCase(-5500, 6500, ExpectedResult = 1000)]
        [TestCase(0, -100, ExpectedResult = 0)]
        [TestCase(5500, -1500, ExpectedResult = 4000)]
        public int Add_A_Value(int initialValue, int valueToAdd)
        {
            ScriptableInt scriptableInt = A.ScriptableInt.WithMinValue(0).WithValue(initialValue);

            scriptableInt.Add(valueToAdd);

            return scriptableInt.Value;
        }

        [Test]
        [TestCase(0, 1, ExpectedResult = 0)]
        [TestCase(-5500, 6500, ExpectedResult = 0)]
        [TestCase(0, -100, ExpectedResult = 100)]
        [TestCase(5500, -1500, ExpectedResult = 7000)]
        public int Substract_A_Value(int initialValue, int valueToSubstract)
        {
            ScriptableInt scriptableInt = A.ScriptableInt.WithMinValue(0).WithValue(initialValue);

            scriptableInt.Subtract(valueToSubstract);

            return scriptableInt.Value;
        }
    }
}