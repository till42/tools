using FantasyArts.Tools.Editor.Helpers;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class FindShould
    {
        [Test]
        public void Return_At_Least_1_ScriptableObject()
        {
            //SomeScriptable instance is in Tests/Scriptables

            var foundObjects = Find.ScriptableObjects<SomeScriptable>();

            foundObjects.Length.Should().BeGreaterOrEqualTo(1);
        }

        [Test]
        public void Return_None_For_NotAvailable()
        {
            //NotAvailableScriptable has no instance

            var foundObjects = Find.ScriptableObjects<NotAvailableScriptable>();

            foundObjects.Length.Should().Be(0);
        }

        [Test]
        public void Return_At_Least_5_ScriptableInts()
        {
            //Some ScriptableInt (1..5) instance is in Tests/Scriptables

            var foundObjects = Find.ScriptableObjects<ScriptableInt>();

            foundObjects.Length.Should().BeGreaterOrEqualTo(5);
        }

        [Test]
        public void Return_One_Instance_Of_SomeScriptable()
        {
            //SomeScriptable instance is in Tests/Scriptables

            var foundObject = Find.ScriptableObject<SomeScriptable>();

            foundObject.Should().NotBeNull();
        }

        [Test]
        public void Return_One_Instance_In_Search_Of_Multiple_ScriptableObjects_Of_Some_ScriptableInt_1()
        {
            //SomeScriptable instance is in Tests/Scriptables

            var foundObjects = Find.ScriptableObjects<ScriptableInt>("Some ScriptableInt 1");

            foundObjects.Length.Should().BeGreaterOrEqualTo(1);
        }

        [Test]
        public void Return_One_Instance_Of_Some_ScriptableInt_1()
        {
            //SomeScriptable instance is in Tests/Scriptables

            var foundObject = Find.ScriptableObject<ScriptableInt>("Some ScriptableInt 1");

            foundObject.Should().NotBeNull();
        }
    }
}