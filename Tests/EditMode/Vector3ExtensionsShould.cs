using FantasyArts.Tools.Architecture;
using FantasyArts.Tools.Runtime.Architecture.Extensions;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using Unity.Profiling;
using UnityEngine;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class Vector3ExtensionsShould
    {
        [Test]
        [TestCase(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity)]
        [TestCase(float.NegativeInfinity, 0f, 0f)]
        [TestCase(0f, float.NegativeInfinity, 0f)]
        [TestCase(0f, 0f, float.NegativeInfinity)]
        [TestCase(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity)]
        [TestCase(float.PositiveInfinity, 0f, 0f)]
        [TestCase(0f, float.PositiveInfinity, 0f)]
        [TestCase(0f, 0f, float.PositiveInfinity)]
        public void Be_Inifinite_For(float x, float y, float z)
        {
            var vector3 = new Vector3(x, y, z);
            vector3.IsInfinite().Should().BeTrue();
        }

        [Test]
        [TestCase(1f, 2f, 3f)]
        [TestCase(-1f, -2f, -3f)]
        [TestCase(float.MinValue, 0f, 0f)]
        [TestCase(0f, float.MinValue, 0f)]
        [TestCase(0f, 0f, float.MinValue)]
        [TestCase(float.MaxValue, 0f, 0f)]
        [TestCase(0f, float.MaxValue, 0f)]
        [TestCase(0f, 0f, float.MaxValue)]
        public void Not_Be_Inifinite_For(float x, float y, float z)
        {
            var vector3 = new Vector3(x, y, z);
            vector3.IsInfinite().Should().BeFalse();
        }
    }
}