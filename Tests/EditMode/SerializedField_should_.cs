using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;
using static FantasyArts.Tools.Tests.EditMode.HelperComponents;

namespace FantasyArts.Tools.Tests.EditMode
{
    public class SerializedField_should_
    {
        private TestComponent _testComponent;
        private TestComponentChild _testComponentChild;
        private TestComponentGrandChild _testComponentChildGrandChild;

        [SetUp]
        public void _setup()
        {
            _testComponent = new Builder<TestComponent>();
            _testComponentChild = new Builder<TestComponentChild>();
            _testComponentChildGrandChild = new Builder<TestComponentGrandChild>();
        }

        [Test]
        public void _get_default_bool_value()
            => _testComponent.GetValue<bool>("_testBool").Should().Be(true);

        [Test]
        public void _get_default_int_value()
            => _testComponent.GetValue<int>("_testInt").Should().Be(15);

        [Test]
        public void _get_default_long_value()
            => _testComponent.GetValue<long>("_testLong").Should().Be(555L);

        [Test]
        public void _get_default_string_value()
            => _testComponent.GetValue<string>("_testString").Should().Be("Test");

        [Test]
        public void _get_default_float_value()
            => _testComponent.GetValue<float>("_testFloat").Should().Be(1f);

        [Test]
        public void _get_default_double_value()
            => _testComponent.GetValue<double>("_testDouble").Should().Be(1d);

        [Test]
        public void _get_default_vector2_value()
            => _testComponent.GetValue<Vector2>("_testVector2").Should().Be(Vector2.one);

        [Test]
        public void _get_default_vector3_value()
            => _testComponent.GetValue<Vector3>("_testVector3").Should().Be(Vector3.one);

        [Test]
        public void _get_default_vector4_value()
            => _testComponent.GetValue<Vector4>("_testVector4").Should().Be(Vector4.one);

        [Test]
        public void _get_default_vector2Int_value()
            => _testComponent.GetValue<Vector2Int>("_testVector2Int").Should().Be(Vector2Int.one);

        [Test]
        public void _get_default_vector3Int_value()
            => _testComponent.GetValue<Vector3Int>("_testVector3Int").Should().Be(Vector3Int.one);

        [Test]
        public void _get_default_quaternion_value()
            => _testComponent.GetValue<Quaternion>("_testQuaternion").Should().Be(Quaternion.Euler(90f, 90f, 90f));

        [Test]
        public void _get_default_color_value()
            => _testComponent.GetValue<Color>("_testColor").Should().Be(Color.cyan);

        [Test]
        public void _get_default_Rect_value()
            => _testComponent.GetValue<Rect>("_testRect").Should().Be(Rect.MinMaxRect(1, 1, 10, 10));

        [Test]
        public void _get_default_RectInt_value()
            => _testComponent.GetValue<RectInt>("_testRectInt").Should().Be(new RectInt(0, 0, 0, 0));

        [Test]
        public void _get_default_Bounds_value()
            => _testComponent.GetValue<Bounds>("_testBounds").Should().Be(new Bounds(Vector3.zero, Vector3.zero));

        [Test]
        public void _get_default_BoundsInt_value()
            => _testComponent.GetValue<BoundsInt>("_testBoundsInt").Should().Be(new BoundsInt(0, 0, 0, 0, 0, 0));

        [Test]
        public void _get_default_enum_value()
            => _testComponent.GetValue<TestEnum>("_testEnum").Should().Be(TestEnum.DefaultValue);

        [Test]
        public void _get_default_AnimationCurve_value()
            => _testComponent.GetValue<AnimationCurve>("_testAnimationCurve").Should()
                .Be(AnimationCurve.Constant(1f, 1f, 5f));

        [Test]
        public void _get_transformComponentValue()
            => _testComponent.GetValue<Transform>("_otherTransform").Should().BeNull();

        [Test]
        public void _get_intArrayValue()
            => _testComponent.GetValue<int[]>("_intArray").Should().BeNull();


        [Test]
        [TestCase(true)]
        [TestCase(false)]
        public void _set_bool_value(bool value)
        {
            _testComponent.SetValue("_testBool", value);
            _testComponent.GetValue<bool>("_testBool").Should().Be(value);
        }

        [Test]
        [TestCase(4)]
        [TestCase(13)]
        [TestCase(-32)]
        public void _set_int_value(int value)
        {
            _testComponent.SetValue("_testInt", value);
            _testComponent.GetValue<int>("_testInt").Should().Be(value);
        }

        [Test]
        [TestCase(4)]
        [TestCase(13)]
        [TestCase(-32)]
        public void _set_long_value(long value)
        {
            _testComponent.SetValue("_testLong", value);
            _testComponent.GetValue<long>("_testLong").Should().Be(value);
        }

        [Test]
        [TestCase("some value")]
        [TestCase("")]
        public void _set_string_value(string value)
        {
            _testComponent.SetValue("_testString", value);
            _testComponent.GetValue<string>("_testString").Should().Be(value);
        }

        [Test]
        [TestCase(1f)]
        [TestCase(0.2f)]
        [TestCase(-2.7f)]
        public void _set_float_value(float value)
        {
            _testComponent.SetValue("_testFloat", value);
            _testComponent.GetValue<float>("_testFloat").Should().Be(value);
        }

        [Test]
        [TestCase(1d)]
        [TestCase(2.34234d)]
        [TestCase(-2.34243d)]
        public void _set_double_value(double value)
        {
            _testComponent.SetValue("_testDouble", value);
            _testComponent.GetValue<double>("_testDouble").Should().Be(value);
        }

        [Test]
        [TestCase(1f, 2f)]
        [TestCase(-2.4f, 3.4f)]
        [TestCase(100.2f, 1222f)]
        public void _set_Vector2_value(float x, float y)
        {
            var value = new Vector2(x, y);
            _testComponent.SetValue("_testVector2", value);
            _testComponent.GetValue<Vector2>("_testVector2").Should().Be(value);
        }

        [Test]
        [TestCase(1f, 2f, 5f)]
        [TestCase(-2.4f, 3.4f, -43.43f)]
        [TestCase(100.2f, 1222f, 5423f)]
        public void _set_Vector3_value(float x, float y, float z)
        {
            var value = new Vector3(x, y, z);
            _testComponent.SetValue("_testVector3", value);
            _testComponent.GetValue<Vector3>("_testVector3").Should().Be(value);
        }

        [Test]
        [TestCase(1f, 2f, 3f, 4f)]
        [TestCase(-2.4f, 3.4f, -43.43f, -654.3f)]
        [TestCase(100.2f, 1222f, 5423f, 32432f)]
        public void _set_Vector4_value(float x, float y, float z, float w)
        {
            var value = new Vector4(x, y, z, w);
            _testComponent.SetValue("_testVector4", value);
            _testComponent.GetValue<Vector4>("_testVector4").Should().Be(value);
        }


        [Test]
        [TestCase(1, 2)]
        [TestCase(-2, 3)]
        [TestCase(100, 1222)]
        public void _set_Vector2Int_value(int x, int y)
        {
            var value = new Vector2Int(x, y);
            _testComponent.SetValue("_testVector2Int", value);
            _testComponent.GetValue<Vector2Int>("_testVector2Int").Should().Be(value);
        }

        [Test]
        [TestCase(1, 2, 5)]
        [TestCase(-2, 3, -43)]
        [TestCase(100, 1222, 5423)]
        public void _set_Vector3Int_value(int x, int y, int z)
        {
            var value = new Vector3Int(x, y, z);
            _testComponent.SetValue("_testVector3Int", value);
            _testComponent.GetValue<Vector3Int>("_testVector3Int").Should().Be(value);
        }

        [Test]
        [TestCase(1f, 2f, 3f, 4f)]
        [TestCase(-2.4f, 3.4f, -43.43f, -654.3f)]
        [TestCase(100.2f, 1222f, 5423f, 32432f)]
        public void _set_Quaternion_value(float x, float y, float z, float w)
        {
            var value = new Quaternion(x, y, z, w);
            _testComponent.SetValue("_testQuaternion", value);
            _testComponent.GetValue<Quaternion>("_testQuaternion").Should().Be(value);
        }

        [Test]
        [TestCase(0f, 0f, 0f, 0f)]
        [TestCase(0.1f, 0.4f, 0.43f, 0.5f)]
        [TestCase(0.2f, 0.1f, 0.3f, 0.8f)]
        [TestCase(1f, 1f, 1f, 1f)]
        public void _set_Color_value(float r, float g, float b, float a)
        {
            var value = new Color(r, g, b, a);
            _testComponent.SetValue("_testColor", value);
            _testComponent.GetValue<Color>("_testColor").Should().Be(value);
        }

        [Test]
        [TestCase(0f, 0f, 0f, 0f)]
        [TestCase(100f, 400f, 430f, 500f)]
        [TestCase(2f, 1f, 30f, 80f)]
        [TestCase(1000f, 1000f, 1000f, 1000f)]
        public void _set_Rect_value(float x, float y, float width, float height)
        {
            var value = new Rect(x, y, width, height);
            _testComponent.SetValue("_testRect", value);
            _testComponent.GetValue<Rect>("_testRect").Should().Be(value);
        }

        [Test]
        [TestCase(0, 0, 0, 0)]
        [TestCase(100, 400, 430, 500)]
        [TestCase(2, 1, 30, 80)]
        [TestCase(1000, 1000, 1000, 1000)]
        public void _set_RectInt_value(int xMin, int yMin, int width, int height)
        {
            var value = new RectInt(xMin, yMin, width, height);
            _testComponent.SetValue("_testRectInt", value);
            _testComponent.GetValue<RectInt>("_testRectInt").Should().Be(value);
        }

        [Test]
        [TestCase(0f, 0f, 0f, 0f, 0f, 0f)]
        [TestCase(100f, 400f, 430f, 2f, 1f, 30f)]
        [TestCase(2f, 1f, 30f, 100f, 400f, 430f)]
        [TestCase(1000f, 1000f, 1000f, 1000f, 1000f, 1000f)]
        public void _set_Bounds_value(float centerX, float centerY, float centerZ, float sizeX, float sizeY,
            float sizeZ)
        {
            var value = new Bounds(new Vector3(centerX, centerY, centerZ), new Vector3(sizeX, sizeY, sizeZ));
            _testComponent.SetValue("_testBounds", value);
            _testComponent.GetValue<Bounds>("_testBounds").Should().Be(value);
        }

        [Test]
        [TestCase(0, 0, 0, 0, 0, 0)]
        [TestCase(100, 400, 430, 2, 1, 30)]
        [TestCase(2, 1, 30, 100, 400, 430)]
        [TestCase(1000, 1000, 1000, 1000, 1000, 1000)]
        public void _set_BoundsInt_value(int xMin, int yMin, int zMin, int sizeX, int sizeY,
            int sizeZ)
        {
            var value = new BoundsInt(xMin, yMin, zMin, sizeX, sizeY, sizeZ);
            _testComponent.SetValue("_testBoundsInt", value);
            _testComponent.GetValue<BoundsInt>("_testBoundsInt").Should().Be(value);
        }


        [Test]
        [TestCase(0f, 0f, 0f)]
        [TestCase(100f, 400f, 430f)]
        [TestCase(2f, 12f, 0.4f)]
        [TestCase(1000f, 1200f, 1000f)]
        public void _set_AnimationCurve_Constant(float timeStart, float timeEnd, float value)
        {
            var animationCurve = AnimationCurve.Constant(timeStart, timeEnd, value);
            _testComponent.SetValue("_testAnimationCurve", animationCurve);
            _testComponent.GetValue<AnimationCurve>("_testAnimationCurve").Should().Be(animationCurve);
        }

        [Test]
        [TestCase(0f, 0f, 0f, 0f)]
        [TestCase(100f, 0.1f, 400f, 3f)]
        [TestCase(2f, 0.65f, 12f, 0.4f)]
        [TestCase(1000f, 1200f, 6000f, 0f)]
        public void _set_AnimationCurve_Constant(float timeStart, float valueStart, float timeEnd, float valueEnd)
        {
            var animationCurve = AnimationCurve.Linear(timeStart, valueStart, timeEnd, valueEnd);
            _testComponent.SetValue("_testAnimationCurve", animationCurve);
            _testComponent.GetValue<AnimationCurve>("_testAnimationCurve").Should().Be(animationCurve);
        }

        [Test]
        [TestCase(0f, 0f, 0f, 0f)]
        [TestCase(100f, 0.1f, 400f, 3f)]
        [TestCase(2f, 0.65f, 12f, 0.4f)]
        [TestCase(1000f, 1200f, 6000f, 0f)]
        public void _set_AnimationCurve_EaseInOut(float timeStart, float valueStart, float timeEnd, float valueEnd)
        {
            var animationCurve = AnimationCurve.EaseInOut(timeStart, valueStart, timeEnd, valueEnd);
            _testComponent.SetValue("_testAnimationCurve", animationCurve);
            _testComponent.GetValue<AnimationCurve>("_testAnimationCurve").Should().Be(animationCurve);
        }

        [Test]
        [TestCase(TestEnum.DefaultValue)]
        [TestCase(TestEnum.FirstValue)]
        [TestCase(TestEnum.SecondValue)]
        public void _set_enum_value(TestEnum value)
        {
            _testComponent.SetValue("_testEnum", value);
            _testComponent.GetValue<TestEnum>("_testEnum").Should().Be(value);
        }

        [Test]
        public void _set_transform_value()
        {
            var otherTransform = new GameObject().transform;
            _testComponent.SetValue("_otherTransform", otherTransform);
            _testComponent.GetValue<Transform>("_otherTransform").Should().BeEquivalentTo(otherTransform);
            _testComponent.OtherTransform.Should().BeEquivalentTo(otherTransform);
        }

        [Test]
        public void _set_otherComponent_value()
        {
            var gameObject = new GameObject();
            var otherTestComponent = gameObject.AddComponent<TestComponent>();

            _testComponent.SetValue("_otherTestComponent", otherTestComponent);
            _testComponent.GetValue<TestComponent>("_otherTestComponent").Should()
                .BeEquivalentTo(otherTestComponent);
            _testComponent.OtherTestComponent.Should().BeEquivalentTo(otherTestComponent);
        }

        [Test]
        public void _set_intArray_value()
        {
            var intArray = new[] {2, 3, 4, 5};
            _testComponent.SetValue("_intArray", intArray);
            var result =
                _testComponent.GetValue<int[]>("_intArray");
            result.Should()
                .Equal(intArray);
        }

        [Test]
        public void _set_testClass_value()
        {
            var testClass = new TestClass {String = "Hello World"};

            _testComponent.SetValue("_testClass", testClass);

            var result =
                _testComponent.GetValue<TestClass>("_testClass");
            result.Should().Be(testClass);
        }


        [Test]
        [TestCase(4)]
        [TestCase(13)]
        [TestCase(-32)]
        public void _set_base_class_private_int_value(int value)
        {
            _testComponentChild.SetValue("_testInt", value);
            _testComponentChild.GetValue<int>("_testInt").Should().Be(value);
        }

        [Test]
        [TestCase(4)]
        [TestCase(13)]
        [TestCase(-32)]
        public void _set_base_base_class_private_int_value(int value)
        {
            _testComponentChildGrandChild.SetValue("_testInt", value);
            _testComponentChildGrandChild.GetValue<int>("_testInt").Should().Be(value);
        }
    }
}