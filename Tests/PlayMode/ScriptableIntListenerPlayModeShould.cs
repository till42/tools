using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace FantasyArts.Tools.Tests.PlayMode
{
    public class ScriptableIntListenerPlayModeShould
    {
        [Test]
        public void Be_Created()
        {
            ScriptableIntListenBehaviour scriptableIntListenBehaviour = A.ScriptableIntListener;
            scriptableIntListenBehaviour.Should().NotBeNull();
        }


        // [Test]
        // public void Not_Raise_UnityEvent_On_ScriptableInt_Update_When_Script_Is_Not_Enabled()
        // {
        //     ScriptableIntListener scriptableIntListener = A.ScriptableIntListener
        //         .WithScriptableInt(out var scriptableInt)
        //         .WithRaiseUnityEventMonitor(out var raisedUnityEvent);
        //
        //     scriptableIntListener.enabled = false;
        //
        //     scriptableInt.Value++;
        //
        //     raisedUnityEvent.WasRaised.Should().BeFalse();
        // }


        // [Test]
        // public void Not_Raise_UnityEvent_On_ScriptableInt_Update_When_GameObject_Is_Not_Active()
        // {
        //     ScriptableIntListener scriptableIntListener = A.ScriptableIntListener
        //         .WithScriptableInt(out var scriptableInt)
        //         .WithRaiseUnityEventMonitor(out var raisedUnityEvent);
        //     scriptableIntListener.gameObject.SetActive(false);
        //
        //     scriptableInt.Value++;
        //
        //     raisedUnityEvent.WasRaised.Should().BeFalse();
        // }
    }
}