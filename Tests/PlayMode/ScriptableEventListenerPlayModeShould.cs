using System.Collections;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace FantasyArts.Tools.Tests.PlayMode
{
    public class ScriptableEventListenerPlayModeShould
    {
        [Test]
        public void Be_Created()
        {
            ScriptableEventListenBehaviour scriptableEventListenBehaviour = A.ScriptableEventListener;
            scriptableEventListenBehaviour.Should().NotBeNull();
        }

        [Test]
        public void Not_Raise_UnityEvent_On_ScriptableEvent_Raise_When_Script_Is_Not_Enabled()
        {
            ScriptableEventListenBehaviour scriptableEventListenBehaviour = A.ScriptableEventListener
                .WithScriptableEvent(out var scriptableEvent)
                .WithRaiseUnityEventMonitor(out var raisedUnityEvent);

            scriptableEventListenBehaviour.enabled = false;

            scriptableEvent.Raise();

            raisedUnityEvent.WasRaised.Should().BeFalse();
        }

        [UnityTest]
        public IEnumerator Not_Raise_UnityEvent_On_ScriptableEvent_Raise_When_GameObject_Is_Not_Active()
        {
            ScriptableEventListenBehaviour scriptableEventListenBehaviour = A.ScriptableEventListener
                .WithScriptableEvent(out var scriptableEvent)
                .WithRaiseUnityEventMonitor(out var raisedUnityEvent);
            scriptableEventListenBehaviour.gameObject.SetActive(false);

            yield return null;

            scriptableEvent.Raise();

            raisedUnityEvent.WasRaised.Should().BeFalse();
        }
    }
}