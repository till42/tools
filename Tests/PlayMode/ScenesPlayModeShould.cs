using System.Collections;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using FantasyArts.Tools.Tests.Helpers;
using FluentAssertions;
using NUnit.Framework;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using static UnityEditor.SceneManagement.EditorSceneManager;

namespace FantasyArts.Tools.Tests.PlayMode
{
    public class ScenesPlayModeShould
    {
        [Test]
        public void Find_TestDirectory_Starting_With_Assets()
        {
            var testDirectory = Scenes.TestDirectory;
            testDirectory.Should().StartWith("Assets");
        }

        [Test]
        [TestCase("TestScene001", ExpectedResult = true)]
        [TestCase("NotAvailbleSceneName", ExpectedResult = false)]
        public bool Find_Scene(string sceneName)
        {
            var scenePath = Scenes.Find(sceneName);
            return !string.IsNullOrEmpty(scenePath);
        }
    }
}