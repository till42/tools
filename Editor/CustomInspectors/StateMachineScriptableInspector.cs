using System.Collections.Generic;
using System.Linq;
using FantasyArts.Tools.Architecture.Scene;
using FantasyArts.Tools.Runtime.Architecture.StateMachine;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace FantasyArts.Tools.Editor.CustomInspectors
{
    [CustomEditor(typeof(StateMachineScriptable))]
    public class StateMachineScriptableInspector : UnityEditor.Editor
    {
        private StateMachineScriptable _stateMachine;

        private SerializedProperty _states;

        private ReorderableList _reorderableStateList;

        private List<SceneStateMachineBehaviour> _initializeBehaviours;
        private List<StateListenerBehaviour> _stateListeners;

        private int _currentStateIndex = -1;
        private bool _isRunning;

        private bool IsRunning
        {
            get => _isRunning;
            set
            {
                _isRunning = value;
                _reorderableStateList.displayAdd = !_isRunning;
                _reorderableStateList.displayRemove = !_isRunning;
            }
        }

        private void OnEnable()
        {
            _stateMachine = target as StateMachineScriptable;
            _states = serializedObject.FindProperty("_states");
            _reorderableStateList = new ReorderableList(serializedObject, _states, true, true, true, true);
            _reorderableStateList.drawHeaderCallback = DrawHeader;
            _reorderableStateList.drawElementCallback = DrawListItem;
            _reorderableStateList.onAddCallback = HandleAdd;
            _reorderableStateList.onRemoveCallback = HandleRemove;
            _reorderableStateList.onReorderCallback = HandleReorder;

            _initializeBehaviours = GetSceneReferencesInitialize();
            _stateListeners = GetSceneReferenceStateListeners();
            var listenersInPrefabs = GetPrefabReferenceStateListeners();

            _stateListeners.AddRange(listenersInPrefabs);
        }

        private List<StateListenerBehaviour> GetPrefabReferenceStateListeners()
        {
            var output = new List<StateListenerBehaviour>();
            var prefabGuids = AssetDatabase.FindAssets("t:prefab", null);
            foreach (var prefabGuid in prefabGuids)
            {
                var path = AssetDatabase.GUIDToAssetPath(prefabGuid);
                var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                var allListeners = prefab.GetComponentsInChildren<StateListenerBehaviour>(true);
                var listeners = allListeners.Where(l => l.StateMachine == _stateMachine).ToList();
                output.AddRange(listeners);
            }

            return output;
        }

        private List<SceneStateMachineBehaviour> GetSceneReferencesInitialize()
        {
            var allStateMachineInitializeBehaviours = SceneComponents.GetAll<SceneStateMachineBehaviour>();

            var stateMachineInitializeBehaviours =
                allStateMachineInitializeBehaviours.Where(s => s.StateMachine == _stateMachine).ToList();

            return stateMachineInitializeBehaviours;
        }

        private List<StateListenerBehaviour> GetSceneReferenceStateListeners()
        {
            var allStateListeners = SceneComponents.GetAll<StateListenerBehaviour>();

            var stateListeners = allStateListeners.Where(l => l.StateMachine == _stateMachine).ToList();

            return stateListeners;
        }

        public override void OnInspectorGUI()
        {
            GUILayout.Space(20);
            serializedObject.Update();
            _reorderableStateList.DoLayoutList(); // Have the ReorderableList do its work

            // We need to call this so that changes on the Inspector are saved by Unity.
            serializedObject.ApplyModifiedProperties();

            GUILayout.Space(20);

            DrawStateMachine();

            GUILayout.Space(20);


            DrawReferences("Initializers:", _initializeBehaviours);

            DrawReferences("Listeners:", _stateListeners);
        }

        private static void DrawReferences<T>(string label, List<T> components) where T : Component
        {
            if (components == null || components.Count <= 0) return;
            EditorGUILayout.LabelField(label);

            EditorGUI.BeginDisabledGroup(true);
            foreach (var component in components)
            {
                EditorGUILayout.ObjectField($"", component,
                    typeof(T), true);
            }

            EditorGUI.EndDisabledGroup();
        }

        private void DrawStateMachine()
        {
            if (!IsInitialized(_stateMachine, out var currentState)) return;

            _currentStateIndex = IndexOf(_stateMachine.States, currentState);

            DisplayCurrentState(_stateMachine, currentState);

            GUILayout.Space(20);

            _stateMachine.ButtonReset("Reset");
        }

        private void HandleReorder(ReorderableList list)
        {
            RefreshIndicies(list);
        }

        private void HandleRemove(ReorderableList list)
        {
            var index = list.index;
            _stateMachine.RemoveNestedStateScriptable(index);
            RefreshIndicies(list);
        }

        private void HandleAdd(ReorderableList list)
        {
            _stateMachine.CreateNestedStateScriptable("New State", list.count);
            list.index = list.count;
            RefreshIndicies(list);
        }

        private void RefreshIndicies(ReorderableList list)
        {
            var index = 0;
            for (int i = 0; i < list.count; i++)
            {
                var element = list.serializedProperty.GetArrayElementAtIndex(i);
                var state = element.objectReferenceValue as StateScriptable;
                if (state == null) continue;
                if (state.Index != index)
                    state.Index = index;
                index++;
            }
        }

        private void DrawHeader(Rect rect)
        {
            EditorGUI.LabelField(rect, "States");
        }

        private void DrawListItem(Rect rect, int index, bool isactive, bool isfocused)
        {
            var property = _reorderableStateList.serializedProperty.GetArrayElementAtIndex(index);
            var objectReferenceValue = property.objectReferenceValue;
            var state = objectReferenceValue as StateScriptable;
            if (state == null) return;

            if (index == _currentStateIndex)
                EditorGUI.DrawRect(rect, new Color(0.368f, 0.454f, 0.266f));

            var buttonWidth = 50f;
            var rectNameField = new Rect(rect.x, rect.y, rect.width - buttonWidth, rect.height);

            var label = $"{state.Name}";
            if (index == 0) label = $"{label} [Default]";
            EditorGUI.LabelField(rectNameField, label);

            if (IsRunning) EditorGUI.BeginDisabledGroup(true);
            var rectButton = new Rect(rect.x + rectNameField.width, rect.y, buttonWidth, rect.height);
            if (GUI.Button(rectButton, "Open")) Selection.activeObject = objectReferenceValue;
            if (IsRunning) EditorGUI.EndDisabledGroup();
        }

        private static int IndexOf<T>(IReadOnlyList<T> list, T element)
        {
            for (int i = 0; i < list.Count; i++)
            {
                var e = list[i];
                if (Equals(e, element)) return i;
            }

            return -1;
        }

        private bool IsInitialized(StateMachineScriptable stateMachine, out StateScriptable currentState)
        {
            currentState = stateMachine.CurrentState as StateScriptable;
            if (IsRunning != stateMachine.IsInitialized)
                IsRunning = stateMachine.IsInitialized;

            if (!stateMachine.IsInitialized)
                _currentStateIndex = -1;

            if (stateMachine.IsInitialized)
                return true;

            _stateMachine.ButtonInitialize("Initialize");

            return false;
        }

        private void DisplayCurrentState(StateMachineScriptable stateMachine, StateScriptable currentState)
        {
            EditorGUILayout.BeginHorizontal();

            DisplayState("Current State", currentState);
            if (GUILayout.Button("Update")) stateMachine.Update();

            EditorGUILayout.EndHorizontal();

            DisplayTriggersOf(stateMachine, currentState);
        }

        private static void DisplayTriggersOf(StateMachineScriptable stateMachine, StateScriptable state)
        {
            foreach (var transition in state.Transitions)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space(20f);
                if (GUILayout.Button(transition.TriggerName)) stateMachine.SetTrigger(transition.TriggerName);
                GUILayout.Space(20f);
                var toState = transition.ToState;
                DisplayState("-->", toState);
                EditorGUILayout.EndHorizontal();
            }
        }

        private static void DisplayState(string label, StateScriptable state)
        {
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.ObjectField(label, state, typeof(StateScriptable), false);
            EditorGUI.EndDisabledGroup();
        }
    }
}