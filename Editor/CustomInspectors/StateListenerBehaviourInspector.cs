﻿using FantasyArts.Tools.Runtime.Architecture.StateMachine;
using UnityEditor;
using UnityEngine;

namespace FantasyArts.Tools.Editor.CustomInspectors
{
    [CustomEditor(typeof(StateListenerBehaviour))]
    public class StateListenerBehaviourInspector : UnityEditor.Editor
    {
        private EventDefinition _initializeEvent;
        private EventDefinition _enterEvent;
        private EventDefinition _exitEvent;

        private StateListenerBehaviour _stateListener;


        private void OnEnable()
        {
            _stateListener = target as StateListenerBehaviour;

            _initializeEvent = new EventDefinition
            {
                IsFolded = false,
                Content = "State Machine Initialize",
                LogProperty = serializedObject.FindProperty("_doLogInitialize"),
                EventProperty = serializedObject.FindProperty("OnInitialize"),
            };

            _enterEvent = new EventDefinition
            {
                IsFolded = false,
                Content = "State Enter",
                LogProperty = serializedObject.FindProperty("_doLogEnter"),
                EventProperty = serializedObject.FindProperty("OnEnter"),
            };

            _exitEvent = new EventDefinition
            {
                IsFolded = false,
                Content = "State Exit",
                LogProperty = serializedObject.FindProperty("_doLogExit"),
                EventProperty = serializedObject.FindProperty("OnExit"),
            };
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            var stateProperty = serializedObject.FindProperty("_state");
            var doActivateWithStateProperty = serializedObject.FindProperty("_doActivateWithState");
            var doEnableOnEnterProperty = serializedObject.FindProperty("_doEnableOnEnter");
            var doDisableOnExitProperty = serializedObject.FindProperty("_doDisableOnExit");

            var hasEventsProperty = serializedObject.FindProperty("_hasEvents");

            var state = _stateListener.State;
            if (state != null)
            {
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.ObjectField("State Machine", state.StateMachine, typeof(StateMachineScriptable),
                    allowSceneObjects: false);
                EditorGUI.EndDisabledGroup();
            }

            EditorGUILayout.PropertyField(stateProperty);


            var doActivatePreviousValue = doActivateWithStateProperty.boolValue;
            EditorGUILayout.PropertyField(doActivateWithStateProperty,
                new GUIContent("State Enabling",
                    tooltip: "Enable/disable the gameobject when the State is entered/exited"));
            if (doActivatePreviousValue != doActivateWithStateProperty.boolValue)
            {
                doEnableOnEnterProperty.boolValue = doActivateWithStateProperty.boolValue;
                doDisableOnExitProperty.boolValue = doActivateWithStateProperty.boolValue;
            }

            EditorGUI.indentLevel++;
            EditorGUI.BeginDisabledGroup(doActivateWithStateProperty.boolValue);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(doEnableOnEnterProperty,
                new GUIContent("Enable On Entered",
                    tooltip: "Enable/disable the gameobject when the State is entered"));
            EditorGUILayout.PropertyField(doDisableOnExitProperty,
                new GUIContent("Disable On Exit",
                    tooltip: "Enable/disable the gameobject when the State is exited"));
            EditorGUILayout.EndHorizontal();
            EditorGUI.EndDisabledGroup();
            EditorGUI.indentLevel--;

            EditorGUILayout.PropertyField(hasEventsProperty, new GUIContent("Events",
                tooltip: "Activate events when the state is initialized, entered or exited"));

            if (hasEventsProperty.boolValue) DrawEvents();

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawEvents()
        {
            DrawFoldout(ref _initializeEvent);
            DrawFoldout(ref _enterEvent);
            DrawFoldout(ref _exitEvent);
        }

        private void DrawFoldout(ref EventDefinition eventDefinition)
        {
            var callsProperty = eventDefinition.EventProperty.FindPropertyRelative("m_PersistentCalls.m_Calls");
            var noOfCalls = callsProperty.arraySize;
            var style = new GUIStyle(EditorStyles.foldout);
            style.fontStyle = noOfCalls > 0 ? FontStyle.Bold : FontStyle.Normal;


            eventDefinition.IsFolded =
                EditorGUILayout.Foldout(eventDefinition.IsFolded, eventDefinition.Content + $" ({noOfCalls})", style);
            if (!eventDefinition.IsFolded) return;

            EditorGUILayout.PropertyField(eventDefinition.LogProperty);

            EditorGUILayout.PropertyField(eventDefinition.EventProperty);
        }

        private struct EventDefinition
        {
            public bool IsFolded;
            public string Content;
            public SerializedProperty LogProperty;
            public SerializedProperty EventProperty;
        }
    }
}