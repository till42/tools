﻿using FantasyArts.Tools.Runtime.Architecture.StateMachine;
using UnityEditor;
using UnityEngine;

namespace FantasyArts.Tools.Editor.CustomInspectors
{
    [CustomEditor(typeof(StateScriptable))]
    public class StateScriptableInspector : UnityEditor.Editor
    {
        private StateScriptable _state;

        private StateMachineScriptable _stateMachine;

        private static readonly string[] _dontIncludeMe = {"m_Script", "_stateMachine"};

        private SerializedProperty _nameProperty;

        private string _newName;

        private void OnEnable()
        {
            _state = target as StateScriptable;
            var property = serializedObject.FindProperty("_stateMachine");
            var objectReferenceValue = property.objectReferenceValue;
            _stateMachine = objectReferenceValue as StateMachineScriptable;
            _nameProperty = serializedObject.FindProperty("m_Name");
            _newName = _state.Name;
        }

        public override void OnInspectorGUI()
        {
            if (_stateMachine == default)
            {
                EditorGUILayout.LabelField("Reference to StateMachine is broken");
                return;
            }

            if (GUILayout.Button($"< {_stateMachine.name}"))
            {
                Selection.activeObject = _stateMachine;
            }

            serializedObject.Update();


            EditorGUILayout.LabelField("State Name", _state.Name);
            EditorGUILayout.BeginHorizontal();
            _newName = EditorGUILayout.TextField("New Name", _newName);
            if (GUILayout.Button("Rename"))
            {
                if (_newName != _state.Name) _state.Name = _newName;
            }

            EditorGUILayout.EndHorizontal();


            DrawPropertiesExcluding(serializedObject, _dontIncludeMe);

            serializedObject.ApplyModifiedProperties();
        }
    }
}