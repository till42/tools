﻿using FantasyArts.Tools.Runtime.Architecture.StateMachine;
using UnityEditor;
using UnityEngine;

namespace FantasyArts.Tools.Editor.CustomInspectors
{
    [CustomEditor(typeof(StateMachineTriggerBehaviour))]
    public class StateMachineTriggerInspector : UnityEditor.Editor
    {
        private StateMachineTriggerBehaviour _triggerBehaviour;

        private static readonly string[] _dontIncludeMe = new string[] {"m_Script"};

        private void OnEnable()
        {
            _triggerBehaviour = target as StateMachineTriggerBehaviour;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            DrawPropertiesExcluding(serializedObject, _dontIncludeMe);

            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("SetTrigger"))
            {
                _triggerBehaviour.Trigger();
            }
        }
    }
}