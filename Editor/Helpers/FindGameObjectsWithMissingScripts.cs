using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FantasyArts.Tools.Editor.Helpers
{
    public class SelectGameObjectsWithMissingScripts : UnityEditor.Editor
    {
        [MenuItem("Tools/FantasyArts/Find GameObjects With Missing Scripts")]
        static void FindGameObjectsWithMissingScripts()
        {
            var currentScene = SceneManager.GetActiveScene();
            var rootObjects = currentScene.GetRootGameObjects();
            var gameObjectsFound = false;

            foreach (var rootObject in rootObjects)
            {
                var tranforms = rootObject.GetComponentsInChildren<Transform>();

                foreach (var t in tranforms)
                {
                    var gameObject = t.gameObject;
                    if (!HasMissingScript(gameObject)) continue;
                    Debug.Log(gameObject + " has a missing script!", gameObject);
                    gameObjectsFound = true;
                    break;
                }
            }

            if (!gameObjectsFound)
            {
                Debug.Log("No GameObject in '" + currentScene.name + "' has missing scripts! Yay!");
            }
        }

        private static bool HasMissingScript(GameObject gameObject)
        {
            if (gameObject == null) return false;
            var components = gameObject.GetComponents<Component>();
            return components.Any(x => x == null);
        }
    }
}