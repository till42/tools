#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace FantasyArts.Tools.Editor.Helpers
{
    public static class Find
    {
        /// <summary>
        /// Find all instances of the given type in the Asset Database with additional filter criteria
        /// </summary>
        /// <param name="additionalFilter">Additional filter criteria for the search on the asset database. E.g. name</param>
        /// <typeparam name="T">type of the scriptable object</typeparam>
        /// <returns>array of instances found</returns>
        public static T[] ScriptableObjects<T>(string additionalFilter) where T : ScriptableObject
        {
            var filter = $"t:{typeof(T)} {additionalFilter}";
            var guids = AssetDatabase.FindAssets(filter);

            var instances = new T[guids.Length];
            for (int i = 0; i < guids.Length; i++)
            {
                var guid = guids[i];
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var instance = AssetDatabase.LoadAssetAtPath<T>(path);
                instances[i] = instance;
            }

            return instances;
        }


        /// <summary>
        /// Find all instances of the given type in the Asset Database
        /// </summary>
        /// <typeparam name="T">type of the scriptable object</typeparam>
        /// <returns>array of instances found</returns>
        public static T[] ScriptableObjects<T>() where T : ScriptableObject => ScriptableObjects<T>("");

        /// <summary>
        /// Find the first instance of a scriptable object with the given type in the Asset Database or default.
        /// </summary>
        /// <typeparam name="T">type of the scriptable object</typeparam>
        /// <returns>instance of the found scriptable object in the Asset Database</returns>
        public static T ScriptableObject<T>() where T : ScriptableObject => ScriptableObjects<T>().FirstOrDefault();

        /// <summary>
        /// Find the first instance of a scriptable object with the given type in the Asset Database with an additional filter criteria or default.
        /// </summary>
        /// <param name="additionalFilter"></param>
        /// <typeparam name="T">Additional filter criteria for the search on the asset database. E.g. name</typeparam>
        /// <returns></returns>
        public static T ScriptableObject<T>(string additionalFilter) where T : ScriptableObject =>
            ScriptableObjects<T>(additionalFilter).FirstOrDefault();
    }
}
#endif