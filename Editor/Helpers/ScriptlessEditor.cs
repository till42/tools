using UnityEngine;
using UnityEditor;

namespace FantasyArts.Tools.Editor.Helpers
{
    public abstract class ScriptlessEditor : UnityEditor.Editor
    {
        private static readonly string[] ExcludedProperties = {"m_Script"};

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            DrawPropertiesExcluding(serializedObject, ExcludedProperties);

            serializedObject.ApplyModifiedProperties();
        }
    }
}