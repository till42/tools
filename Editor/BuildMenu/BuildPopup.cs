﻿#if UNITY_EDITOR && !TOOLS_SKIP_GENERIC_BUILD_POPUP


using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;


namespace FantasyArts.Tools.Editor.BuildMenu
{
    public class BuildPopup : EditorWindow
    {
        [MenuItem("Tools/Build WebGL...")]
        private static void Build()
        {
            DisplayVersionAndPathPopup();
        }

        private static string _currentVersion;
        private static string _buildVersion;

        private static string _outputPath;
        private static string _rootPath;

        private const string PathKey = "BUILD_DEFAULTPATH";

        private static void DisplayVersionAndPathPopup()
        {
            //get the path from previous
            _rootPath = GetRootPath();

            var window = GetWindow<BuildPopup>();
            window.position = new Rect(Screen.width * 0.5f, Screen.height * 0.5f, 550, 450);
            window.titleContent = new GUIContent("Build WebGL");
            _buildVersion = PlayerSettings.bundleVersion;
            _currentVersion = _buildVersion;
            window.ShowModalUtility();
        }

#pragma warning disable CS0414
        private bool _isBuildPerform = false;
#pragma warning restore CS0414

        private void OnGUI()
        {
            GUILayout.Space(10);
            EditorGUILayout.LabelField("Current Version:", _currentVersion, EditorStyles.wordWrappedLabel);
            GUILayout.Space(10);
            _buildVersion = EditorGUILayout.TextField("Version: ", _buildVersion);
            GUILayout.Space(60);
            _outputPath = $"{_rootPath}{Path.DirectorySeparatorChar}v{_buildVersion}";
            EditorGUILayout.LabelField("Output Path:", _outputPath, EditorStyles.wordWrappedLabel);
            if (GUILayout.Button("Change output root"))
            {
                var newRoot = EditorUtility.OpenFolderPanel("Select Build Output Root Path", _rootPath, "");
                newRoot = UnifyDirectorySeparatorChars(newRoot);
                if (!string.IsNullOrEmpty(newRoot))
                    _rootPath = newRoot;
            }

            GUILayout.Space(100);
            using (new GUILayout.HorizontalScope())
            {
                // EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Cancel"))
                {
                    Close();
                }

                if (GUILayout.Button("BUILD"))
                {
                    _isBuildPerform = true;
                    if (_buildVersion != "")
                        PlayerSettings.bundleVersion = _buildVersion;

                    SaveRootPath(_rootPath);

                    EditorApplication.delayCall += PerformBuild;
                    Close();
                }
            }
        }

        private static string UnifyDirectorySeparatorChars(string path)
        {
            if (string.IsNullOrEmpty(path)) return path;
            path = path.Replace('\\', Path.DirectorySeparatorChar);
            path = path.Replace('/', Path.DirectorySeparatorChar);
            return path;
        }

        private static void PerformBuild()
        {
            var editorBuildSettingsScenes = EditorBuildSettings.scenes;
            var levels = editorBuildSettingsScenes.Select(x => x.path);


            var buildPlayerOptions = new BuildPlayerOptions
            {
                scenes = levels.ToArray(),
                locationPathName = _outputPath,
                target = BuildTarget.WebGL,
                options = BuildOptions.None
            };

            var report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        }

        private static string GetRootPath()
            => !PlayerPrefs.HasKey(PathKey) ? "" : UnifyDirectorySeparatorChars(PlayerPrefs.GetString(PathKey));

        private static void SaveRootPath(string newRootPath)
        {
            PlayerPrefs.SetString(PathKey, UnifyDirectorySeparatorChars(newRootPath));
        }
    }
}
#endif