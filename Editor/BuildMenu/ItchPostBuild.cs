﻿#if UNITY_EDITOR && !TOOLS_SKIP_GENERIC_ITCH_POSTBUILDPROCESS
using System.IO;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;
using ZipPlugin;

namespace FantasyArts.Tools.Editor.BuildMenu
{
    public class ItchPostBuild : IPostprocessBuildWithReport
    {
        public int callbackOrder => 0;

        private string zipName = "build.zip";

        private string[] _folders =
        {
            "Build",
            "TemplateData",
            "StreamingAssets"
        };

        private string getZipPath(string path)
        {
            Directory.CreateDirectory(path);
            return Path.Combine(path, zipName);
        }


        public void OnPostprocessBuild(BuildReport report)
        {
            Debug.Log(string.Format("Post processing {0} Build at {1} with folders={2}, zipName={3}",
                report.summary.platform, report.summary.outputPath, _folders, zipName));

            //only for WebGL Build
            if (report.summary.platform != BuildTarget.WebGL)
                return;

            //delete the previous output file
            if (File.Exists(Path.Combine(report.summary.outputPath, zipName)))
            {
                // If file found, delete it    
                File.Delete(Path.Combine(report.summary.outputPath, zipName));
            }


            //zip the two folders "Build" and "TemplateData" to "build.zip" here
            var outputPath = getZipPath(report.summary.outputPath);

            ZipUtil.ZipFoldersAndFiles(outputPath, "index.html", _folders);

            if (File.Exists(outputPath))
                EditorUtility.RevealInFinder(outputPath);
        }
    }
}
#endif