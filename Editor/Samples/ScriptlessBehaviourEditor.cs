using FantasyArts.Tools.Editor.Helpers;
using FantasyArts.Tools.Runtime.Samples;
using UnityEditor;
using UnityEngine;

namespace FantasyArts.Tools.Editor.Samples
{
    [CustomEditor(typeof(ScriptlessBehaviour))]
    public class ScriptlessBehaviourEditor : ScriptlessEditor { }
}