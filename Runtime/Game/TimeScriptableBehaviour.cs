using System.Collections;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Game
{
    public class TimeScriptableBehaviour : MonoBehaviour
    {
        [SerializeField] private ScriptableFloat _time;

        [SerializeField] private float _initialValue = 30f;

        [SerializeField] private float _updateTime = 1f;

        [SerializeField] private bool _runAtStart = true;

        public UnityEvent OnTimeOver;

        private Countdown _countdown;

        private void Awake()
        {
            _time.Value = _initialValue;
        }

        private void Start()
        {
            if (_runAtStart) Run();
        }

        public void Run()
        {
            StopAllCoroutines();
            StartCoroutine(Co_Run());
        }

        private IEnumerator Co_Run()
        {
            _countdown = new Countdown(_initialValue);

            var waitForNextRefresh = new WaitForSeconds(_updateTime);

            while (_time.Value > 0f)
            {
                yield return waitForNextRefresh;

                _time.Value = _countdown.Remaining;
            }

            OnTimeOver?.Invoke();
        }
    }
}