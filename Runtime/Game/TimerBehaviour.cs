using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Game
{
    public class TimerBehaviour : MonoBehaviour
    {
        [SerializeField] private float _seconds = 1f;

        [SerializeField] private UnityEvent _timedEvent;

        [SerializeField] private bool _repeat = true;
        

        private IEnumerator Start()
        {
            while (_repeat)
            {
                yield return new WaitForSeconds(_seconds);
                _timedEvent?.Invoke();
            }
        }
    }
}