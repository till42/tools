using System;
using System.Diagnostics;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Game
{
    public class Countdown
    {
        private TimeSpan _timeSpan;

        private Stopwatch _stopwatch;

        public Countdown(float seconds)
        {
            _timeSpan = new TimeSpan(0, 0, 0, 0, Mathf.RoundToInt(seconds * 1000f));
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
        }

        public float Remaining
        {
            get
            {
                var milliseconds = _timeSpan.TotalMilliseconds - _stopwatch.ElapsedMilliseconds;
                return (float) milliseconds / 1000f;
            }
        }
    }
}