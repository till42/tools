using System;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Game
{
    [Serializable]
    public class Hitpoint
    {
        [SerializeField] private int _currentValue;

        public int CurrentValue
        {
            get => _currentValue;
            private set => _currentValue = value;
        }

        [SerializeField] private int _maxValue;

        public int MaxValue
        {
            get => _maxValue;
            private set => _maxValue = value;
        }

        public float Percentage
        {
            get
            {
                if (_maxValue == 0) return 0f;
                var percentage = _currentValue / (float) _maxValue;
                return percentage <= 0 ? 0f : percentage;
            }
        }

        public bool IsDepleted => _currentValue <= 0;

        public event Action<int> OnUpdate;

        public event Action OnDepleted;

        public Hitpoint()
        {
        }

        public Hitpoint(int maxValue)
        {
            MaxValue = Mathf.Max(0, maxValue);
            CurrentValue = MaxValue;
        }

        public void Reset()
        {
            var previousValue = CurrentValue;
            CurrentValue = MaxValue;
            RaiseUpdateEventIfChangedComparedTo(previousValue);
        }

        public void TakeDamage(int amount)
        {
            if (amount <= 0) return;
            var previouslyDepleted = IsDepleted;
            var previousValue = CurrentValue;
            CurrentValue -= amount;
            CurrentValue = Mathf.Max(0, CurrentValue);
            RaiseUpdateEventIfChangedComparedTo(previousValue);
            RaiseDepletedEventIfChangedComparedTo(previouslyDepleted);
        }

        public void SetValue(int value)
        {
            var previouslyDepleted = IsDepleted;
            var previousValue = CurrentValue;
            value = Mathf.Clamp(value, 0, MaxValue);
            CurrentValue = value;
            RaiseUpdateEventIfChangedComparedTo(previousValue);
            RaiseDepletedEventIfChangedComparedTo(previouslyDepleted);
        }

        private void RaiseUpdateEventIfChangedComparedTo(int previousValue)
        {
            if (CurrentValue != previousValue) OnUpdate?.Invoke(CurrentValue);
        }

        private void RaiseDepletedEventIfChangedComparedTo(bool previouslyDepleted)
        {
            if (!IsDepleted) return;
            if (!previouslyDepleted) OnDepleted?.Invoke();
        }

        public void HealBy(int amount)
        {
            if (amount <= 0) return;
            var previousValue = CurrentValue;
            CurrentValue += amount;
            CurrentValue = Mathf.Min(CurrentValue, MaxValue);
            RaiseUpdateEventIfChangedComparedTo(previousValue);
        }
    }
}