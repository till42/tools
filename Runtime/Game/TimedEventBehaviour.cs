using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Game
{
    public class TimedEventBehaviour : MonoBehaviour
    {
        [SerializeField] private float _seconds = 1f;

        [SerializeField] private bool _repeat;

        [SerializeField] private UnityEvent _timedEvent;

        public void Run()
        {
            StartCoroutine(Co_Run(_seconds));
        }

        public void Run(float seconds)
        {
            StartCoroutine(Co_Run(seconds));
        }

        public void StopAndRun()
        {
            StopAllCoroutines();
            Run();
        }

        public void StopAndRun(float seconds)
        {
            StopAllCoroutines();
            Run(seconds);
        }

        private IEnumerator Co_Run(float seconds)
        {
            do
            {
                yield return new WaitForSeconds(seconds);
                _timedEvent?.Invoke();
            } while (_repeat);
        }
    }
}