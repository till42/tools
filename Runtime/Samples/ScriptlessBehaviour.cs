using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Samples
{
    public class ScriptlessBehaviour : MonoBehaviour
    {
        [SerializeField] private int _sampleInt;
        public UnityEvent SampleEvent;
    }
}