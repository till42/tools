using UnityEngine;
using UnityEngine.EventSystems;

namespace FantasyArts.Tools.UI
{
    public class ReceivePointerBehaviour : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private CursorSystem _cursorSystem;

        [SerializeField] private PointerEvent[] _onClicked;

        [SerializeField] private bool _reactIfDisabled;

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!_reactIfDisabled && _cursorSystem.IsDisabled)
                return;
            _onClicked.InvokeWithPointer(CurrentPointer);
        }

        private Pointer CurrentPointer => _cursorSystem.Pointer;
    }
}