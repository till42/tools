using UnityEngine;

namespace FantasyArts.Tools.UI
{
    [CreateAssetMenu(menuName = "FantasyArts/UI/Pointer",
        fileName = "Pointer")]
    public class Pointer : ScriptableObject
    {
        [SerializeField] private Texture2D _texture;
        public Texture2D Texture => _texture;

        [SerializeField] private Vector2 _hotspot;
        public Vector2 Hotspot => _hotspot;
    }
}