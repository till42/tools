﻿using System.Linq;
using UnityEngine.Events;

namespace FantasyArts.Tools.UI
{
    public static class PointerEventExtensions
    {
        public static void InvokeWithPointer(this PointerEvent[] pointerEvents, Pointer pointer)
        {
            if (pointerEvents.TryGetUnityEvent(pointer, out var unityEvent)) unityEvent?.Invoke();

            if (pointerEvents.TryGetUnityEvent(null, out var unityEventDefault)) unityEventDefault?.Invoke();
        }

        private static bool TryGetUnityEvent(this PointerEvent[] pointerEvents, Pointer pointer,
            out UnityEvent unityEvent)
        {
            unityEvent = default;
            var pointerEvent = pointerEvents.FirstOrDefault(p => p.Pointer == pointer);
            if (pointerEvent == null) return false;
            unityEvent = pointerEvent.UnityEvent;
            return true;
        }
    }
}