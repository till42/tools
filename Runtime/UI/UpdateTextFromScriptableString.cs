using System;
using System.Collections;
using System.Collections.Generic;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using TMPro;
using UnityEngine;

namespace FantasyArts.Tools.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class UpdateTextFromScriptableString : MonoBehaviour
    {
        [SerializeField] private ScriptableString _scriptableString;

        protected TMP_Text _tmpText;

        private void Awake()
            => _tmpText = GetComponent<TMP_Text>();

        private void OnEnable()
        {
            _scriptableString.RegisterUpdateAction(HandleUpdate);
            HandleUpdate(_scriptableString.Value);
        }

        protected virtual void HandleUpdate(string value)
            => _tmpText.text = _scriptableString.Value;

        private void OnDisable()
            => _scriptableString.RegisterUpdateAction(HandleUpdate);
    }
}