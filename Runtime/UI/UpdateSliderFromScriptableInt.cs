using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;
using UnityEngine.UI;

namespace FantasyArts.Tools.UI
{
    [RequireComponent(typeof(Slider))]
    public class UpdateSliderFromScriptableInt : MonoBehaviour
    {
        [SerializeField] private ScriptableInt _scriptableInt;

        private Slider _slider;

        private void Awake()
            => _slider = GetComponent<Slider>();

        private void OnEnable()
            => _scriptableInt.RegisterUpdateAction(HandleUpdate);

        private void HandleUpdate(int value)
            => _slider.value = _scriptableInt;

        private void OnDisable()
            => _scriptableInt.RegisterUpdateAction(HandleUpdate);
    }
}