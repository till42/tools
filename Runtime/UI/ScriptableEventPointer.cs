﻿using System;
using FantasyArts.Tools.UI;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableEventPointer",
        fileName = "New ScriptableEventPointer")]
    [Serializable]
    public class ScriptableEventPointer : ScriptableEventT<Pointer> { }
}