using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using TMPro;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class UpdateTextFromScriptableFloat : MonoBehaviour
    {
        [SerializeField] private ScriptableFloat _scriptableFloat;
        [SerializeField] private string _format;

        protected TMP_Text _tmpText;

        private void Awake() => _tmpText = GetComponent<TMP_Text>();

        private void OnEnable() => _scriptableFloat.RegisterUpdateAction(HandleUpdate);

        protected virtual void HandleUpdate(float value) => _tmpText.text = _scriptableFloat.Value.ToString(_format);

        private void OnDisable() => _scriptableFloat.RegisterUpdateAction(HandleUpdate);
    }
}