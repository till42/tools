using FantasyArts.Tools.Runtime.Architecture.Scriptables;

namespace FantasyArts.Tools.UI
{
    public class ScriptableEventPointerListenBehaviour : ScriptableEventTListenBehaviour<Pointer> { }
}