using System;
using System.Collections;
using System.Collections.Generic;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using TMPro;
using UnityEngine;

namespace FantasyArts.Tools.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class UpdateTextFromScriptableBool : MonoBehaviour
    {
        [SerializeField] private ScriptableBool _scriptableBool;

        protected TMP_Text _tmpText;

        private void Awake() => _tmpText = GetComponent<TMP_Text>();

        private void OnEnable()
        {
            _scriptableBool.RegisterUpdateAction(HandleUpdate);
            HandleUpdate(_scriptableBool.Value);
        }

        protected virtual void HandleUpdate(bool value)
        {
            _tmpText.text = _scriptableBool.ToString();
        }

        private void OnDisable() => _scriptableBool.UnregisterUpdateAction(HandleUpdate);
    }
}