using System;
using System.Collections;
using System.Collections.Generic;
using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using TMPro;
using UnityEngine;

namespace FantasyArts.Tools.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class UpdateTextFromScriptableInt : MonoBehaviour
    {
        [SerializeField] private ScriptableInt _scriptableInt;

        protected TMP_Text _tmpText;

        private void Awake() => _tmpText = GetComponent<TMP_Text>();

        private void OnEnable()
        {
            _scriptableInt.RegisterUpdateAction(HandleUpdate);
            HandleUpdate(_scriptableInt.Value);
        }

        protected virtual void HandleUpdate(int value)
        {
            _tmpText.text = _scriptableInt.ToString();
        }

        private void OnDisable() => _scriptableInt.RegisterUpdateAction(HandleUpdate);
    }
}