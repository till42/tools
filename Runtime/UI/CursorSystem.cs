using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;

namespace FantasyArts.Tools.UI
{
    [CreateAssetMenu(menuName = "FantasyArts/UI/Cursor System",
        fileName = "New Cursor System")]
    public class CursorSystem : ScriptableEventPointer
    {
        [SerializeField] private CursorMode _cursorMode;

        [SerializeField] private Pointer _defaultPointer;

        [SerializeField] private Pointer _disablePointer;

        private Pointer _pointer;
        public Pointer Pointer => _pointer;

        public bool IsDisabled { get; private set; }

        public void Reset()
            => SetPointer(_defaultPointer);

        public void SetPointer(Pointer pointer)
        {
            _pointer = pointer;
            Cursor.SetCursor(pointer.Texture, pointer.Hotspot, _cursorMode);
            IsDisabled = _pointer == _disablePointer;
            Raise(_pointer);
        }
    }
}