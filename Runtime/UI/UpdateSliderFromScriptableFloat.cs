using FantasyArts.Tools.Runtime.Architecture.Scriptables;
using UnityEngine;
using UnityEngine.UI;

namespace FantasyArts.Tools.UI
{
    [RequireComponent(typeof(Slider))]
    public class UpdateSliderFromScriptableFloat : MonoBehaviour
    {
        [SerializeField] private ScriptableFloat _scriptableFloat;

        private Slider _slider;

        private void Awake() => _slider = GetComponent<Slider>();

        private void OnEnable() => _scriptableFloat.RegisterUpdateAction(HandleUpdate);

        private void HandleUpdate(float value) => _slider.value = _scriptableFloat;

        private void OnDisable() => _scriptableFloat.RegisterUpdateAction(HandleUpdate);
    }
}