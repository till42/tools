using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FantasyArts.Tools.Runtime.UI
{
    public class DragMove2dBehaviour : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IDragHandler,
        IEndDragHandler, IPointerUpHandler
    {
        [SerializeField] protected Transform _targetTransform;

        [SerializeField] protected Camera _camera;

        public UnityEvent PointerDown;

        public UnityEvent BeginDrag;

        public UnityEvent EndDrag;

        public UnityEvent PointerUp;

        protected PositionUpdater _positionUpdater;

        protected virtual void Awake()
        {
            var target = _targetTransform != null ? _targetTransform : transform;
            var cam = _camera != null ? _camera : Camera.main;
            _positionUpdater = new PositionUpdater(target, cam);
        }

        public virtual void OnPointerDown(PointerEventData eventData)
        {
            _positionUpdater.Begin(eventData.position);
            PointerDown?.Invoke();
        }

        public virtual void OnBeginDrag(PointerEventData eventData) => BeginDrag?.Invoke();

        public virtual void OnDrag(PointerEventData eventData) => _positionUpdater.Update(eventData.position);

        public virtual void OnEndDrag(PointerEventData eventData)
        {
            _positionUpdater.Update(eventData.position);
            EndDrag?.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _positionUpdater.Update(eventData.position);
            PointerUp?.Invoke();
        }

        protected class PositionUpdater
        {
            protected Transform _target;
            protected Camera _camera;

            protected Vector3 _beginTargetPosition;
            protected Vector3 _beginPointerPosition;

            protected float _cameraDistance;

            public PositionUpdater(Transform target, Camera camera)
            {
                _target = target;
                _camera = camera;
                _cameraDistance = -_camera.transform.position.z;
            }

            public virtual void Begin(Vector2 screenPosition)
            {
                _beginTargetPosition = _target.position;
                _beginPointerPosition = WorldPosition(screenPosition);
            }

            public virtual void Update(Vector2 screenPosition)
            {
                var worldDelta = WorldPosition(screenPosition) - _beginPointerPosition;
                SetTargetPosition(worldDelta);
            }

            protected virtual void SetTargetPosition(Vector3 worldDelta) =>
                _target.position = _beginTargetPosition + worldDelta;

            protected Vector3 WorldPosition(Vector2 position)
            {
                var screenPoint = ScreenPoint(position);
                return _camera.ScreenToWorldPoint(screenPoint);
            }

            protected Vector3 ScreenPoint(Vector2 position) => new Vector3(position.x, position.y, _cameraDistance);
        }
    }
}