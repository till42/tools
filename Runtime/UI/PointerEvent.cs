﻿using System;
using UnityEngine.Events;

namespace FantasyArts.Tools.UI
{
    [Serializable]
    public class PointerEvent
    {
        public Pointer Pointer;
        public UnityEvent UnityEvent;
    }
}