using UnityEngine;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;

#endif

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public class StateMachineInstantiateBehaviour : MonoBehaviour, IStateMachineProvider, IStateMachine
    {
        [SerializeField] private StateMachineScriptable _stateMachinePrefab;

#if ODIN_INSPECTOR
        [ShowInInspector, InlineEditor]
#endif
        private StateMachineScriptable _instance;

        public StateMachineScriptable Instance
        {
            get
            {
                if (_instance == null) Reset();
                return _instance;
            }
        }

#if ODIN_INSPECTOR
        [Button]
#endif
        public void Reset()
        {
            if (_stateMachinePrefab == null)
            {
                Debug.LogError($"Could not reset {this} because State Machine Prefab is not valid");
                return;
            }

            _instance = _stateMachinePrefab.Clone();
        }

        private void Start()
        {
            Instance.Initialize();
        }

        public void SetTrigger(string trigger)
        {
            Instance.SetTrigger(trigger);
        }
    }
}