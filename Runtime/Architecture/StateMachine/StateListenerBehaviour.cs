using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using static UnityEngine.SceneManagement.SceneManager;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public class StateListenerBehaviour : MonoBehaviour
    {
        [SerializeField] protected StateScriptable _state;
        public virtual StateScriptable State => _state;

        public StateMachineScriptable StateMachine => State == null ? null : State.StateMachine;

        [FormerlySerializedAs("_doSetGameObjectActiveWithState")] [SerializeField]
        protected bool _doActivateWithState = true;

        [SerializeField] protected bool _doEnableOnEnter;
        [SerializeField] protected bool _doDisableOnExit;

        [SerializeField] private bool _doLogEnter;

        [SerializeField] protected bool _hasEvents = false;

        [SerializeField] protected bool _doLogInitialize;

        public UnityEvent OnInitialize;

        public UnityEvent OnEnter;

        [SerializeField] private bool _doLogExit;

        public UnityEvent OnExit;

        protected bool _isInitialized;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void InitAll()
        {
            sceneLoaded -= InitializeStateListeners; // make sure we do not register more than once
            sceneLoaded += InitializeStateListeners;
            var scene = GetActiveScene();
            InitializeStateListeners(scene, LoadSceneMode.Single);
        }

        private static void InitializeStateListeners(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
        {
            var listeners = scene.StateListenerBehaviours();
            listeners.ForEach(l => l.Initialize());
        }

        private void OnEnable()
        {
            Initialize();
        }

        private void Initialize()
        {
            if (!enabled) return;

            if (_isInitialized) return;

            _isInitialized = true;

            FirstTimeInitialization();
        }

        protected virtual void FirstTimeInitialization()
        {
            if (!StateMachine.IsInitialized) StateMachine.Initialize();

            if (_doActivateWithState || _doEnableOnEnter) SetActive(false);
            AddListeners();

            if (_doLogInitialize)
                Debug.Log($"OnInitialize Listener {name} on State={State}");

            if (_hasEvents) OnInitialize?.Invoke();

            if (State == null)
            {
                Debug.LogError($"State is not set in {gameObject.name}", this);
                return;
            }

            if (State.IsCurrent) Enter();
        }

        protected virtual void SetActive(bool isActive) => gameObject.SetActive(isActive);

        protected void AddListeners()
        {
            if (State == null) return;

            State.OnEnter.RemoveListener(Enter);
            State.OnEnter.AddListener(Enter);

            State.OnUpdate.RemoveListener(StateUpdate);
            State.OnUpdate.AddListener(StateUpdate);

            State.OnExit.RemoveListener(Exit);
            State.OnExit.AddListener(Exit);
        }

        protected virtual void Enter()
        {
            if (_doActivateWithState || _doEnableOnEnter) SetActive(true);

            if (_doLogEnter) Debug.Log($"OnEnter State {name}");

            if (!_hasEvents) return;

            OnEnter?.Invoke();
        }

        protected virtual void StateUpdate() { }

        protected virtual void Exit()
        {
            if (_doActivateWithState || _doDisableOnExit) SetActive(false);

            if (_doLogExit) Debug.Log($"OnExit State {name}");

            if (!_hasEvents) return;

            OnExit?.Invoke();
        }

        protected void RemoveListeners()
        {
            if (State == null) return;

            State.OnEnter.RemoveListener(Enter);
            State.OnUpdate.RemoveListener(StateUpdate);
            State.OnExit.RemoveListener(Exit);
        }

        private void OnDestroy()
        {
            RemoveListeners();
        }
    }
}