using System;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public class StateMachineTriggerBehaviour : MonoBehaviour
    {
        [SerializeField] private string _trigger;

        [SerializeField] private StateMachineScriptable _stateMachine;

        [ContextMenu("Trigger")]
        public void Trigger()
        {
            _stateMachine.SetTrigger(_trigger);
        }
    }
}