using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public static class SceneExtensions
    {
        public static List<StateListenerBehaviour> StateListenerBehaviours(this UnityEngine.SceneManagement.Scene scene)
        {
            var rootGameObjects = scene.GetRootGameObjects();
            var listeners = new List<StateListenerBehaviour>();
            foreach (var rootGameObject in rootGameObjects)
            {
                var rootListeners =
                    rootGameObject.GetComponentsInChildren<StateListenerBehaviour>(true);
                listeners.AddRange(rootListeners);
            }

            //when the scene is first loaded, the root GameObjects are empty, but FindObjectsOfType will return the active gameObjects
            var listenersOfType = GameObject.FindObjectsOfType<StateListenerBehaviour>();
            listeners.AddRange(listenersOfType);
            listeners = listeners.Distinct().ToList();

            return listeners;
        }
    }
}