using System;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public static class TriggerScriptableExtensions
    {
        public static bool HasName(this TriggerScriptable triggerScriptable, string name)
            => triggerScriptable != null &&
               string.Equals(triggerScriptable.Name, name, comparisonType: StringComparison.Ordinal);
    }
}