#if ODIN_INSPECTOR
using Sirenix.OdinInspector;

#endif

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public class StateInstanceListenerBehaviour : StateListenerBehaviour
    {
        private int StateIndex => _state.StateMachine.GetIndex(_state);

        public override StateScriptable State
        {
            get
            {
                if (_stateInstance != null) return _stateInstance;
                var index = StateIndex;
                if (StateMachineInstance == null)
                    return _stateInstance;
                _stateInstance = StateMachineInstance.States[index];
                return _stateInstance;
            }
        }

        private StateScriptable _stateInstance;

        private StateMachineScriptable _stateMachineScriptable;

#if ODIN_INSPECTOR
        [ShowInInspector, ReadOnly]
#endif
        private StateMachineScriptable StateMachineInstance
        {
            get
            {
                if (_stateMachineScriptable != null) return _stateMachineScriptable;

                var stateMachineProvider = gameObject.GetComponentInParent<IStateMachineProvider>();
                if (stateMachineProvider != null) _stateMachineScriptable = stateMachineProvider.Instance;

                return _stateMachineScriptable;
            }
        }
    }
}