using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    [CreateAssetMenu(menuName = "FantasyArts/State Machine/State Machine Trigger (scriptable)")]
    public class TriggerScriptable : ScriptableObject
    {
        public string Name
        {
            get => name;
            private set => name = value;
        }
    }
}