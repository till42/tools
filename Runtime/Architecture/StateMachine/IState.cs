﻿namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public interface IState
    {
        string Name { get; }

        void Initialize();
        void Enter();
        void Update();
        void Exit();

        void AddTransitionTo(IState state, params string[] triggers);
        bool TryGetNextState(string trigger, out IState nextState);
        string ToString();

        ITrigger[] Triggers { get; }
    }
}