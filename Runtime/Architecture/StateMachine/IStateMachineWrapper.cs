﻿namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public interface IStateMachineWrapper
    {
        void SetTrigger(string trigger);
    }
}