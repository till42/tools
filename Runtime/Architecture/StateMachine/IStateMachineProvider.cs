﻿namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public interface IStateMachineProvider
    {
        StateMachineScriptable Instance { get; }
    }
}