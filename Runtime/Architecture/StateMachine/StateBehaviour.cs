﻿using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public class StateBehaviour : MonoBehaviour
    {
        [SerializeField] private bool _deactivateOnExit = true;

        public UnityEvent OnInitialize;
        public UnityEvent OnEnter;
        public UnityEvent OnExit;

        public virtual void Initialize(State state)
        {
            gameObject.SetActive(false);

            state.OnEnter -= Enter;
            state.OnEnter += Enter;

            state.OnExit -= Exit;
            state.OnExit += Exit;

            OnInitialize?.Invoke();
        }

        public virtual void Enter()
        {
            gameObject.SetActive(true);
            OnEnter?.Invoke();
        }

        public virtual void Exit()
        {
            if (_deactivateOnExit)
                gameObject.SetActive(false);
            OnExit?.Invoke();
        }
    }
}