﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public class State : IState
    {
        public string Name { get; }

        public State(string name) => Name = name;

        public event Action OnInitialize;
        public event Action OnEnter;
        public event Action OnUpdate;
        public event Action OnExit;

        public Dictionary<string, IState> Transitions { get; private set; } = new Dictionary<string, IState>();

        public void Initialize() => OnInitialize?.Invoke();


        public void Enter() => OnEnter?.Invoke();

        public void Update() => OnUpdate?.Invoke();

        public void Exit() => OnExit?.Invoke();

        public bool TryGetNextState(string trigger, out IState nextState) =>
            Transitions.TryGetValue(trigger, out nextState);

        public void AddTransitionTo(IState state, params string[] triggers)
        {
            foreach (var t in triggers)
            {
                Transitions.Add(t, state);
            }
        }

        public void AddAnimatedTransitionTo(IState state, Action animationAction, params string[] trigger)
        {
            var viaState = new State($"{Name}To{state.Name}");
            AddTransitionTo(viaState, trigger);
            viaState.OnEnter += animationAction;
            viaState.AddTransitionTo(state, "OnCompleted");
        }

        public override string ToString() => Name;

        public ITrigger[] Triggers => Transitions.Keys.Select(x => new ITrigger.StringTrigger(x)).ToArray();

        public List<string> GetTransitionStrings()
            => Transitions.Select(t => $"{Name}->[{t.Key}]->{t.Value.ToString()}")
                .ToList();
    }
}