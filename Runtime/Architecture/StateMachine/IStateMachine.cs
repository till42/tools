﻿namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public interface IStateMachine
    {
        void SetTrigger(string trigger);
    }
}