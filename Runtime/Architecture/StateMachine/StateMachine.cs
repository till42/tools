﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public class StateMachine : IStateMachine
    {
        public IState CurrentState { get; private set; }

        private Dictionary<Func<bool>, IState> Transitions { get; } = new Dictionary<Func<bool>, IState>();

        public void Start(IState state)
        {
            CurrentState = state;
            CurrentState?.Enter();
        }

        public void Update()
        {
            CurrentState?.Update();
            CheckTriggers();
        }

        public void SetTrigger(string trigger)
        {
            if (CurrentState == null || !CurrentState.TryGetNextState(trigger, out IState nextState)) return;
            SwitchState(nextState, trigger);
        }

        private void SwitchState(IState state, string trigger = null)
        {
            CurrentState?.Exit();
            CurrentState = state;
            CurrentState?.Enter();
        }

        protected virtual void CheckTriggers()
        {
            foreach (var keyValuePair in Transitions)
            {
                var condition = keyValuePair.Key;
                var state = keyValuePair.Value;
                if (!condition()) continue;
                SwitchState(state, "machine trigger");
            }
        }

        public void AddTriggerFromAnyState(Func<bool> condition, IState states)
        {
            Transitions.Add(condition, states);
        }
    }
}