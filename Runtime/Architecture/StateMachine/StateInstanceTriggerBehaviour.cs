using UnityEngine;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public class StateInstanceTriggerBehaviour : MonoBehaviour
    {
        [SerializeField] private string _trigger;

        private StateMachineScriptable _stateMachine;

        private StateMachineScriptable StateMachine
        {
            get
            {
                if (_stateMachine == null)
                    _stateMachine = GetComponentInParent<IStateMachineProvider>()?.Instance;
                return _stateMachine;
            }
        }

#if ODIN_INSPECTOR
        [Button]
#endif
        public void SetTrigger()
        {
            SetTrigger(_trigger);
        }
#if ODIN_INSPECTOR
        [Button]
#endif
        public void SetTrigger(string trigger)
        {
            StateMachine.SetTrigger(trigger);
        }
    }
}