using System;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    [Serializable]
    public class TransitionToScriptable
    {
        public string TriggerName => _triggerScriptable != default ? _triggerScriptable.Name : _trigger;

        [SerializeField] private string _trigger;

        public string Trigger
        {
            get => _trigger;
            private set => _trigger = value;
        }

        [SerializeField] private TriggerScriptable _triggerScriptable;

        [SerializeField] private StateScriptable _toState;

        public StateScriptable ToState
        {
            get => _toState;
            private set => _toState = value;
        }

        public TransitionToScriptable() { }

        public TransitionToScriptable(string trigger, StateScriptable toState)
        {
            _trigger = trigger;
            _toState = toState;
        }

        public TransitionToScriptable(TriggerScriptable triggerScriptable, StateScriptable toState)
        {
            _triggerScriptable = triggerScriptable;
            _toState = toState;
        }

        public bool TryGetState(string trigger, out IState state)
        {
            state = default;

            if (trigger != _trigger && !_triggerScriptable.HasName(trigger)) return false;

            state = _toState;
            return true;
        }

        public void SwitchStateMachine(StateMachineScriptable sourceMachine, StateMachineScriptable targetMachine)
        {
            var index = sourceMachine.GetIndex(_toState);
            if (index == -1 || index >= targetMachine.States.Count) return;
            _toState = targetMachine.States[index];
        }
    }
}