using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    [CreateAssetMenu(menuName = "FantasyArts/State Machine/StateMachine (scriptable)")]
    public class StateMachineScriptable : ScriptableObject, IStateMachine
    {
        [SerializeField] private List<StateScriptable> _states = new();
        public IReadOnlyList<StateScriptable> States => _states;

        public IState CurrentState { get; private set; }

        public bool IsInitialized => CurrentState != default;

        public void Initialize()
        {
            InitializeAllStates();
            var firstState = _states.FirstOrDefault();
            SwitchTo(firstState, isAllowingSelfReference: false);
        }

        [ContextMenu("Update")]
        public void Update()
        {
            CurrentState?.Update();
        }

        public void SetTrigger(string trigger)
        {
            if (!IsInitialized)
            {
                Debug.LogError($"State Machine ({name}) is not initialized when setting trigger ({trigger})", this);
                return;
            }

            if (!CurrentState.TryGetNextState(trigger, out IState nextState)) return;
            SwitchTo(nextState);
        }

        public void SetTrigger(TriggerScriptable triggerScriptable)
            => SetTrigger(triggerScriptable.Name);

        public void SetTrigger(ITrigger trigger)
            => SetTrigger(trigger.Name);

        private void InitializeAllStates()
        {
            foreach (IState state in _states) state.Initialize();
        }

        public void Reset()
        {
            CurrentState = default;
        }

        private void SwitchTo(IState toState, bool isAllowingSelfReference = true)
        {
            if (!isAllowingSelfReference && CurrentState == toState) return;
            CurrentState?.Exit();
            CurrentState = toState;
            CurrentState?.Enter();
        }

        public StateMachineScriptable Clone()
        {
            var stateMachineClone = Instantiate(this);
            stateMachineClone._states.Clear();
            foreach (var state in States)
            {
                var stateClone = StateScriptable.CreateClone(stateMachineClone, state);
                stateMachineClone._states.Add(stateClone);
            }

            foreach (var state in stateMachineClone.States)
            {
                state.UpdateTransitionsFrom(this);
            }

            return stateMachineClone;
        }

        public int GetIndex(StateScriptable state)
        {
            for (int i = 0; i < States.Count; i++)
            {
                var s = States[i];
                if (s == state) return i;
            }

            return -1;
        }

#if UNITY_EDITOR
        public StateScriptable AppendNewStateScriptable(string stateName)
        {
            var index = _states.Count;
            return CreateStateScriptable(stateName, index);
        }

        public StateScriptable CreateStateScriptable(string stateName, int index)
        {
            var state = StateScriptable.CreateInstance(this);
            state.Index = index;
            state.Name = stateName;

            _states.Add(state);
            return state;
        }


        public StateScriptable CreateNestedStateScriptable(string stateName, int index)
        {
            var state = CreateStateScriptable(stateName, index);

            EditorUtility.SetDirty(this);

            AssetDatabase.AddObjectToAsset(state, this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            return state;
        }

        public bool RemoveNestedStateScriptable(int index)
        {
            var state = _states.ElementAtOrDefault(index);
            if (state == default) return false;
            _states.Remove(state);
            DestroyImmediate(state, true);
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return true;
        }

        public bool ButtonInitialize(string label)
        {
            if (!GUILayout.Button(label)) return false;

            Initialize();
            return true;
        }

        public bool ButtonReset(string label)
        {
            if (!GUILayout.Button(label)) return false;

            Reset();
            return true;
        }

#endif
    }
}