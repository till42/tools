﻿namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public interface ITrigger
    {
        string Name { get; }

        public class StringTrigger : ITrigger
        {
            public string Name => _name;
            private readonly string _name;

            public StringTrigger(string name)
            {
                _name = name;
            }
        }
    }
}