using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    [DefaultExecutionOrder(-1000)]
    public class SceneStateMachineBehaviour : MonoBehaviour
    {
        [SerializeField] private StateMachineScriptable _stateMachine;
        public StateMachineScriptable StateMachine => _stateMachine;
        [SerializeField] private bool _doInitializeOnEnable = true;
        [SerializeField] private bool _doResetOnDestroy = true;

        private void OnEnable()
        {
            if (!_doInitializeOnEnable) return;
            Initialize();
        }

        [ContextMenu("Initialize")]
        public void Initialize()
        {
            StateListenerBehaviour.InitAll();
            _stateMachine.Initialize();
        }

        [ContextMenu("Reset")]
        public void Reset()
        {
            _stateMachine.Reset();
        }

        private void OnDestroy()
        {
            if (!_doResetOnDestroy) return;
            Reset();
        }
    }
}