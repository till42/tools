using System.Collections.Generic;
using FantasyArts.Tools.Runtime.Architecture.Extensions;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Architecture.StateMachine
{
    public class StateScriptable : ScriptableObject, IState
    {
        [SerializeField] private StateMachineScriptable _stateMachine;
        public StateMachineScriptable StateMachine => _stateMachine;

        [SerializeField] private bool _doLogInitialize;
        public UnityEvent OnInitialize;
        [SerializeField] private bool _doLogEnter;
        public UnityEvent OnEnter;
        [SerializeField] private bool _doLogUpdate;
        public UnityEvent OnUpdate;
        [SerializeField] private bool _doLogExit;
        public UnityEvent OnExit;

        [SerializeField] private List<TransitionToScriptable> _transitions = new List<TransitionToScriptable>();
        public List<TransitionToScriptable> Transitions => _transitions;

        public bool IsCurrent => (StateScriptable)_stateMachine.CurrentState == this;

        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name)) _name = name.TrimOrdinalPrefix();
                return _name;
            }
#if UNITY_EDITOR
            set
            {
                _name = value;
                RefreshName();
            }
#endif
        }

        public int Index
        {
            get => _index;
#if UNITY_EDITOR
            set
            {
                _index = value;
                RefreshName();
            }
#endif
        }

        private string _name;
        private int _index;
        private Dictionary<string, IState> _runtimeTransitions = new Dictionary<string, IState>();

#if UNITY_EDITOR
        private void RefreshName()
        {
            name = _name.AddOrdinalPrefix(Index);
            var path = AssetDatabase.GetAssetPath(this);
            if (string.IsNullOrEmpty(path)) return;
            AssetDatabase.SaveAssets();
            AssetDatabase.ImportAsset(path);
        }
#endif

        public virtual void Initialize()
        {
            if (_doLogInitialize) Debug.Log($"OnInitialize State {name}");
            OnInitialize?.Invoke();
        }

        public virtual void Enter()
        {
            if (_doLogEnter) Debug.Log($"OnEnter State {name}");

            OnEnter?.Invoke();
        }

        public virtual void Update()
        {
            if (_doLogUpdate)
                Debug.Log($"OnUpdate State {name}");

            OnUpdate?.Invoke();
        }

        public void Exit()
        {
            if (_doLogExit)
                Debug.Log($"OnExit State {name}");

            OnExit?.Invoke();
        }

        public void AddTransitionTo(IState state, params string[] triggers)
        {
            foreach (var trigger in triggers) _runtimeTransitions.Add(trigger, state);
        }

        public bool TryGetNextState(string trigger, out IState nextState)
        {
            foreach (var transition in _transitions)
                if (transition.TryGetState(trigger, out nextState))
                    return true;

            if (_runtimeTransitions.ContainsKey(trigger))
            {
                nextState = _runtimeTransitions[trigger];
                return true;
            }

            nextState = null;
            return false;
        }

        public ITrigger[] Triggers
        {
            get
            {
                var triggers = new List<ITrigger.StringTrigger>();
                foreach (var transition in _transitions)
                {
                    triggers.Add(new ITrigger.StringTrigger(transition.Trigger));
                }

                return triggers.ToArray();
            }
        }

        public static StateScriptable CreateInstance(StateMachineScriptable stateMachine)
        {
            var instance = CreateInstance<StateScriptable>();
            instance._stateMachine = stateMachine;
            return instance;
        }

        public static StateScriptable CreateClone(StateMachineScriptable stateMachine, StateScriptable original)
        {
            var instance = Instantiate(original);
            instance._stateMachine = stateMachine;
            return instance;
        }

        public override string ToString() => Name;

        public void UpdateTransitionsFrom(StateMachineScriptable original)
        {
            foreach (var transition in _transitions)
            {
                transition.SwitchStateMachine(original, _stateMachine);
            }
        }
    }
}