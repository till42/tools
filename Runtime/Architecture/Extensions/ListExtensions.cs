﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Extensions
{
    public static class ListExtensions
    {
        /// <summary>
        /// pick a random item from the list
        /// </summary>
        /// <typeparam name="T">type of the list</typeparam>
        /// <param name="list">list of type T</param>
        /// <returns>one random element</returns>
        public static T PickRandomItem<T>(this List<T> list)
        {
            if (list == null || list.Count <= 0)
                return default;

            var index = Random.Range(0, list.Count);
            return list[index];
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            for (var i = list.Count; i > 0; i--)
            {
                list.Swap(0, Random.Range(0, i));
            }
        }


        public static void Swap<T>(this IList<T> list, int i, int j)
        {
            var temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
    }
}