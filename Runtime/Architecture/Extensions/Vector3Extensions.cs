using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Extensions
{
    public static class Vector3Extensions
    {
        public static bool IsInfinite(this Vector3 vector3)
        {
            if (float.IsInfinity(vector3.x)) return true;
            if (float.IsInfinity(vector3.y)) return true;
            if (float.IsInfinity(vector3.z)) return true;
            return false;
        }
    }
}