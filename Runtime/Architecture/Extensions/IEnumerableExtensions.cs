using System.Collections.Generic;
using System.Linq;

namespace FantasyArts.Tools.Runtime.Architecture.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> Duplicate<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.ToList();
        }
    }
}