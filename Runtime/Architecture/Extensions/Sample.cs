using System.Collections.Generic;

namespace FantasyArts.Tools.Runtime.Architecture.Extensions
{
    public class Sample<T>
    {
        private readonly IEnumerable<T> _enumerable;

        private List<T> _samples;

        public Sample(IEnumerable<T> enumerable)
        {
            _enumerable = enumerable;
            _samples = new List<T>(enumerable);
        }

        public bool DrawWithoutReplacement(out T item, bool loopSamples = false)
        {
            item = default;
            if (_samples.Count <= 0 && !loopSamples)
                return false;

            if (_samples.Count <= 0 && loopSamples)
                _samples = new List<T>(_enumerable);

            item = _samples.PickRandomItem();
            _samples.Remove(item);
            return true;
        }
    }

    public static class SampleExtensions
    {
        public static Sample<T> CreateSample<T>(this IEnumerable<T> enumerable) => new Sample<T>(enumerable);
    }
}