using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FantasyArts.Tools.Runtime.Architecture.Extensions
{
    public static class ComponentExtensions
    {
        public static T GetComponentInScene<T>(bool includeInactive = false)
        {
            var scene = SceneManager.GetActiveScene();
            var rootObjects = scene.GetRootGameObjects();
            foreach (var rootObject in rootObjects)
            {
                var component = rootObject.GetComponentInChildren<T>(includeInactive);
                if (component != null)
                    return component;
            }

            return default;
        }

        public static T[] GetComponentsInScene<T>(bool includeInactive = false)
        {
            var result = new List<T>();
            var scene = SceneManager.GetActiveScene();
            var rootObjects = scene.GetRootGameObjects();
            foreach (var rootObject in rootObjects)
            {
                var components = rootObject.GetComponentsInChildren<T>(includeInactive);
                if (components == null || components.Length <= 0)
                    continue;
                result.AddRange(components);
            }

            return result.ToArray();
        }
    }
}