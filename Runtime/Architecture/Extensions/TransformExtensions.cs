using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Extensions
{
    public static class TransformExtensions
    {
        public static void DestroyAllChildren(this Transform transform)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                var child = transform.GetChild(i);
                if (Application.isEditor)
                    GameObject.DestroyImmediate(child.gameObject);
                else
                    GameObject.Destroy(child.gameObject);
            }
        }
    }
}