using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Extensions
{
    public static class IntExtensions
    {
        public static string EmptyIfZero(this int value) => value == 0 ? "" : value.ToString();
    }
}