using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Extensions
{
    public static class ArrayExtensions
    {
        public static T PickRandomItem<T>(this T[] array)
        {
            if (array == null || array.Length <= 0) return default;

            var index = Random.Range(0, array.Length);
            return array[index];
        }

        public static T GetRollingElementAt<T>(this T[] array, int index)
        {
            if (array == default) return default;
            if (array.Length <= 0) return default;

            int i = index % array.Length;

            return array[i];
        }
    }
}