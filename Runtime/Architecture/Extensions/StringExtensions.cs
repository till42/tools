using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Removes a leading ordinal number from a string.
        /// </summary>
        /// <param name="input">A string with (or without) a leading ordinal number, e.g. "04. Item"</param>
        /// <returns>The input string stripped from the leading number. E.g. "Item"</returns>
        public static string TrimOrdinalPrefix(this string input)
        {
            if (string.IsNullOrEmpty(input)) return "";
            const string pattern = @"^\d+\.";
            var match = Regex.Match(input, pattern);
            if (match.Index != 0) return input;
            var matchValue = match.Value;
            var matchChars = matchValue.ToCharArray();
            var trimChars = new List<char>(matchChars) {' '};
            var trimmedInput = input.TrimStart(trimChars.ToArray());
            return trimmedInput;
        }

        /// <summary>
        /// Adds the ordinal
        /// </summary>
        /// <param name="input">A string</param>
        /// <param name="index">index that should be converted into an ordinal prefix</param>
        /// <param name="ordinal">the prefix (5-> "05.")</param>
        /// <param name="leadingZeros">the number of leading zeros for the prefix</param>
        /// <returns>The input string with a ordinal prefix. E.g. "Item", 5 --> "05. Item"</returns>
        public static string AddOrdinalPrefix(this string input, int ordinal, int leadingZeros = 2)
        {
            if (ordinal < 0) return input;
            input = input.TrimOrdinalPrefix();
            if (input != "") input = " " + input;
            var prefix = ordinal.ToString($"D{leadingZeros}") + ".";
            var output = $"{prefix}{input}";
            return output;
        }
    }
}