using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Extensions
{
    public static class FloatExtensions
    {
        public static string AsMinutesAndSeconds(this float value)
        {
            if (value < 0f) value = 0f;
            var minutes = (int) Mathf.Floor(value / 60f);
            value -= minutes * 60;
            var seconds = (int) Mathf.Floor(value);

            return $"{minutes}:{seconds:00}";
        }
    }
}