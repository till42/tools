﻿using System;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    public abstract class ScriptableT<T> : ScriptableObject
    {
        private event Action<T> OnUpdate;

        [SerializeField] protected T _value = default;
        [SerializeField] private bool _resetOnEnable = true;

        public virtual T Value
        {
            get => _value;
            set
            {
                _value = value;
                Validate(ref _value);
                InvokeUpdate();
            }
        }

        protected virtual void Validate(ref T value) { }

        private void OnValidate()
            => InvokeUpdate();

        protected virtual void OnEnable()
        {
            if (!_resetOnEnable) return;
            Reset();
        }

        public abstract void Reset();

        public override string ToString()
            => Value.ToString();

        public void RegisterUpdateAction(Action<T> updateAction)
        {
            UnregisterUpdateAction(updateAction);
            OnUpdate += updateAction;
            updateAction?.Invoke(Value);
        }

        public void UnregisterUpdateAction(Action<T> updateAction)
            => OnUpdate -= updateAction;

        protected void InvokeUpdate() => OnUpdate?.Invoke(_value);
    }
}