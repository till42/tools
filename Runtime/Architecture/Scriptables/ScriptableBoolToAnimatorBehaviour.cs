using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [RequireComponent(typeof(Animator))]
    public class ScriptableBoolToAnimatorBehaviour : MonoBehaviour
    {
        [SerializeField] private ScriptableBool _scriptableBool;

        [SerializeField] private string _boolParameterName;

        private string ParameterName
            => !string.IsNullOrEmpty(_boolParameterName) ? _boolParameterName : _scriptableBool.name;

        private Animator _animator;

        private void Awake()
            => _animator = GetComponent<Animator>();

        private void OnEnable()
            => _scriptableBool.RegisterUpdateAction(HandleUpdate);

        private void HandleUpdate(bool value)
            => _animator.SetBool(ParameterName, value);

        private void OnDisable()
            => _scriptableBool.UnregisterUpdateAction(HandleUpdate);
    }
}