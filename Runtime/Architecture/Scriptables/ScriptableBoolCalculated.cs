﻿using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    public abstract class ScriptableBoolCalculated : ScriptableBool
    {
        [SerializeField] protected InputBool[] _inputBools = { };
        public InputBool[] InputBools => _inputBools;

        public override void Reset()
        {
            RegisterToInput();
            Evaluate();
        }

        public override bool Value
        {
            get => _value;
            // ReSharper disable ValueParameterNotUsed
            set => Debug.LogError("Cannot set a logical bool value. It read-only.");
            // ReSharper restore ValueParameterNotUsed
        }

        protected abstract bool Calculate();

        private void RegisterToInput()
        {
            foreach (var inputBool in _inputBools)
            {
                inputBool.ScriptableBool.RegisterUpdateAction(Evaluate);
            }
        }

        private void Evaluate(bool input = false)
        {
            var newValue = Calculate();
            if (newValue == _value) return;
            _value = newValue;
            InvokeUpdate();
        }
    }
}