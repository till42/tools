﻿using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableString", fileName = "New ScriptableString")]
    public class ScriptableString : ScriptableT<string>
    {
        public override void Reset() => Value = "";

        public static implicit operator string(ScriptableString scriptableString) => scriptableString.Value;
    }
}