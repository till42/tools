using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/Scriptable Int Condition Greater Than",
        fileName = "New ScriptableIntConditionGreaterThan")]
    public class ScriptableIntConditionGreaterThan : ScriptableIntCondition
    {
        [SerializeField] private int _threshold;

        public int Threshold
        {
            get => _threshold;
            private set => _threshold = value;
        }

        public override bool IsFulfilled(int value) => value > _threshold;
    }
}