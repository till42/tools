﻿using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableBool", fileName = "New ScriptableBool")]
    public class ScriptableBool : ScriptableT<bool>
    {
        public override void Reset() => Value = default;

        public void Toggle() => Value = !Value;

        public static implicit operator bool(ScriptableBool scriptableBool) => scriptableBool.Value;
    }
}