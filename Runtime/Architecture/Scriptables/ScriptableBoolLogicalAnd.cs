﻿using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableBoolLogicalAnd",
        fileName = "New ScriptableBoolLogicalAnd")]
    public class ScriptableBoolLogicalAnd : ScriptableBoolCalculated
    {
        protected override bool Calculate()
        {
            var result = true;
            foreach (var inputBool in InputBools)
                result &= inputBool.Value;

            return result;
        }

        public static implicit operator bool(ScriptableBoolLogicalAnd scriptableBool) => scriptableBool.Value;
    }
}