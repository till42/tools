using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableLeveledInt",
        fileName = "New ScriptableLeveledInt")]
    public class ScriptableLeveledInt : ScriptableLeveledT<int>
    {
    }
}