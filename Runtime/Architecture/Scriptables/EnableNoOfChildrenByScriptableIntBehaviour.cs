using System;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    public class EnableNoOfChildrenByScriptableIntBehaviour : EnableByScriptableIntBehaviour
    {
        protected override void SetChild(GameObject childObject, int index, int value)
        {
            childObject.SetActive(index < value);
        }
    }
}