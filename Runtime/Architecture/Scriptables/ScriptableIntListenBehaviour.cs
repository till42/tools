﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [DefaultExecutionOrder(1000)]
    public class ScriptableIntListenBehaviour : MonoBehaviour
    {
        [SerializeField] private ScriptableInt _scriptableInt;

        [SerializeField] private bool _logOnRaise;

        [SerializeField] private ScriptableIntCondition _condition;

        public UnityEvent OnEventRaised = new UnityEvent();

        public event Action OnEventAction;

        public ScriptableInt ScriptableInt
        {
            get => _scriptableInt;
            private set
            {
                if (_scriptableInt != null)
                    _scriptableInt.UnregisterUpdateAction(RaiseAction);

                _scriptableInt = value;

                if (value != null)
                    value.RegisterUpdateAction(RaiseAction);
            }
        }

        public ScriptableIntCondition Condition
        {
            get => _condition;
            private set => _condition = value;
        }

        private bool _wasRaised;

        private void OnEnable()
        {
            if (_scriptableInt == null) return;
            _scriptableInt.RegisterUpdateAction(RaiseAction);
        }

        public void Clear() => _wasRaised = false;

        private void RaiseAction(int value)
        {
            if (!RaiseConditionFulfilled(value)) return;

            if (_logOnRaise)
                Debug.Log($"Received RaiseAction from {_scriptableInt.name}", this);
            OnEventRaised?.Invoke();
            OnEventAction?.Invoke();
        }

        protected bool RaiseConditionFulfilled(int value)
        {
            if (_wasRaised) return false;

            if (Condition != null && !Condition.IsFulfilled(value)) return false;

            _wasRaised = true;

            return true;
        }

        private void OnDisable()
        {
            if (_scriptableInt == null) return;
            _scriptableInt.UnregisterUpdateAction(RaiseAction);
        }
    }
}