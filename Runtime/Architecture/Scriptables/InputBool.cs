﻿using System;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [Serializable]
    public struct InputBool
    {
        public bool IsNegated;
        public ScriptableBool ScriptableBool;

        public InputBool(ScriptableBool scriptableBool) : this(scriptableBool, false) { }

        public InputBool(ScriptableBool scriptableBool, bool isNegated)
        {
            ScriptableBool = scriptableBool;
            IsNegated = isNegated;
        }

        public bool Value
        {
            get
            {
                var b = ScriptableBool.Value;
                if (IsNegated)
                    b = !b;
                return b;
            }
        }
    }
}