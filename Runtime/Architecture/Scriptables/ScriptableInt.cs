﻿using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableInt", fileName = "New ScriptableInt")]
    public class ScriptableInt : ScriptableT<int>
    {
        [SerializeField] private int _minValue = int.MinValue;

        public int MinValue
        {
            get => _minValue;
            private set => _minValue = value;
        }

        protected override void Validate(ref int value)
        {
            base.Validate(ref value);
            if (value >= _minValue) return;
            value = _minValue;
        }

        public void Add(int valueToAdd) => Value += valueToAdd;

        public void Subtract(int valueToSubtract) => Add(-valueToSubtract);
        public override void Reset() => Value = default;

        public static implicit operator int(ScriptableInt scriptableInt) => scriptableInt.Value;
    }
}