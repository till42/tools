﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [DefaultExecutionOrder(1000)]
    public class ScriptableEventListenBehaviour : MonoBehaviour
    {
        [SerializeField] private ScriptableEvent _scriptableEvent;

        public ScriptableEvent ScriptableEvent
        {
            get => _scriptableEvent;
            private set
            {
                if (_scriptableEvent != null)
                    _scriptableEvent.UnRegisterRaiseAction(RaiseAction);

                _scriptableEvent = value;

                if (value != null)
                    value.RegisterRaiseAction(RaiseAction);
            }
        }

        [SerializeField] private bool _logOnRaise;

        public UnityEvent OnEventRaised = new UnityEvent();

        public event Action OnEventAction;

        private void OnEnable()
        {
            if (_scriptableEvent == null) return;
            _scriptableEvent.RegisterRaiseAction(RaiseAction);
        }

        private void RaiseAction()
        {
            if (_logOnRaise)
                Debug.Log($"Received RaiseAction from {_scriptableEvent.name}", this);
            OnEventRaised?.Invoke();
            OnEventAction?.Invoke();
        }

        private void OnDisable()
        {
            if (_scriptableEvent == null) return;
            _scriptableEvent.UnRegisterRaiseAction(RaiseAction);
        }
    }
}