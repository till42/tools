﻿using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/Scriptable One-Time Event",
        fileName = "New ScriptableOneTimeEvent")]
    public class ScriptableOneTimeEvent : ScriptableEvent
    {
        private bool _wasRaised;

        public override void Raise()
        {
            if (_wasRaised)
                return;
            base.Raise();
            _wasRaised = true;
        }

        public void Reset()
        {
            _wasRaised = false;
        }
    }
}