﻿using System;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [Serializable]
    public class ScriptableEventInt : ScriptableEventT<int> { }
}