﻿using System;
using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    public abstract class ScriptableEventT<T> : ScriptableObject
    {
        [SerializeField] private bool _logOnRaise;

        private event Action<T> OnRaise;

        [ContextMenu("Raise")]
        public void Raise(T value)
        {
            if (_logOnRaise) Debug.Log($"Event {name} raised.");
            OnRaise?.Invoke(value);
        }

        public void RegisterRaiseAction(Action<T> raiseAction) => OnRaise += raiseAction;

        public void UnRegisterRaiseAction(Action<T> raiseAction) => OnRaise -= raiseAction;
    }
}