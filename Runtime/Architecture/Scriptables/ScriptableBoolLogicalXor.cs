﻿using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableBoolLogicalXor",
        fileName = "New ScriptableBoolLogicalXor")]
    public class ScriptableBoolLogicalXor : ScriptableBoolCalculated
    {
        protected override bool Calculate()
        {
            var result = true;
            foreach (var inputBool in InputBools)
                result &= inputBool.Value;

            return result;
        }

        public static implicit operator bool(ScriptableBoolLogicalXor scriptableBool) => scriptableBool.Value;
    }
}