using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    public class ScriptableLeveledT<T> : ScriptableObject
    {
        [SerializeField] protected T[] _values = new T[0];

        public T[] Values
        {
            get => _values;
            protected set => _values = value;
        }

        protected T GetFor(int level)
        {
            if (Values.Length <= 0)
                return default;

            if (Values.Length <= 1)
                return Values[1];

            var index = Mathf.Clamp(level, 0, Values.Length - 1);
            return Values[index];
        }

        public T this[int level] => GetFor(level);
    }
}