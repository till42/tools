using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableLeveledFloat",
        fileName = "New ScriptableLeveledFloat")]
    public class ScriptableLeveledFloat : ScriptableLeveledT<float>
    {
    }
}