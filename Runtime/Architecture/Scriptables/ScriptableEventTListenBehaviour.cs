﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    public abstract class ScriptableEventTListenBehaviour<T> : MonoBehaviour
    {
        [SerializeField] private ScriptableEventT<T> _scriptableEvent;

        public ScriptableEventT<T> ScriptableEvent
        {
            get => _scriptableEvent;
            private set
            {
                if (_scriptableEvent != null)
                    _scriptableEvent.UnRegisterRaiseAction(RaiseAction);

                _scriptableEvent = value;

                if (value != null)
                    value.RegisterRaiseAction(RaiseAction);
            }
        }

        [SerializeField] private bool _logOnRaise;

        public UnityEvent<T> OnEventRaised = new UnityEvent<T>();

        public event Action<T> OnEventAction;

        private void OnEnable()
        {
            if (_scriptableEvent == null) return;
            _scriptableEvent.RegisterRaiseAction(RaiseAction);
        }

        private void RaiseAction(T value)
        {
            if (_logOnRaise)
                Debug.Log($"Received RaiseAction from {_scriptableEvent.name}", this);
            OnEventRaised?.Invoke(value);
            OnEventAction?.Invoke(value);
        }

        private void OnDisable()
        {
            if (_scriptableEvent == null) return;
            _scriptableEvent.UnRegisterRaiseAction(RaiseAction);
        }
    }
}