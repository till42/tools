using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    public abstract class ScriptableIntCondition : ScriptableObject
    {
        public abstract bool IsFulfilled(int value);
    }
}