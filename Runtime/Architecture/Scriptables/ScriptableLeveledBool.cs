using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableLeveledBool",
        fileName = "New ScriptableLeveledBool")]
    public class ScriptableLeveledBool : ScriptableLeveledT<bool>
    {
    }
}