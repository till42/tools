﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableEvent", fileName = "New ScriptableEvent")]
    public class ScriptableEvent : ScriptableObject
    {
        [SerializeField] private bool _logOnRaise;

        private event Action OnRaise;
        public UnityEvent RaisedEvent = new UnityEvent();

        [ContextMenu("Raise")]
        public virtual void Raise()
        {
            if (_logOnRaise) Debug.Log($"Event {name} raised.");
            OnRaise?.Invoke();
            RaisedEvent?.Invoke();
        }

        public void RegisterRaiseAction(Action raiseAction) => OnRaise += raiseAction;

        public void UnRegisterRaiseAction(Action raiseAction) => OnRaise -= raiseAction;
    }
}