﻿using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableFloat", fileName = "New ScriptableFloat")]
    public class ScriptableFloat : ScriptableT<float>
    {
        public override void Reset() => Value = default;

        public void Add(float valueToAdd) => Value += valueToAdd;

        public void Subtract(float valueToSubtract) => Add(-valueToSubtract);

        public static implicit operator float(ScriptableFloat scriptableFloat) => scriptableFloat.Value;
    }
}