﻿using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    [CreateAssetMenu(menuName = "FantasyArts/Architecture/ScriptableBoolLogicalOr",
        fileName = "New ScriptableBoolLogicalOr")]
    public class ScriptableBoolLogicalOr : ScriptableBoolCalculated
    {
        protected override bool Calculate()
        {
            var result = true;
            foreach (var inputBool in InputBools)
                result &= inputBool.Value;

            return result;
        }

        public static implicit operator bool(ScriptableBoolLogicalOr scriptableBool) => scriptableBool.Value;
    }
}