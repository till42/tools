﻿using UnityEngine;

namespace FantasyArts.Tools.Runtime.Architecture.Scriptables
{
    public abstract class EnableByScriptableIntBehaviour : MonoBehaviour
    {
        [SerializeField] private ScriptableInt _scriptableInt;
        [SerializeField] private int _offset;


        private void OnEnable()
        {
            _scriptableInt.RegisterUpdateAction(HandleUpdate);
        }

        private void HandleUpdate(int value)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                var childObject = child.gameObject;
                SetChild(childObject, i, value + _offset);
            }
        }

        protected abstract void SetChild(GameObject childObject, int index, int value);

        private void OnDisable()
        {
            _scriptableInt.UnregisterUpdateAction(HandleUpdate);
        }
    }
}