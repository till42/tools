﻿using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Architecture.UnityEvents
{
    public class EnableEventBehaviour : MonoBehaviour
    {
        public UnityEvent EnableEvent;

        private void OnEnable() => EnableEvent?.Invoke();
    }
}