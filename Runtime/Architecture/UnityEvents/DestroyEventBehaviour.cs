﻿using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Architecture.UnityEvents
{
    public class DestroyEventBehaviour : MonoBehaviour
    {
        public UnityEvent DestroyEvent;

        private void OnDestroy() => DestroyEvent?.Invoke();
    }
}