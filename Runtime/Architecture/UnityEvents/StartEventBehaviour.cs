﻿using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Architecture.UnityEvents
{
    public class StartEventBehaviour : MonoBehaviour
    {
        public UnityEvent StartEvent;

        private void Start() => StartEvent?.Invoke();
    }
}