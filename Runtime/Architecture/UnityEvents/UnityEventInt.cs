using System;
using UnityEngine.Events;

namespace FantasyArts.Tools.Architecture.UnityEvents
{
    [Serializable]
    public class UnityEventInt : UnityEvent<int>
    {
    }
}