﻿using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Architecture.UnityEvents
{
    public class DisableEventBehaviour : MonoBehaviour
    {
        public UnityEvent DisableEvent;

        private void OnDisable() => DisableEvent?.Invoke();
    }
}