using System;
using UnityEngine;
using UnityEngine.Events;

namespace FantasyArts.Tools.Runtime.Architecture.UnityEvents
{
    public class AwakeEventBehaviour : MonoBehaviour
    {
        public UnityEvent AwakeEvent;

        private void Awake() => AwakeEvent?.Invoke();
    }
}