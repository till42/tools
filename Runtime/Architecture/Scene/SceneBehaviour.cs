using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;

#endif

namespace FantasyArts.Tools.Runtime.Architecture.Scene
{
    public class SceneBehaviour : MonoBehaviour
    {
        [SerializeField] private SceneReference _scene;

        [SerializeField] private bool _doesLoadScenesNotInBuild;

        [ContextMenu("Load")]
        public void Load()
        {
#if UNITY_EDITOR
            if (_doesLoadScenesNotInBuild)
            {
                EditorSceneManager.LoadSceneInPlayMode(_scene.ScenePath, new LoadSceneParameters(LoadSceneMode.Single));
                return;
            }
#endif

            SceneManager.LoadScene(_scene);
        }

        public void Reload()
        {
            var scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
}