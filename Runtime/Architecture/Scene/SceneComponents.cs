using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.SceneManagement.SceneManager;

namespace FantasyArts.Tools.Architecture.Scene
{
    public static class SceneComponents
    {
        public static List<T> GetAll<T>(bool includeInactive = true)
        {
            var output = new List<T>();
            for (int i = 0; i < sceneCount; i++)
            {
                var scene = GetSceneAt(i);

                var rootGameObjects = scene.GetRootGameObjects();
                foreach (var rootGameObject in rootGameObjects)
                {
                    var components = rootGameObject.GetComponentsInChildren<T>(includeInactive);
                    output.AddRange(components);
                }
            }

            return output;
        }
    }
}