using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace FantasyArts.Tools.Runtime.Architecture.Scene
{
    public class QuitBehaviour : MonoBehaviour
    {
        public void Quit()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }
    }
}