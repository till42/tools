# About Fantasy Arts Tools

Fantasy Arts Tools is a collection of useful scripts for all kinds of Unity projects.

# Installing Fantasy Arts Tools

Download the package
Install Fluent Assertions from https://github.com/BoundfoxStudios/fluentassertions-unity.git#upm

# Using Fantasy Arts Tools

Use the namespace. See documentation.