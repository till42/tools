# Changelog

All notable changes to this package will be documented in this file. The format is based
on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## [0.1.1] - 2020-12-14
### Created
- Initial creation

## [0.1.2] - 2021-01-25
- Added Editor Script: HierarchyWindowGroupHeader, which shows all Game Objects in the Hierarchy in a different style,
  if preceded with "---"
- Added ScriptableInt, ScriptableFloat, ScriptableBool with tests

## [0.1.3] - 2021-01-25
- Added Post Build: ZIP WebGL files

## [0.1.4] - 2021-01-26
- Created UI Scripts (Update TMP) for ScriptableInt, Bool and Float

## [0.1.5] - 2021-01-26
- Added Post Build: ZIP WebGL files

## [0.1.7] - 2021-04-08
- Changed Root Namespace

## [0.2.0] - 2021-04-08
- Added MinValue to ScriptableInt

## [0.2.1] - 2021-04-08
- Added ScriptableIntListener

## [0.2.2] - 2021-04-08
Added int.EmptyIfZero extension incl. Tests

## [0.2.3] - 2021-04-09
Added Lifetime Event Behaviours (Awake, Enable, Start, Disable, Destroy)
Added LogBehaviour for testing Added QuitBehaviour

## [0.2.4] - 2021-04-09
Added Reload to SceneBehaviour.cs

## [0.2.5] - 2021-04-10
Added ScriptableIntConditions

## [0.2.6] - 2021-04-10
Added completedly white and black sprites

## [0.2.7] - 2021-04-10
Added List.PickRandomItem()

## [0.2.8] - 2021-04-10
Enhanced ScriptableIntListener so that it only raises once until cleared

## [0.3.0] - 2021-04-11
Added StateMachine scripts

## [0.3.1] - 2021-04-12
Added Playmode Load Scene Test

## [0.3.2] - 2021-04-12
Updated IState to include Add Transition

## [0.3.3] - 2021-05-01
Added StateMachine level conditional triggers: AddTriggerFromAnyState

## [0.3.4] - 2021-05-04
Cleaned up editor namespace

## [0.3.5] - 2021-05-16
Added ScriptableInt.Add and Substract functions

## [0.4.1] - 2021-05-22
Added Hitpoint Class incl. UnitTests

## [0.4.2] - 2021-05-23
Made Hitpoint Class Serializeable

## [0.4.3] - 2021-05-23
Added IsDepleted, OnDepleted to Hitpoint. Fixed Percentage

## [0.4.4] - 2021-05-24
Removed depency from platform "GameCoreScarlett", "GameCoreXboxOne" (bug in asmdef of playmode tests)

## [0.4.5] - 2021-05-24
Added Sample class for draw samples without replacement of IEnumerable Added Duplicate extension for IEnumerable to
deep-copy

## [0.4.6] - 2021-05-30
Changed SCriptableT.OnEnabled to protected virtual so that it can be overriden

## [0.4.7] - 2021-05-31
Added ArrayExtensions.GetRollingElementAt

## [0.4.8] - 2021-06-01
Created Find.ScriptableObject<T>()

## [0.5.0] - 2021-06-07
Created SCriptableLeveledFloat, Int and Bool

## [0.5.1] - 2021-06-12
Added SceneReference

## [0.5.3] - 2021-06-12
Added Time conversion (Minutes and Seconds) values for float

## [0.5.4] - 2021-06-12
Made UpdateTextFromScriptable extensible

## [0.5.5] - 2021-06-16
Extended SceneBehaviour so that it can also load scenes not in Build from Editor Playmode

## [0.5.6] - 2021-06-19
Added DragMove2dBehaviour

## [0.5.7] - 2021-06-21
Added DragMove2dBehaviour Events

## [0.5.8] - 2021-06-22
Added DragMove2dBehaviour PointerDown and PointerUp Events

## [0.5.9] - 2021-06-22
Adjusted DragMove2dBehaviour to record position with Pointerdown

## [0.5.10] - 2021-07-08
### Added
- State Machine Scriptable

## [0.5.11] - 2021-07-10
### Added
- State Machine Scriptable with nested States and Custom Inspector
- Disabling of Inspector Functions while State Machine is running
- text to Scriptable State "[Default]" in the inspector for the first item
- Created Icons for State Machine and State

## [0.5.12] - 2021-07-10
### Fixed

- StateScriptalbe Transitions could be null, which led to a Runtime Error

## [0.5.13] - 2021-07-10
### Added
- StateMachine can be reset when destroyed in StateMachineInitializeBehaviour

## [0.5.14] - 2021-07-10
### Added
- EnableChildIndexByScriptableIntBehaviour
- EnableNoOfChildrenByScriptableIntBehaviour

## [0.5.15] - 2021-07-10
### Added
- Offset to EnableChildIndexByScriptableIntBehaviour
- Offset to EnableNoOfChildrenByScriptableIntBehaviour

## [0.5.16] - 2021-07-10
### Added
- Check for current in State Listener Initialize

## [0.5.17] - 2021-07-11
### Changed
- Change State Name only in State Inspector

## [0.5.18] - 2021-07-14
### Added
- References to be seen inspector
- Naming of States with "01.", "02."

## [0.6.1] - 2021-08-29
### Added
- Instance Support for State Machine

## [0.6.2] - 2021-08-29
### Bugfix
- StateInstanceListener could not be used without a state defined in the Editor

## [0.6.3] - 2021-08-30
### Changed
- Removed dependency on Odin Inspector

## [0.6.4] - 2021-08-30
### Changed
- Removed dependency on Odin Inspector

## [0.6.5] - 2021-08-30
### Removed
- AssemblyDefinition GUID References

## [0.6.6] - 2021-08-30
### Removed
- Using Sirenix statements if not defined

## [0.6.7] - 2021-08-30
### Removed
- Another dependency to Sirenix

## [0.6.8] - 2021-08-30
### Removed
- Another dependency to Sirenix

## [0.6.9] - 2021-08-31
### Fixed
- Compile error on build in StateMachineScriptable

## [0.6.10] - 2021-09-01
### Added
- interface for StateMachine IStateMachine
- StateMachineInstantiateBehaviour as IStateMachine

## [0.6.11] - 2021-11-21
### Added
- ScriptableEvents with parameter T

## [0.6.12] - 2021-11-21
### Changed
- Default Execution order of Event Listeners

## [0.6.13] - 2021-11-27
### Added
- TimerBehaviour
### Changed
- ScriptableFloat has now add and subtract
- Corrected spelling in ScriptableInt.Subtract
- UpdateTextFromScriptableFloat has now format parameter

## [0.6.14] - 2021-11-27
### Added
- TimeScriptableBehaviour
- Countdown class (reverse stopwatch)

## [0.6.15] - 2021-11-27
### Fixed
- Removed unnecessary using from TimeScriptableBehaviour

## [0.6.16] - 2021-11-27
### Added
- List Extensions: List.Shuffle() and List.Swap

## [0.6.17] - 2021-11-30
### Added
- ScriptlessEditor.cs to quickly create CustomInspectors without the Script field

## [0.6.18] - 2021-12-03
### Added
- Script to find missing references in the scene

## [0.6.19] - 2021-12-09
### Fixed
- StateListenerBehaviour was not always initialized when disabled

## [0.6.20] - 2021-12-20
### Removed
- Fluent Assertions DLL
### Added
- Reference to Boundfox Fluent Assertiions Package

## [0.6.21] - 2021-12-21
### Added
- Boundfox Fluent Assertiions Package as dependency

## [0.6.22] - 2021-12-21
### Changed
- Removed Boundfox Fluent Assertiions dependency (not supported by package manager to have a dependency to external git)

## [0.7.1] - 2022-01-11
### Changed
- Added Test suite for overriding serialized private fields: component.SetSerialized("_field",value) 

## [0.7.2] - 2022-01-11
### Changed
- Integrated enums and references into setter and getter for serialized private fields:
component.SetSerialized("_enum", value) component.GetSerialized<MyBehaviour>("_reference"); 

## [0.7.3] - 2022-01-29
### Added
- ScriptableString functionality 

## [0.7.4] - 2022-01-29
### Added
- UpdateTextFromScriptableString functionality

## [0.7.5] - 2022-02-01
### Added
- Test Helper for ClassFile path (var path = ClassFilePath.GetFileInSubfolder(subfolder, filename);)

## [0.7.6] - 2022-02-18
### Added
- ScriptableBoolToAnimator
  extended SerializeField tests for primitive arrays
- Test helper: SerializedPropertyExtensions
- Scritpable Calculated Bools
- Cursor System

## [0.7.7] - 2022-02-18
### Changed
- CursorSystem.Reset() to public for access in the inspector

## [0.7.8] - 2022-02-19
### Changed
- ScriptableBoolToAnimator: use scriptableBoolName if no boolParameterName is specified

## [0.7.9] - 2022-02-19
### Added
- ScriptableBool.Toggle function

## [0.7.10] - 2022-02-19
### Added
- ReceivePointer has default UnityEvent

## [0.7.11] - 2022-02-19
### Added
- UpdateSliderFromScriptableInt
- ScriptableEventPointer

## [0.7.12] - 2022-02-20
### Added
- Feature to define disable pointer in Cursor system which then disables receives

## [0.7.13] - 2022-04-12
### Added
- ComponentExtensions.GetComponentInScene (includes Interfaces)
- ComponentExtensions.GetComponentsInScene (includes Interfaces)

## [0.7.50] - 2022-05-03
### Added
- ScriptableTriggers for StateMachine
- Custom Inspector for StateListenerBehaviour
### Fixed
- State Renaming Issues in Custom inspector

## [0.7.51] - 2022-05-04
### Fixed
- Disabled StateListener does not disable gameobject on Initialize
- Icon for StateListenerBehaviour
- Menu Position for TriggerScriptable

## [0.7.52] - 2022-05-06
### Fixed
- State Machine with fast play mode
- Enable Event fired more than once when already active
### Added
- Scene State Machine Behaviour to reinitialize the state machine on enable

## [0.7.53] - 2022-05-06
### Added
- Timed Event Behaviour

## [0.7.54] - 2022-05-06
### Added
- TimeScriptableBehaviour has possibility to not run at start

## [0.7.55] - 2022-05-06
### Fixed
- Self referenced transitions

## [0.7.56] - 2022-06-21
### Fixed
- Post Build Zip: no error when folder does not exist (e.g. Template Data)

## [0.7.57] - 2022-08-20
### Fixed
- WebGL Build Popup does not throw errors by default

## [0.7.58] - 2022-08-20
### Added
- Open Explorer Window with WebGL Build folder after build

## [0.7.59] - 2022-08-26
### Fixed
- Serialized Value can also set / get inherited private fields

## [0.7.60] - 2022-09-27
### Fixed
- UpdateTextFromScriptableBool did not unregister itself on disable

## [0.7.61] - 2022-10-17
### Fixed
- TransitionToScriptable works now with second transition, when transisitionScriptable is null

## [0.7.62] - 2022-10-17
### Fixed
- TransitionToScriptable refix

## [0.7.63] - 2023-03-26
### Added
- Scripting symbol: TOOLS_SKIP_GENERIC_BUILD_POPUP (disable Build WebGL Popup)
- Scripting symbol: TOOLS_SKIP_GENERIC_ITCH_POSTBUILDPROCESS (disable ZIP on WebGL build postprocess)

## [0.7.64] - 2023-11-07
### Fixed
- TransitionToScriptable added constructor for triggerScriptable

## [0.7.65] - 2023-11-07
### Added
- StateListenerBehaviour: Flag & functionality to only enable on enter / disable on exit

## [0.7.66] - 2023-11-07
### Changed
- StateListenerBehaviour: opened more functionality for inheritance

## [0.7.67] - 2024-08-30
### Changed
- Itch.io Build Post Process: Added "StreamingAssets" Folder to be included in build.zip

## [0.7.68] - 2024-12-13
### Changed
- Custom Icons for Scene Reference in editor (did not work for Unity 6)